package com.hqt.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.junit.Test;

import java.time.Duration;
import java.util.Properties;
import java.util.regex.Pattern;

public class KafkaTest {

    @Test
    public void t1(){

    }
    @Test
    public void testSubscribe() {
        Properties properties = new Properties();
        // Kafka集群地址
        properties.put("bootstrap.servers", "localhost:9092");
        // 消费者组，仅在subscribe模式下生效，用于分区自动再均衡，而assign模式直接指定分区
        properties.put("group.id", "default_consumer_group");
        // 反序列化器
        properties.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        properties.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(properties);
        // 订阅topic
        String topic = "test";
        consumer.subscribe(Pattern.compile(topic));
        while (true) {
            // 每1000ms轮询一次
            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000L));
            for (ConsumerRecord<String, String> record : records) {
                System.out.println(String.format("-------消息来了：topic={%s}, partition={%s}, offset={%s}, value={%s}", record.topic(), record.partition(),
                        record.offset(), record.value()));
            }
        }
    }

    @Test
    public void testProducer(){
        Properties properties = new Properties();
        // Kafka集群地址
        properties.put("bootstrap.servers", "localhost:9092");
        // 反序列化器
        properties.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        properties.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        Producer<String,String> kafkaProducer = new KafkaProducer<>(properties);
        String topicName = "test";
        ProducerRecord<String,String> producerRecord = new ProducerRecord<>(topicName,"生产消息"+System.currentTimeMillis());
        kafkaProducer.send(producerRecord);
        kafkaProducer.close();
    }
}
