package com.hqt.thread;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.junit.Test;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadTest {
    private final static ThreadFactory namedThreadFactory = new ThreadFactoryBuilder().setNameFormat("demo-pool-%d").build();
    private final static ExecutorService executorService = new ThreadPoolExecutor(5 , 200, 0L,
            TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>(1024), namedThreadFactory,
            new ThreadPoolExecutor.AbortPolicy());
    @Test
    public void t1() throws ExecutionException, InterruptedException {

        for (int i = 0; i < 10; i++) {
            executorService.submit(() -> {
                System.out.println(Thread.currentThread().getName() + ": 线程池执行任务！");
            });
        }
        // 如果Executor后台线程池还没有完成Callable的计算，这调用返回Future对象的get()方法，会阻塞直到计算完成。
        for (int i = 0; i < 100; i++) {
            Future<TestObject> submit = executorService.submit(new TestCallable());
            System.out.println(submit.get().getValue()+","+submit.get().getCas_value());
        }
        // 关闭线程池
        executorService.shutdown();
    }
    @Test
    public void t2() throws ExecutionException, InterruptedException {

        // 如果Executor后台线程池还没有完成Callable的计算，这调用返回Future对象的get()方法，会阻塞直到计算完成。
        for (int i = 0; i < 100; i++) {
            Future<Integer> submit = executorService.submit(new TicketCallable());
            System.out.println("剩余："+ submit.get());
        }
        // 关闭线程池
        executorService.shutdown();
    }

    @Test
    public void t3() {
        //窗口01
        new Thread(() -> {
            while (true) {
                //售票并获取售票后的当前票余量
                int currentTickets = TestTicket.saleTickets();
                //模拟售票员卖出一张票用时1秒
                //waitProcess();
                //票已卖完
                if (currentTickets <= 0) break;
            }
        }, "01").start();

        //窗口02
        new Thread(() -> {
            while (true) {
                int currentTickets = TestTicket.saleTickets();
                //waitProcess();
                if (currentTickets <= 0) break;
            }
        }, "02").start();

        //窗口03
        new Thread(() -> {
            while (true) {
                int currentTickets = TestTicket.saleTickets();
                //waitProcess();
                if (currentTickets <= 0) break;
            }
        }, "03").start();


    }
    private static void waitProcess() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
