package com.hqt.thread;

public class TestObject {
    private int value;
    private int cas_value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getCas_value() {
        return cas_value;
    }

    public void setCas_value(int cas_value) {
        this.cas_value = cas_value;
    }
}
