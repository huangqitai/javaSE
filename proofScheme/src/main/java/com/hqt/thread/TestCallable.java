package com.hqt.thread;

import java.util.Random;
import java.util.concurrent.Callable;

public class TestCallable implements Callable<TestObject> {
    @Override
    public TestObject call() throws Exception {

        Random random = new Random();
        Integer randomInt = null;
        randomInt = random.nextInt(100) + 1;
        TestObject testObject = new TestObject();
        testObject.setValue(randomInt);
        testObject.setCas_value(randomInt);
        return testObject;
    }
}

class TicketCallable implements Callable<Integer> {
    @Override
    public Integer call() throws Exception {
        int currentTickets = TestTicket.saleTickets();
        return currentTickets;
    }
}
