package com.hqt.aes;

import org.apache.commons.codec.binary.Hex;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import java.nio.charset.StandardCharsets;

/**
 * @author longzy
 * @Description
 * @date 2022/10/28 11:42
 */
public class AESUtils {


    public AESUtils() {
    }

    public static byte[] AES_CBC_Decrypt(byte[] data, byte[] key, byte[] iv) throws Exception {
        Cipher cipher = getCipher(2, key, iv);
        return cipher.doFinal(data);
    }

    public static byte[] AES_CBC_Encrypt(byte[] data, byte[] key, byte[] iv) throws Exception {
        Cipher cipher = getCipher(1, key, iv);
        return cipher.doFinal(data);
    }

    private static Cipher getCipher(int mode, byte[] key, byte[] iv) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
        cipher.init(mode, secretKeySpec, new IvParameterSpec(iv));
        return cipher;
    }

    public static String encrypt(String content, String expandedKey) {
        return encrypt(content, expandedKey, expandedKey);
    }

    public static String encrypt(String content, String expandedKey, String expandedIv) {
        try {
            byte[] data = content.getBytes(StandardCharsets.UTF_8);
            byte[] key = expandedKey.getBytes(StandardCharsets.UTF_8);
            byte[] iv = expandedIv.getBytes(StandardCharsets.UTF_8);

            try {
                byte[] json = AES_CBC_Encrypt(data, key, iv);
                return Hex.encodeHexString(json);
            } catch (Exception var7) {

            }
        } catch (Exception var8) {

        }

        return null;
    }

    public static String encryptBase64(String content, String expandedKey, String expandedIv) {
        try {
            byte[] data = content.getBytes(StandardCharsets.UTF_8);
            byte[] key = expandedKey.getBytes(StandardCharsets.UTF_8);
            byte[] iv = expandedIv.getBytes(StandardCharsets.UTF_8);

            try {
                byte[] json = AES_CBC_Encrypt(data, key, iv);
                return encryptBase64(json);
            } catch (Exception var7) {
            }
        } catch (Exception var8) {
        }

        return null;
    }

    public static String decrypt(String content, String expandedKey) {
        try {
            byte[] data = Hex.decodeHex(content.toCharArray());
            byte[] key = expandedKey.getBytes();

            try {
                byte[] json = AES_CBC_Decrypt(data, key, key);
                return new String(json);
            } catch (Exception var5) {
            }
        } catch (Exception var6) {
        }

        return null;
    }

    public static String decrypt(String content, String expandedKey, String expandedIv) {
        try {
            byte[] data = Hex.decodeHex(content.toCharArray());
            byte[] key = expandedKey.getBytes(StandardCharsets.UTF_8);
            byte[] iv = expandedIv.getBytes(StandardCharsets.UTF_8);

            try {
                byte[] json = AES_CBC_Decrypt(data, key, iv);
                return new String(json);
            } catch (Exception var7) {
            }
        } catch (Exception var8) {
        }

        return null;
    }

    public static String decryptBase64(String content, String expandedKey, String expandedIv) {
        try {
            byte[] data = decryptBase64(content);
            byte[] key = expandedKey.getBytes(StandardCharsets.UTF_8);
            byte[] iv = expandedIv.getBytes(StandardCharsets.UTF_8);

            try {
                byte[] json = AES_CBC_Decrypt(data, key, iv);
                return new String(json);
            } catch (Exception var7) {
            }
        } catch (Exception var8) {
        }

        return null;
    }

    public static byte[] decryptBase64(String key) {
        return DatatypeConverter.parseBase64Binary(key);
    }

    public static String encryptBase64(byte[] key) {
        return DatatypeConverter.printBase64Binary(key);
    }

    public static void main(String[] args) {
        String encrypt = encrypt("nxnf@2020", "gisq39561c9fe068");
        System.out.println(encrypt);
        String s = decrypt(encrypt, "gisq39561c9fe068");
        System.out.println(s);
    }
}
