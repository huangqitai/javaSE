package com.hqt.zebds;

import com.alibaba.fastjson.JSONObject;
import org.junit.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexTest {
    @Test
    public void t1(){
        String s = "江门市蓬江区良化新村东78号二层3'-6'+2.5MB1-E1+2M1-2M-4J-3'轴";
        String ss = "{\"data\":{\"qlxx_list\":[{\"bsm\":\"8418ddba-7e5d-4728-b213-fece2c3cc908\",\"tyywh\":\"20220520-54518\",\"dyr\":\"彭卫雄\",\"bdbzzqse\":192.66,\"htbh\":null,\"dyfs\":\"2\",\"djyy\":\"抵押权注销\",\"dbfw\":null,\"zjjzwzl\":\"江门市蓬江区良化新村东78号二层3'-6'+2.5MB1-E1+2M1-2M-4J-3'轴\",\"zjjzwdyfw\":null,\"zwlxqssj\":\"2014-05-23 00:00:00\",\"zwlxjssj\":\"2024-05-23 00:00:00\",\"zgzqqdss\":null,\"zgzqse\":192.66,\"jedw\":\"2\",\"dyawjz\":null,\"fj\":null,\"dysx\":\"1\"}],\"qlrxx_list\":[{\"bsm\":\"22604d1a-fb54-4d7b-89a7-0cab99b2a63a\",\"tyywh\":\"20220520-54518\",\"qlrlb\":\"8\",\"sxh\":null,\"qlrmc\":\"彭卫雄\",\"zjzl\":\"1\",\"zjh\":\"440701196901260314\",\"gj\":\"142\",\"xb\":null,\"dz\":null,\"yb\":null,\"qlbl\":null,\"qlrlx\":\"1\",\"gyfs\":\"0\",\"gyqk\":\"单独所有\",\"qlrfrmc\":null,\"qlrfrdh\":null,\"qlrfrzjzl\":null,\"qlrfrzjh\":null,\"qlrfrlxdz\":null,\"qlrdlrmc\":null,\"qlrdlrdh\":null,\"qlrdlrzjzl\":null,\"qlrdlrzjh\":null,\"qlrdljg\":null,\"qlrdljgdm\":null,\"bz\":null},{\"bsm\":\"e79e65c5-84cd-43e9-8cb1-1b63951524fa\",\"tyywh\":\"20220520-54518\",\"qlrlb\":\"7\",\"sxh\":null,\"qlrmc\":\"中国建设银行股份有限公司江门市分行\",\"zjzl\":\"7\",\"zjh\":\"914407038939331498\",\"gj\":\"142\",\"xb\":null,\"dz\":null,\"yb\":null,\"qlbl\":null,\"qlrlx\":\"2\",\"gyfs\":\"0\",\"gyqk\":\"单独所有\",\"qlrfrmc\":null,\"qlrfrdh\":null,\"qlrfrzjzl\":null,\"qlrfrzjh\":null,\"qlrfrlxdz\":null,\"qlrdlrmc\":\"饶洪光\",\"qlrdlrdh\":\"13422601999\",\"qlrdlrzjzl\":\"1\",\"qlrdlrzjh\":\"441422197305220050\",\"qlrdljg\":null,\"qlrdljgdm\":null,\"bz\":null}],\"dyxx_list\":[{\"bsm\":\"77289852-de37-40f9-a3bf-f8d2b2edad1b\",\"tyywh\":\"20220520-54518\",\"djlx\":\"400\",\"dybm\":\"H\",\"qxdm\":\"440703\",\"qlbm\":\"FDCQ\",\"ydybsm\":\"440703008007GB00019F00010117\",\"xdybsm\":null,\"yqlbsm\":\"fdc13013-0000000000000166064079686\",\"qtdjbm\":\"DYAQ_ZX\",\"yqtdjbsm\":\"dy2014450723382379686\",\"gxsj\":\"2022-05-20 18:40:03\",\"xdybbh\":null,\"ydybbh\":\"2\",\"zl\":\"江门市蓬江区良化新村东78号二层3’-6’+2.5M B1-E1+2M 1-2M-4 J-3’轴\",\"bdcdyh\":\"440703008007GB00019F00010117\",\"mj\":\"2190.9/370.5\",\"yt\":\"商业服务\",\"cflx\":null,\"fwbm\":\"79686\"}],\"clxx_list\":[{\"xh\":1,\"tyywh\":\"20220520-54518\",\"sjsj\":null,\"sjsl\":null,\"sfsjsy\":null,\"sfewsj\":null,\"sfbcsj\":null,\"ys\":null,\"bz\":null,\"cllj\":null,\"qxdm\":null,\"qlbsm_ywsb\":null,\"bsm\":\"b37d89d1-6e52-4825-adca-f2925aca0423\"}],\"sqxx_list\":[{\"dd\":1,\"hh\":0,\"sbr\":\"09fdf5\",\"lzrdh\":null,\"qxdm\":\"440703\",\"djsxbm\":\"BDC02301440053040002\",\"sqzsbs\":\"0\",\"sfgg\":null,\"lzrzjzl\":null,\"ywh\":\"20220520-54518\",\"sllx\":\"抵押权-注销登记\",\"lzrmc\":null,\"lzrdz\":null,\"sfczxzyd\":null,\"bz\":null,\"mm\":0,\"dqszhj\":null,\"djfj\":null,\"lzfs\":\"6\",\"blzt\":0,\"lzrzjh\":null,\"djr\":\"许钊怡\",\"djsj\":\"2022-05-25 11:19:41\",\"sqfbcz\":\"1\",\"blyj\":\"注销登记\",\"qlbsm_ywsb\":null,\"zsfflx\":\"1\",\"sfznsp\":0,\"adyfbfz\":\"1\",\"bsm\":\"23c3fe5d-db04-4c4d-a227-a76256be9f87\",\"tyywh\":\"20220520-54518\"}],\"sfxx_list\":[],\"lzr_list\":[]}}";
        System.out.println(RegexCheckUtil.isValid(ss));
    }
    @Test
    public void t2(){
        String s = "XMGUID={xmGuid} and a={aa}";
        String regex = "\\{([^}]*)}";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(s);

        while (matcher.find()) {
            //extractedContent = matcher.group(1);
            System.out.println("提取到的内容为：" + matcher.group());
            System.out.println("提取到的内容为：" + matcher.group(1));
        }
    }
    @Test
    public void t3(){
        String s = "DLTB_2020_#{xzqdm,4},DLTB_2020_#{xzqdm,5}";
        String regex = "#\\{([^}]*)}";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(s);
        JSONObject dataMap = new JSONObject();
        String xzqdm = "440981";
        dataMap.put("xzqdm",xzqdm);
        String where = s;
        while (matcher.find()) {
            //extractedContent = matcher.group(1);
            System.out.println("提取到的内容为：" + matcher.group());
            System.out.println("提取到的内容为：" + matcher.group(1));
            String str = matcher.group(1);
            String[] strs = str.split(",");
            String value = dataMap.getString(strs[0]).substring(0,Integer.parseInt(strs[1]));
            where = where.replace(matcher.group(),value);
        }
        System.out.println(where);
    }
}
