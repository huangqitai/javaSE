package com.hqt.string;

import java.util.Date;

public class ZdNydsyq {
    private String bsm;
    private String zddm;
    private String bdcdyh;
    private String ysdm;
    private String zdtzm;
    private String zl;
    private Double zdmj;
    private String mjdw;
    private String yt;
    private String dj;
    private Double jg;
    private String qllx;
    private String qlxz;
    private String qlsdfs;
    private String rjl;
    private Double jzmd;
    private Double jzxg;
    private String zdszd;
    private String zdszn;
    private String zdszx;
    private String zdszb;
    private String zdt;
    private String tfh;
    private String djh;
    private String zt;
    private String tdsyqr;
    private Double jzmj;
    private Double jzwzdmj;
    private Double scmj;
    private Double fzmj;
    private String yxtbm;
    private String yxtbsm;
    private String dcbsm;
    private String qxdm;
    private String dcywh;
    private String ywh;
    private Date gxsj;
    private String bz;
    private String dybbh;
    private String xmguid;
    private String ydyhfl;
    private String jgdw;
    private String dah;
    private String dkdm;
    private String fj;
    private String jdh;
    private String jfh;
    private String zh;
    private String ghytmc;
    private Integer bblx;
    private String gdhth;
    private String dckjbm;

    public String getBsm() {
        return bsm;
    }

    public void setBsm(String bsm) {
        this.bsm = bsm;
    }

    public String getZddm() {
        return zddm;
    }

    public void setZddm(String zddm) {
        this.zddm = zddm;
    }

    public String getBdcdyh() {
        return bdcdyh;
    }

    public void setBdcdyh(String bdcdyh) {
        this.bdcdyh = bdcdyh;
    }

    public String getYsdm() {
        return ysdm;
    }

    public void setYsdm(String ysdm) {
        this.ysdm = ysdm;
    }

    public String getZdtzm() {
        return zdtzm;
    }

    public void setZdtzm(String zdtzm) {
        this.zdtzm = zdtzm;
    }

    public String getZl() {
        return zl;
    }

    public void setZl(String zl) {
        this.zl = zl;
    }

    public Double getZdmj() {
        return zdmj;
    }

    public void setZdmj(Double zdmj) {
        this.zdmj = zdmj;
    }

    public String getMjdw() {
        return mjdw;
    }

    public void setMjdw(String mjdw) {
        this.mjdw = mjdw;
    }

    public String getYt() {
        return yt;
    }

    public void setYt(String yt) {
        this.yt = yt;
    }

    public String getDj() {
        return dj;
    }

    public void setDj(String dj) {
        this.dj = dj;
    }

    public Double getJg() {
        return jg;
    }

    public void setJg(Double jg) {
        this.jg = jg;
    }

    public String getQllx() {
        return qllx;
    }

    public void setQllx(String qllx) {
        this.qllx = qllx;
    }

    public String getQlxz() {
        return qlxz;
    }

    public void setQlxz(String qlxz) {
        this.qlxz = qlxz;
    }

    public String getQlsdfs() {
        return qlsdfs;
    }

    public void setQlsdfs(String qlsdfs) {
        this.qlsdfs = qlsdfs;
    }

    public String getRjl() {
        return rjl;
    }

    public void setRjl(String rjl) {
        this.rjl = rjl;
    }

    public Double getJzmd() {
        return jzmd;
    }

    public void setJzmd(Double jzmd) {
        this.jzmd = jzmd;
    }

    public Double getJzxg() {
        return jzxg;
    }

    public void setJzxg(Double jzxg) {
        this.jzxg = jzxg;
    }

    public String getZdszd() {
        return zdszd;
    }

    public void setZdszd(String zdszd) {
        this.zdszd = zdszd;
    }

    public String getZdszn() {
        return zdszn;
    }

    public void setZdszn(String zdszn) {
        this.zdszn = zdszn;
    }

    public String getZdszx() {
        return zdszx;
    }

    public void setZdszx(String zdszx) {
        this.zdszx = zdszx;
    }

    public String getZdszb() {
        return zdszb;
    }

    public void setZdszb(String zdszb) {
        this.zdszb = zdszb;
    }

    public String getZdt() {
        return zdt;
    }

    public void setZdt(String zdt) {
        this.zdt = zdt;
    }

    public String getTfh() {
        return tfh;
    }

    public void setTfh(String tfh) {
        this.tfh = tfh;
    }

    public String getDjh() {
        return djh;
    }

    public void setDjh(String djh) {
        this.djh = djh;
    }

    public String getZt() {
        return zt;
    }

    public void setZt(String zt) {
        this.zt = zt;
    }

    public String getTdsyqr() {
        return tdsyqr;
    }

    public void setTdsyqr(String tdsyqr) {
        this.tdsyqr = tdsyqr;
    }

    public Double getJzmj() {
        return jzmj;
    }

    public void setJzmj(Double jzmj) {
        this.jzmj = jzmj;
    }

    public Double getJzwzdmj() {
        return jzwzdmj;
    }

    public void setJzwzdmj(Double jzwzdmj) {
        this.jzwzdmj = jzwzdmj;
    }

    public Double getScmj() {
        return scmj;
    }

    public void setScmj(Double scmj) {
        this.scmj = scmj;
    }

    public Double getFzmj() {
        return fzmj;
    }

    public void setFzmj(Double fzmj) {
        this.fzmj = fzmj;
    }

    public String getYxtbm() {
        return yxtbm;
    }

    public void setYxtbm(String yxtbm) {
        this.yxtbm = yxtbm;
    }

    public String getYxtbsm() {
        return yxtbsm;
    }

    public void setYxtbsm(String yxtbsm) {
        this.yxtbsm = yxtbsm;
    }

    public String getDcbsm() {
        return dcbsm;
    }

    public void setDcbsm(String dcbsm) {
        this.dcbsm = dcbsm;
    }

    public String getQxdm() {
        return qxdm;
    }

    public void setQxdm(String qxdm) {
        this.qxdm = qxdm;
    }

    public String getDcywh() {
        return dcywh;
    }

    public void setDcywh(String dcywh) {
        this.dcywh = dcywh;
    }

    public String getYwh() {
        return ywh;
    }

    public void setYwh(String ywh) {
        this.ywh = ywh;
    }

    public Date getGxsj() {
        return gxsj;
    }

    public void setGxsj(Date gxsj) {
        this.gxsj = gxsj;
    }

    public String getBz() {
        return bz;
    }

    public void setBz(String bz) {
        this.bz = bz;
    }

    public String getDybbh() {
        return dybbh;
    }

    public void setDybbh(String dybbh) {
        this.dybbh = dybbh;
    }

    public String getXmguid() {
        return xmguid;
    }

    public void setXmguid(String xmguid) {
        this.xmguid = xmguid;
    }

    public String getYdyhfl() {
        return ydyhfl;
    }

    public void setYdyhfl(String ydyhfl) {
        this.ydyhfl = ydyhfl;
    }

    public String getJgdw() {
        return jgdw;
    }

    public void setJgdw(String jgdw) {
        this.jgdw = jgdw;
    }

    public String getDah() {
        return dah;
    }

    public void setDah(String dah) {
        this.dah = dah;
    }

    public String getDkdm() {
        return dkdm;
    }

    public void setDkdm(String dkdm) {
        this.dkdm = dkdm;
    }

    public String getFj() {
        return fj;
    }

    public void setFj(String fj) {
        this.fj = fj;
    }

    public String getJdh() {
        return jdh;
    }

    public void setJdh(String jdh) {
        this.jdh = jdh;
    }

    public String getJfh() {
        return jfh;
    }

    public void setJfh(String jfh) {
        this.jfh = jfh;
    }

    public String getZh() {
        return zh;
    }

    public void setZh(String zh) {
        this.zh = zh;
    }

    public String getGhytmc() {
        return ghytmc;
    }

    public void setGhytmc(String ghytmc) {
        this.ghytmc = ghytmc;
    }

    public Integer getBblx() {
        return bblx;
    }

    public void setBblx(Integer bblx) {
        this.bblx = bblx;
    }

    public String getGdhth() {
        return gdhth;
    }

    public void setGdhth(String gdhth) {
        this.gdhth = gdhth;
    }

    public String getDckjbm() {
        return dckjbm;
    }

    public void setDckjbm(String dckjbm) {
        this.dckjbm = dckjbm;
    }
}
