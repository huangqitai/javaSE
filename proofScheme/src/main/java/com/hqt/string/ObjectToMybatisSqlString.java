package com.hqt.string;

import org.junit.Test;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ObjectToMybatisSqlString {

    @Test
    public void objectToMybatisUpdateSql() throws IntrospectionException {
        String mybatisSqlIf = "<if test=\"{0} != null\">{0} = #'{'{0},jdbcType={1}'}',</if>\n";
        StringBuilder mybatisSql = new StringBuilder();
        ZdNydsyq o = new ZdNydsyq();

        Class oClass = o.getClass();
        Field[] fields = oClass.getDeclaredFields();
        List<String> fieldNames = new ArrayList<>();
        for (Field field:fields){
            String fieldName = field.getName();
            fieldNames.add(fieldName);
            if ("class java.lang.String".equals(field.getType().toString())){
                mybatisSql.append(MessageFormat.format(mybatisSqlIf, fieldName, "VARCHAR"));
            }else if ("int".equals(field.getType().toString())){
                mybatisSql.append(MessageFormat.format(mybatisSqlIf, fieldName, "INTEGER"));
            }else if ("class java.lang.Integer".equals(field.getType().toString())){
                mybatisSql.append(MessageFormat.format(mybatisSqlIf, fieldName, "INTEGER"));
            }else if ("class java.util.Date".equals(field.getType().toString())){
                mybatisSql.append(MessageFormat.format(mybatisSqlIf, fieldName, "TIMESTAMP"));
            }else if ("class java.lang.Double".equals(field.getType().toString())){
                mybatisSql.append(MessageFormat.format(mybatisSqlIf, fieldName, "DOUBLE"));
            }
        }
        System.out.println(mybatisSql.toString());
    }

     @Test
    public void objectToMybatisInsertValue() throws IntrospectionException {
        String mybatisSqlIf = "#'{'{0},jdbcType={1}'}',";
        StringBuilder mybatisSql = new StringBuilder();
        ZdNydsyq o = new ZdNydsyq();

        Class oClass = o.getClass();
        Field[] fields = oClass.getDeclaredFields();
        List<String> fieldNames = new ArrayList<>();
        for (Field field:fields){
            String fieldName = field.getName();
            fieldNames.add(fieldName);
            if ("class java.lang.String".equals(field.getType().toString())){
                mybatisSql.append(MessageFormat.format(mybatisSqlIf, fieldName, "VARCHAR"));
            }else if ("int".equals(field.getType().toString())){
                mybatisSql.append(MessageFormat.format(mybatisSqlIf, fieldName, "INTEGER"));
            }else if ("class java.lang.Integer".equals(field.getType().toString())){
                mybatisSql.append(MessageFormat.format(mybatisSqlIf, fieldName, "INTEGER"));
            }else if ("class java.util.Date".equals(field.getType().toString())){
                mybatisSql.append(MessageFormat.format(mybatisSqlIf, fieldName, "TIMESTAMP"));
            }else if ("class java.lang.Double".equals(field.getType().toString())){
                mybatisSql.append(MessageFormat.format(mybatisSqlIf, fieldName, "DOUBLE"));
            }
        }
        System.out.println(mybatisSql.toString());
    }


}
