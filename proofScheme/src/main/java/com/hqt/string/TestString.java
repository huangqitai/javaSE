package com.hqt.string;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.hqt.utils.JsonUtil;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestString {
    @Test
    public void t1(){
        String jsonBody = "{\n" +
                "\t\"reqData\": \"eyJwYXJhbWV0ZXJzIjpbeyJ1c2VybmFtZSI6ICJYWFgiLCJwYXNzd29yZCI6ICJZWVkiLCJzb3VyY2UiOiAi55m76K6w5Lit5b+DIn1dLCJmb3JtcyI6IFt7ImZvcm1ObyI6ICIyMDIyMDUxOTEwMDU0NTAwMSIsInpnIjogIjIy55m76K6wIiwiYWgiOiAiMTIwMTUzNDEifV19\"\n" +
                "}";
        System.out.println("获取参数："+jsonBody);

        Map<String, String> map = JsonUtil.jsonStringToMapString(jsonBody);
        String base64ReqData = (String) map.get("reqData");
        String reqStr = new String(Base64.decodeBase64(base64ReqData));
        JSONObject jsonObject = JSONObject.parseObject(reqStr);
        //JSONObject reqData = jsonObject.getJSONObject("reqData");

        JSONArray forms = jsonObject.getJSONArray("forms");
    }
    @Test
    public void t2(){
        String json = "{\"parameters\":[{\"username\":\"GZCHDAG\",\"password\":\"CHdag@54321\",\"source\":\"税务局\"}],\"forms\":[{\"downloadNum\":\"C958905F91CC49F0A23B0C6\",\"filePath\":\"J:\\BDC\\cache\\2022\\2022-05-10\\21\\43.JPG\"}]}";
        JSONObject jsonpObject = JSON.parseObject(json);
        System.out.println(JSON.toJSONString(jsonpObject));
    }

    @Test
    public void t3(){
        String bizXml = "version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "\n" +
                "<Message>\n" +
                " <Head>\n" +
                "  <BizMsgID>440705220601002469</BizMsgID>\n" +
                "  <DigitalSign>4a691ca8d1611285dc9ea7b4b5dee9db6f655d787726f72e5349a850eb1cb60412f3296c4d3e427050a44f6a2a9784da6f56ba3660f3cf76e7786f846ba7e2dc3c7b9501c21764fcbcd156b0c069c81e4e3e90e285f342e832beafddad2be6dbaac6a1924bd8e5ab520cc8efedc2cbc630b9a428379c4c1d5e54bc0d44a9910d</DigitalSign>\n" +
                "  <ASID>AS100</ASID>\n" +
                "  <AreaCode>440705</AreaCode>\n" +
                "  <RecType>2000402</RecType>\n" +
                "  <RightType>4</RightType>\n" +
                "  <RegType>200</RegType>\n" +
                "  <CreateDate>2022-06-01T05:29:05</CreateDate>\n" +
                "  <RecFlowID>20220601-60636</RecFlowID>\n" +
                "  <RegOrgID>12440705MB2C16269H</RegOrgID>\n" +
                "  <ParcelID>440705011010GB00102</ParcelID>\n" +
                "  <EstateNum>440705011010GB00102F00010440</EstateNum>\n" +
                "  <PreEstateNum>440705011010GB00102F00010440</PreEstateNum>\n" +
                "  <PreCertID>(新会)50050500</PreCertID>\n" +
                "  <CertCount>1</CertCount>\n" +
                "  <ProofCount>0</ProofCount>\n" +
                " </Head>\n" +
                " <Data>\n" +
                "  <ZTT_GY_QLR DH=\"13542180802\" QXDM=\"440705\" SSHY=\"/\" DZYJ=\"/\" YWH=\"20220601-60636\" GYFS=\"0\" QLBL=\"100%\" QLLX=\"4\" QLRTZ=\"1\" DZ=\"江门市新会区会城东庆北路7号怡馨雅苑4座101\" BDCDYH=\"440705011010GB00102F00010440\" YSDM=\"6003000000\" ZJZL=\"1\" SXH=\"2\" GJ=\"142\" QLRMC=\"何健英\" QLRLX=\"1\" QSZT=\"1\" SFCZR=\"1\" DJSJ=\"2022-06-01T16:09:10\" BDCQZH=\"粤（2022）江门市不动产权第2026690号\" ZJH=\"440721196908060345\" GYQK=\"单独所有100%\"></ZTT_GY_QLR>\n" +
                "  <KTT_FW_ZRZ QXDM=\"440705\" YTMC=\"商服用地、城镇住宅用地\" DAH=\"G1-43-4692-A\" ZDDM=\"440705011010GB00102\" GHYT=\"11\" JGRQ=\"2015-10-01T00:00:00\" ZYDMJ=\"7054.0\" DSCS=\"17\" BDCDYH=\"440705011010GB00102F00010440\" LDZL=\"江门市新会区会城潮江路18号，冈州大道东90号之一，御龙轩1、2、3、5、6座、御龙轩车库\" BZ=\"黄颖珊20200525\" YSDM=\"6001030110\" ZRZH=\"440705011010GB00102F0001\" SCJZMJ=\"38832.33\" YCJZMJ=\"38443.0\" ZCS=\"18\" DXCS=\"1\" ZZDMJ=\"7054.0\" XMMC=\"江门市新会区会城潮江路18号，冈州大道东90号之一，御龙轩1、2、3、5、6座、御龙轩车库\" JZWMC=\"江门市新会区会城潮江路18号，冈州大道东90号之一，御龙轩1、2、3、5、6座、御龙轩车库\" ZTS=\"600\" ZT=\"0\" BSM=\"130695\" FWJG=\"3\"></KTT_FW_ZRZ>\n" +
                "  <KTT_FW_H HH=\"1096\" MJDW=\"1\" QXDM=\"440705\" SJYT=\"/\" ZDDM=\"440705011010GB00102\" YCFTXS=\"0.0\" FWYT1=\"85\" FWYT3=\"80\" FWYT2=\"80\" SCTNJZMJ=\"2.4\" BDCDYH=\"440705011010GB00102F00010440\" YCTNJZMJ=\"2.4\" SCFTJZMJ=\"2.55\" YSDM=\"6001030140\" LJZH=\"御龙轩（车库）\" FCFHT=\"存量数据\\\\8978\\\\313416.dwg\" ZRZH=\"440705011010GB00102F0001\" SHBW=\"1096＃车位\" YCFTJZMJ=\"2.55\" SCJZMJ=\"4.95\" FWXZ=\"0\" CH=\"-1\" YCJZMJ=\"4.95\" FWBM=\"0200163875\" FWLX=\"6\" ZL=\"江门市新会区会城冈州大道东90号之一御龙轩车库1096＃车位\" SJCS=\"1\" SCFTXS=\"0.0\" FWXZMC=\"市场化商品房\" ZT=\"1\" HXJG=\"1\" FWLXMC=\"车库\"></KTT_FW_H>\n" +
                "  <QLT_FW_FDCQ_YZ QXDM=\"440705\" DJYY=\"转移登记\" YWH=\"20220601-60636\" GHYT=\"85\" QLLX=\"4\" BDCDYH=\"440705011010GB00102F00010440\" FTJZMJ=\"2.55\" YSDM=\"6002010200\" YT=\"0701\" TDSYQSSJ=\"2013-11-04T00:00:00\" SZC=\"-1\" TDSYQR=\"何健英\" FDZL=\"江门市新会区会城冈州大道东90号之一御龙轩车库1096＃车位\" JZMJ=\"4.95\" SSYWH=\"QQ201511256\" FWXZ=\"0\" JGSJ=\"2015\" ZYJZMJ=\"2.4\" QSZT=\"1\" ZCS=\"18\" DJJG=\"江门市不动产登记中心新会分中心\" DJSJ=\"2022-06-01T16:09:10\" FDCJYJG=\"2.5000\" BDCQZH=\"粤（2022）江门市不动产权第2026690号\" DBR=\"陈慧玲\" TDSYJSSJ=\"2083-11-04T00:00:00\" FWXZMC=\"市场化商品房\" DJLX=\"200\" FWJG=\"3\" JEDW=\"2\"></QLT_FW_FDCQ_YZ>\n" +
                "  <DJF_DJ_SQ SQSJ=\"2022-06-01T10:30:02\" QXDM=\"440705\" QLRMC=\"何健英\" YWRMC=\"江门市新会区尚豪房地产开发有限公司\" YWH=\"20220601-60636\" YWRTXDZ=\"会城东庆北路30号113\" QLRZJZL=\"1\" YWRZJZL=\"7\" YWRZJH=\"440782000059912\" QLRTXDZ=\"江门市新会区会城东庆北路7号怡馨雅苑4座101\" QLRZJH=\"440721196908060345\" YSDM=\"6004010000\"></DJF_DJ_SQ>\n" +
                "  <DJT_DJ_SL ZL=\"江门市新会区会城冈州大道东90号之一御龙轩车库1096＃车位\" SQFBCZ=\"0\" QXDM=\"440705\" SLRY=\"吴银瑞\" SLSJ=\"2022-06-01T10:30:02\" SQZSBS=\"0\" YSDM=\"6004020000\" YWH=\"20220601-60636\" DJLX=\"200\"></DJT_DJ_SL>\n" +
                "  <DJF_DJ_SJ SFSJSY=\"0\" SFBCSJ=\"0\" SJSL=\"1\" QXDM=\"440705\" YWH=\"20220601-60636\" SJLX=\"1\" SFEWSJ=\"0\" SJMC=\"计价表\" YS=\"1\" YSDM=\"6004030000\" SJSJ=\"2022-06-01T10:30:02\"></DJF_DJ_SJ>\n" +
                "  <DJF_DJ_SJ SFSJSY=\"0\" SFBCSJ=\"0\" SJSL=\"1\" QXDM=\"440705\" YWH=\"20220601-60636\" SJLX=\"1\" SFEWSJ=\"0\" SJMC=\"不动产登记申请书\" YS=\"4\" YSDM=\"6004030000\" SJSJ=\"2022-06-01T10:30:03\"></DJF_DJ_SJ>\n" +
                "  <DJF_DJ_SJ SFSJSY=\"0\" SFBCSJ=\"0\" SJSL=\"1\" QXDM=\"440705\" YWH=\"20220601-60636\" SJLX=\"1\" SFEWSJ=\"0\" SJMC=\"申请人身份证明\" YS=\"1\" YSDM=\"6004030000\" SJSJ=\"2022-06-01T10:30:03\"></DJF_DJ_SJ>\n" +
                "  <DJF_DJ_SJ SFSJSY=\"0\" SFBCSJ=\"0\" SJSL=\"1\" QXDM=\"440705\" YWH=\"20220601-60636\" SJLX=\"1\" SFEWSJ=\"0\" SJMC=\"完税证明\" YS=\"1\" YSDM=\"6004030000\" SJSJ=\"2022-06-01T10:30:03\"></DJF_DJ_SJ>\n" +
                "  <DJF_DJ_SJ SFSJSY=\"0\" SFBCSJ=\"0\" SJSL=\"1\" QXDM=\"440705\" YWH=\"20220601-60636\" SJLX=\"1\" SFEWSJ=\"0\" SJMC=\"商品房屋权属证明书\" YS=\"20\" YSDM=\"6004030000\" SJSJ=\"2022-06-01T10:30:03\"></DJF_DJ_SJ>\n" +
                "  <DJF_DJ_SJ SFSJSY=\"0\" SFBCSJ=\"0\" SJSL=\"1\" QXDM=\"440705\" YWH=\"20220601-60636\" SJLX=\"1\" SFEWSJ=\"0\" SJMC=\"商品房买卖合同\" YS=\"1\" YSDM=\"6004030000\" SJSJ=\"2022-06-01T10:30:03\"></DJF_DJ_SJ>\n" +
                "  <DJF_DJ_SJ SFSJSY=\"0\" SFBCSJ=\"0\" SJSL=\"1\" QXDM=\"440705\" YWH=\"20220601-60636\" SJLX=\"1\" SFEWSJ=\"0\" SJMC=\"宗地图\" YSDM=\"6004030000\" SJSJ=\"2022-06-01T10:30:03\"></DJF_DJ_SJ>\n" +
                "  <DJF_DJ_SJ SFSJSY=\"0\" SFBCSJ=\"0\" SJSL=\"1\" QXDM=\"440705\" YWH=\"20220601-60636\" SJLX=\"1\" SFEWSJ=\"0\" SJMC=\"电子合同\" YSDM=\"6004030000\" SJSJ=\"2022-06-01T10:30:03\"></DJF_DJ_SJ>\n" +
                "  <DJF_DJ_SJ SFSJSY=\"0\" SFBCSJ=\"0\" SJSL=\"1\" QXDM=\"440705\" YWH=\"20220601-60636\" SJLX=\"1\" SFEWSJ=\"0\" SJMC=\"电子税票\" YSDM=\"6004030000\" SJSJ=\"2022-06-01T10:30:03\"></DJF_DJ_SJ>\n" +
                "  <DJF_DJ_SJ SFSJSY=\"0\" SFBCSJ=\"0\" SJSL=\"1\" QXDM=\"440705\" YWH=\"20220601-60636\" SJLX=\"1\" SFEWSJ=\"0\" SJMC=\"电子证照\" YSDM=\"6004030000\" SJSJ=\"2022-06-01T10:30:03\"></DJF_DJ_SJ>\n" +
                "  <DJF_DJ_SH JDMC=\"受理\" SXH=\"1\" SHYJ=\"经初步核查，申请材料内容齐全及真实有效；该不动产符合登记条件，建议予以办理国有建设用地使用权/房屋所有权-转移登记（买卖(一手房)）\" QXDM=\"440705\" CZJG=\"1\" YWH=\"20220601-60636\" YSDM=\"6004030000\" SHRYXM=\"吴银瑞\" SHKSSJ=\"2022-06-01T10:30:02\" SHJSSJ=\"2022-06-01T10:30:02\"></DJF_DJ_SH>\n" +
                "  <DJF_DJ_SH JDMC=\"初审\" SXH=\"2\" SHYJ=\"经初步核查，申请材料内容齐全及真实有效；该不动产符合登记条件，建议予以办理国有建设用地使用权/房屋所有权-转移登记（买卖(一手房)）\" QXDM=\"440705\" CZJG=\"1\" YWH=\"20220601-60636\" YSDM=\"6004030000\" SHRYXM=\"吴银瑞\" SHKSSJ=\"2022-06-01T10:30:02\" SHJSSJ=\"2022-06-01T10:30:02\"></DJF_DJ_SH>\n" +
                "  <DJF_DJ_SH JDMC=\"核定\" SXH=\"4\" SHYJ=\"同意登簿，颁发不动产权证书。\" QXDM=\"440705\" CZJG=\"1\" YWH=\"20220601-60636\" YSDM=\"6004030000\" SHRYXM=\"陈慧玲\" SHKSSJ=\"2022-06-01T16:09:10\" SHJSSJ=\"2022-06-01T16:09:10\"></DJF_DJ_SH>\n" +
                "  <DJF_DJ_SH JDMC=\"登簿\" SXH=\"5\" SHYJ=\"同意登簿，颁发不动产权证书。\" QXDM=\"440705\" CZJG=\"1\" YWH=\"20220601-60636\" YSDM=\"6004030000\" SHRYXM=\"陈慧玲\" SHKSSJ=\"2022-06-01T16:09:10\" SHJSSJ=\"2022-06-01T16:09:10\"></DJF_DJ_SH>\n" +
                "  <DJF_DJ_DB DBYJ=\"同意登簿，颁发不动产权证书。\" DBRYXM=\"陈慧玲\" QXDM=\"440705\" CZJG=\"1\" YWH=\"20220601-60636\" YSDM=\"6004040000\" DBSJ=\"2022-06-01T16:09:10\"></DJF_DJ_DB>\n" +
                " </Data>\n" +
                "</Message>";
        String extXml ="version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "\n" +
                "<Message>\n" +
                " <Head>\n" +
                "  <BizMsgID>440705220601002469</BizMsgID>\n" +
                "  <DigitalSign>ac8af0cfef6fd4a69e2ce5d79a3ea13746f285ac5854fcd61b445e8be829a2c83f96401ec586eadfcd5a5fda0a1d714e0e97ed7ce652bc4b9ecdcc8add94ab0aca76e1d3f94d0bcb38e546b46687f8d842527d3c2b1326284371d9feb168bedbfae335a4d2edca85657384eadea602751985b5121a4f7556013f093dfd7bec54</DigitalSign>\n" +
                "  <ASID>AS100</ASID>\n" +
                "  <AreaCode>440705</AreaCode>\n" +
                "  <RecType>2000402</RecType>\n" +
                "  <RightType>4</RightType>\n" +
                "  <RegType>200</RegType>\n" +
                "  <CreateDate>2022-06-01T05:29:05</CreateDate>\n" +
                "  <RecFlowID>20220601-60636</RecFlowID>\n" +
                "  <RegOrgID>12440705MB2C16269H</RegOrgID>\n" +
                "  <ParcelID>440705011010GB00102</ParcelID>\n" +
                "  <EstateNum>440705011010GB00102F00010440</EstateNum>\n" +
                "  <PreEstateNum>440705011010GB00102F00010440</PreEstateNum>\n" +
                "  <PreCertID>(新会)50050500</PreCertID>\n" +
                "  <CertCount>1</CertCount>\n" +
                "  <ProofCount>0</ProofCount>\n" +
                "  <PreRecFlowID>QQ201511256</PreRecFlowID>\n" +
                "  <CertCheck>/</CertCheck>\n" +
                "  <Projection>1</Projection>\n" +
                "  <QLID>/</QLID>\n" +
                "  <GLID>/</GLID>\n" +
                "  <PreQLID>/</PreQLID>\n" +
                " </Head>\n" +
                " <Data>\n" +
                "  <ZD_K_103 XH=\"21\" YZB=\"22.53396529\" XZB=\"113.04441849\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"5\" YZB=\"22.53405768\" XZB=\"113.04454887\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"6\" YZB=\"22.53398115\" XZB=\"113.04461212\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"7\" YZB=\"22.53397791\" XZB=\"113.04460756\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"1\" YZB=\"22.53516587\" XZB=\"113.04364779\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"2\" YZB=\"22.53499679\" XZB=\"113.04378713\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"3\" YZB=\"22.53501406\" XZB=\"113.04381034\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"4\" YZB=\"22.53408031\" XZB=\"113.04458064\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"8\" YZB=\"22.53393289\" XZB=\"113.04464455\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"9\" YZB=\"22.53393618\" XZB=\"113.04464919\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"10\" YZB=\"22.53390849\" XZB=\"113.04467201\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"11\" YZB=\"22.53390676\" XZB=\"113.04479492\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"12\" YZB=\"22.53390139\" XZB=\"113.04479485\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"13\" YZB=\"22.53390066\" XZB=\"113.04485560\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"14\" YZB=\"22.53390599\" XZB=\"113.04485568\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"15\" YZB=\"22.53390540\" XZB=\"113.04490390\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"16\" YZB=\"22.53382220\" XZB=\"113.04494390\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"17\" YZB=\"22.53375927\" XZB=\"113.04494297\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"47\" YZB=\"22.53516587\" XZB=\"113.04364779\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"46\" YZB=\"22.53492559\" XZB=\"113.04330966\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"45\" YZB=\"22.53459252\" XZB=\"113.04301509\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"44\" YZB=\"22.53458893\" XZB=\"113.04301979\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"43\" YZB=\"22.53458965\" XZB=\"113.04302043\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"42\" YZB=\"22.53453021\" XZB=\"113.04309820\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"41\" YZB=\"22.53453087\" XZB=\"113.04309880\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"40\" YZB=\"22.53450904\" XZB=\"113.04323853\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"39\" YZB=\"22.53453325\" XZB=\"113.04326032\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"38\" YZB=\"22.53453212\" XZB=\"113.04326177\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"37\" YZB=\"22.53458134\" XZB=\"113.04330600\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"36\" YZB=\"22.53454149\" XZB=\"113.04335687\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"35\" YZB=\"22.53463401\" XZB=\"113.04343939\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"18\" YZB=\"22.53376450\" XZB=\"113.04453467\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"19\" YZB=\"22.53385164\" XZB=\"113.04446258\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"20\" YZB=\"22.53391852\" XZB=\"113.04445663\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"34\" YZB=\"22.53471032\" XZB=\"113.04352423\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"33\" YZB=\"22.53479710\" XZB=\"113.04364608\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"32\" YZB=\"22.53474567\" XZB=\"113.04368847\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"31\" YZB=\"22.53474617\" XZB=\"113.04368917\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"30\" YZB=\"22.53462942\" XZB=\"113.04378571\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"29\" YZB=\"22.53442558\" XZB=\"113.04394711\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"28\" YZB=\"22.53426018\" XZB=\"113.04407724\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"27\" YZB=\"22.53399919\" XZB=\"113.04428230\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"26\" YZB=\"22.53398019\" XZB=\"113.04429723\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"25\" YZB=\"22.53401078\" XZB=\"113.04434029\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"24\" YZB=\"22.53394913\" XZB=\"113.04439105\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"23\" YZB=\"22.53396807\" XZB=\"113.04441734\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"22\" YZB=\"22.53396582\" XZB=\"113.04441924\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"41\" YZB=\"22.53390676\" XZB=\"113.04479492\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"40\" YZB=\"22.53390849\" XZB=\"113.04467201\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"39\" YZB=\"22.53393618\" XZB=\"113.04464919\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"38\" YZB=\"22.53393289\" XZB=\"113.04464455\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"37\" YZB=\"22.53397791\" XZB=\"113.04460756\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"36\" YZB=\"22.53398115\" XZB=\"113.04461212\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"31\" YZB=\"22.53516587\" XZB=\"113.04364779\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"32\" YZB=\"22.53499679\" XZB=\"113.04378713\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"33\" YZB=\"22.53501406\" XZB=\"113.04381034\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"34\" YZB=\"22.53408031\" XZB=\"113.04458064\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"35\" YZB=\"22.53405768\" XZB=\"113.04454887\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"1\" YZB=\"22.53375927\" XZB=\"113.04494297\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"2\" YZB=\"22.53376450\" XZB=\"113.04453467\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"3\" YZB=\"22.53385164\" XZB=\"113.04446258\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"4\" YZB=\"22.53391852\" XZB=\"113.04445663\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"5\" YZB=\"22.53396529\" XZB=\"113.04441849\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"6\" YZB=\"22.53396582\" XZB=\"113.04441924\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"7\" YZB=\"22.53396807\" XZB=\"113.04441734\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"8\" YZB=\"22.53394912\" XZB=\"113.04439105\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"9\" YZB=\"22.53401078\" XZB=\"113.04434029\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"10\" YZB=\"22.53398019\" XZB=\"113.04429723\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"11\" YZB=\"22.53399919\" XZB=\"113.04428230\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"12\" YZB=\"22.53426018\" XZB=\"113.04407724\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"13\" YZB=\"22.53442558\" XZB=\"113.04394711\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"14\" YZB=\"22.53462942\" XZB=\"113.04378571\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"47\" YZB=\"22.53375927\" XZB=\"113.04494297\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"15\" YZB=\"22.53474617\" XZB=\"113.04368917\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"16\" YZB=\"22.53474567\" XZB=\"113.04368847\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"17\" YZB=\"22.53479710\" XZB=\"113.04364608\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"18\" YZB=\"22.53471032\" XZB=\"113.04352423\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"19\" YZB=\"22.53463401\" XZB=\"113.04343939\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"20\" YZB=\"22.53454149\" XZB=\"113.04335687\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"21\" YZB=\"22.53458134\" XZB=\"113.04330600\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"22\" YZB=\"22.53453212\" XZB=\"113.04326177\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"23\" YZB=\"22.53453325\" XZB=\"113.04326032\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"24\" YZB=\"22.53450904\" XZB=\"113.04323853\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"25\" YZB=\"22.53453087\" XZB=\"113.04309880\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"26\" YZB=\"22.53453021\" XZB=\"113.04309820\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"27\" YZB=\"22.53458964\" XZB=\"113.04302043\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"28\" YZB=\"22.53458893\" XZB=\"113.04301979\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"29\" YZB=\"22.53459252\" XZB=\"113.04301509\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"30\" YZB=\"22.53492559\" XZB=\"113.04330966\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"46\" YZB=\"22.53382220\" XZB=\"113.04494390\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"45\" YZB=\"22.53390540\" XZB=\"113.04490390\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"44\" YZB=\"22.53390599\" XZB=\"113.04485568\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"43\" YZB=\"22.53390066\" XZB=\"113.04485560\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"42\" YZB=\"22.53390139\" XZB=\"113.04479485\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"14\" YZB=\"22.53462942\" XZB=\"113.04378571\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"45\" YZB=\"22.53390540\" XZB=\"113.04490390\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"46\" YZB=\"22.53382220\" XZB=\"113.04494390\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"47\" YZB=\"22.53375927\" XZB=\"113.04494297\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"37\" YZB=\"22.53397791\" XZB=\"113.04460756\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"36\" YZB=\"22.53398115\" XZB=\"113.04461212\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"35\" YZB=\"22.53405768\" XZB=\"113.04454887\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"34\" YZB=\"22.53408031\" XZB=\"113.04458064\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"33\" YZB=\"22.53501406\" XZB=\"113.04381034\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"32\" YZB=\"22.53499679\" XZB=\"113.04378713\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"31\" YZB=\"22.53516587\" XZB=\"113.04364779\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"30\" YZB=\"22.53492559\" XZB=\"113.04330966\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"29\" YZB=\"22.53459252\" XZB=\"113.04301509\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"28\" YZB=\"22.53458893\" XZB=\"113.04301979\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"27\" YZB=\"22.53458964\" XZB=\"113.04302043\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"26\" YZB=\"22.53453021\" XZB=\"113.04309820\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"25\" YZB=\"22.53453087\" XZB=\"113.04309880\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"24\" YZB=\"22.53450904\" XZB=\"113.04323853\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"23\" YZB=\"22.53453325\" XZB=\"113.04326032\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"22\" YZB=\"22.53453212\" XZB=\"113.04326177\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"21\" YZB=\"22.53458134\" XZB=\"113.04330600\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"20\" YZB=\"22.53454149\" XZB=\"113.04335687\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"19\" YZB=\"22.53463401\" XZB=\"113.04343939\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"18\" YZB=\"22.53471032\" XZB=\"113.04352423\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"17\" YZB=\"22.53479710\" XZB=\"113.04364608\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"16\" YZB=\"22.53474567\" XZB=\"113.04368847\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"15\" YZB=\"22.53474617\" XZB=\"113.04368917\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"44\" YZB=\"22.53390599\" XZB=\"113.04485568\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"13\" YZB=\"22.53442558\" XZB=\"113.04394711\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"12\" YZB=\"22.53426018\" XZB=\"113.04407724\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"11\" YZB=\"22.53399919\" XZB=\"113.04428230\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"10\" YZB=\"22.53398019\" XZB=\"113.04429723\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"9\" YZB=\"22.53401078\" XZB=\"113.04434029\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"8\" YZB=\"22.53394912\" XZB=\"113.04439105\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"7\" YZB=\"22.53396807\" XZB=\"113.04441734\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"6\" YZB=\"22.53396582\" XZB=\"113.04441924\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"5\" YZB=\"22.53396529\" XZB=\"113.04441849\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"4\" YZB=\"22.53391852\" XZB=\"113.04445663\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"3\" YZB=\"22.53385164\" XZB=\"113.04446258\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"2\" YZB=\"22.53376450\" XZB=\"113.04453467\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"1\" YZB=\"22.53375927\" XZB=\"113.04494297\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"38\" YZB=\"22.53393289\" XZB=\"113.04464455\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"39\" YZB=\"22.53393618\" XZB=\"113.04464919\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"40\" YZB=\"22.53390849\" XZB=\"113.04467201\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"41\" YZB=\"22.53390676\" XZB=\"113.04479492\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"42\" YZB=\"22.53390139\" XZB=\"113.04479485\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"43\" YZB=\"22.53390066\" XZB=\"113.04485560\" BDCDYH=\"440705011010GB00102F00010440\" KJLX=\"1\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <KTT_ZDJBXX QLSDFS=\"2\" MJDW=\"1\" QXDM=\"440705\" DJ=\"10\" DAH=\"C3-01-125897\" ZDDM=\"440705011010GB00102\" ZDT=\"/\" QLLX=\"4\" ZDSZD=\"间距\" ZDSZB=\"间距\" BDCDYH=\"440705011010GB00102F00010440\" YSDM=\"6001010000\" ZDTZM=\"B\" YT=\"商服用地/城镇住宅用地\" ZDSZN=\"间距\" JGDW=\"2\" ZDSZX=\"广东大光明集团有限公司用地\" GHYTMC=\"/\" QLXZ=\"102\" JFH=\"440705011010\" ZH=\"/\" JDH=\"440705011\" ZL=\"江门市新会区潮江路18号，冈州大道东90号之一,冈州大道东90号之一御龙轩1、2、3、5、6座,冈州大道东90号之一御龙轩车库\" RJL=\"-1\" TFH=\"F49G036081\" ZT=\"0\" BSM=\"204407\" ZDMJ=\"7054.0000\" DKDM=\"/\"></KTT_ZDJBXX>\n" +
                " </Data>\n" +
                "</Message>";
        String bwid = "440705220601002469";
        String businessCode = "2000402";
        String data = "["
                + "{"
                + "\"BWID\": \"" + bwid + "\","
                + "\"BIZXML\": \"" + bizXml.substring(bizXml.indexOf("<Message>"), bizXml.indexOf("</Message>") + 10).replace("\"", "'") + "\","
                + "\"EXTXML\": \"" + extXml.substring(extXml.indexOf("<Message>"), extXml.indexOf("</Message>") + 10).replace("\"", "'") + "\","
                + "\"CODE\": \"" + businessCode + "\""
                + "}"
                + "]";
        System.out.println(data);
        JSONArray jsonObject = JSONObject.parseArray(data);
        System.out.println("xxx");
    }

    @Test
    public void t4() throws UnsupportedEncodingException {
        String s = "差出差";
        String oldUnicode = new String(s.getBytes(StandardCharsets.UTF_8), StandardCharsets.UTF_8);
        System.out.println(oldUnicode );
        String s1 = new String(oldUnicode.getBytes("GBK"),"GBK");
        System.out.println(s1);
    }
    @Test
    public void t5(){
        //String s = "xxx谢谢xss";
        String s = "xxxx";
        System.out.println(isContainsChinese(s));
    }
    public boolean isContainsChinese(String str) {
        String regEx = "[\u4e00-\u9fa5]";
        Pattern pat = Pattern.compile(regEx);
        Matcher matcher = pat.matcher(str);
        boolean flg = false;
        if (matcher.find())    {
            flg = true;
        }
        return flg;
    }
    @Test
    public void t6(){
        String s ="22登记12018874号";
        System.out.println(s.substring(0,4));
        System.out.println(s.substring(4,s.length()-1));
    }
    @Test
    public void t7() throws UnsupportedEncodingException {
        String ss = "江门市蓬江区杜阮镇龙溪股份合作经济联合社农民集体;江门市蓬江区杜阮镇龙溪南塘股份合作经济社农民集体;江潮股份合作经济社农民集体;江门市蓬江区杜阮镇龙溪塘尾股份合作经济社农民集体";
        System.out.println(ss.length());
        int index = ss.substring(0, Math.min(254, ss.length())).getBytes("GB18030").length;
        System.out.println(index);
        StringBuffer s = new StringBuffer("江门市蓬江区杜阮镇龙溪股份合作经济联合社农民集体;江门市蓬江区杜阮镇龙溪南塘股份合作经济社农民集体;江潮股份合作经济社农民集体;江门市蓬江区杜阮镇龙溪塘尾股份合作经济社农民集体");
        System.out.println(s.length());
        char s1 = s.charAt(253);
        System.out.println(s1);
    }
    @Test
    public void t8(){
        String s = "我v打发发发嘎嘎嘎";
        byte[] bytes = s.getBytes();
        System.out.println(new String(bytes, StandardCharsets.UTF_8));
    }
    @Test
    public void t10(){
        System.out.println((int)((Math.random()*9+1)*100000));
        String ss = String.valueOf((int)((Math.random()*9+1)*100000));
        System.out.println(ss);
        System.out.println(System.currentTimeMillis());
    }
    @Test
    public void t9() throws InterruptedException {
        Long star = System.currentTimeMillis();
        Thread.sleep(5000);
        Long end = System.currentTimeMillis();
        System.out.println((end-star)/1000);
    }
    @Test
    public void t11(){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, -1);
        Date start = c.getTime();
        //前一天
        String dateStr= format.format(start);
        System.out.println(dateStr);
    }
    @Test
    public void t12(){
        System.out.println("440703003004JC00541".substring(12,14));
    } @Test
    public void t15() throws UnsupportedEncodingException {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("http://61.142.131.10:888/OaListenerServlet");
        builder.queryParam("businessLineId", "xxxxss");
        builder.queryParam("slideTitle", "测试短信1");
        URI uri = builder.build().encode(Charset.forName("GBK")).toUri();
        System.out.println(uri.toString());
    }
  @Test
    public void t14(){
        //System.out.println("国有（铁路用地）;江门市新会区古井镇文楼村和联第二经济合作社农民集体;江门市新会区古井镇文楼村福庆第一经济合作社、文楼村福庆第二经济合作社、文楼村三元第三经济合作社农民集体共有".getBytes(Charset.forName("GBK")).length);
        String s = "江门市新会区古井镇文楼村三元第二经济合作社农民集体;江门市新会区古井镇文楼村福庆第一经济合作社、文楼村福庆第二经济合作社、文楼村三元第三经济合作社农民集体共有;江门市新会区古井镇文楼村福庆第二经济合作社农民集体;江门市新会区古井镇文楼村三元第一经济合作社农民集体";
        System.out.println(s.substring(0, Math.min(254, s.length())).getBytes(Charset.forName("GBK")).length);
        System.out.println(s.length());
    }

    @Test
    public void t13(){
        Calendar date = Calendar.getInstance();
        String year = String.valueOf(date.get(Calendar.YEAR));
        System.out.println(year);
    }
    @Test
    public void t16(){
        String geomStr = "MULTIPOLYGON(((2513194.697 711422.022,2513196.921 711429.132,2513188.733 711431.921,2513194.697 711422.022)))";
        geomStr = geomStr.replace("MULTIPOLYGON(((","").replace(")))","");
        String[] geomStrs = geomStr.split(",");
        List<List<List<Object>>> rings = new ArrayList<>();
        List<List<Object>> zbList = new ArrayList<>();
        for (String zbStr:geomStrs){
            List<Object> zbs = new ArrayList<>();
            zbs.add(Double.valueOf(zbStr.split(" ")[0]));
            zbs.add(Double.valueOf(zbStr.split(" ")[1]));
            zbList.add(zbs);
        }
        rings.add(zbList);
        Map<String,List<List<List<Object>>>> ringsMap = new HashMap<>();
        ringsMap.put("rings",rings);
        System.out.println(JsonUtil.toJsonString(rings));
        System.out.println(JSONObject.toJSONString(ringsMap));
    }

    @Test
    public void t17() throws UnsupportedEncodingException {
        System.out.println(URLDecoder.decode("%7B%22sign%22%3A%2271c97182d9da014344db064ecbf2fa79%22%2C%22content%22%3A%22%E3%80%90%E6%B5%8B%E8%AF%95%E3%80%91%E5%B0%8A%E6%95%AC%E7%9A%84%24%7B2%7D%E6%82%A8%E5%A5%BD%2C%E6%9C%AC%E6%AC%A1%E6%82%A8%E7%9A%84%E9%AA%8C%E8%AF%81%E7%A0%81%E6%98%AF%24%7B3%7D%22%2C%22username%22%3A%22zjdgl_nynct%22%2C%22timestamp%22%3A%2220221020142326%22%7D","utf-8"));
    }

    @Test
    public void t18(){
        String extFilter = "and COALESCE(zt,'1')<>'0' and exists(select 1 from \n" +
                "(select bsm,max(dybbh::numeric)::varchar dybbh from bdcdj.h b where \n" +
                "(\n" +
                "(b.zrzbsm='${zrzbsm}' and b.zrzbbh='${zrzbbh}')\n" +
                "or (b.fdcqxmbsm='${fdcqxmbsm}' and b.fdcqxmbbh='${fdcqxmbbh}')\n" +
                ")\n" +
                "GROUP BY b.bsm)\n" +
                "b where bdcdj.h.bsm=b.bsm  and bdcdj.h.dybbh=b.dybbh)";
        System.out.println(getContentInfo(extFilter));

        System.out.println(getContentInfo("and COALESCE(zt,'1')<>'0' and exists(select 1 from "));
    }
    /**
     * 获取表达式中${}中的值
     * @param content
     * @return
     */
    public List<String> getContentInfo(String content) {
        List<String> argNames = new ArrayList<>();
        Pattern regex = Pattern.compile("\\$\\{([^}]*)\\}");
        Matcher matcher = regex.matcher(content);
        StringBuilder sql = new StringBuilder();
        while(matcher.find()) {
            argNames.add(matcher.group(1));
        }
        return argNames;
    }

    @Test
    public void t19(){
        String qxdm = null;
        if (qxdm.isEmpty()){
            System.out.println("xx");
        }
    }

    @Test
    public void t20(){
        String str = "{\n" +
                "\"code\": 0,\n" +
                "\"msg\": \"分析失败。当前宗地图形超出行政区[441802]范围。\",\n" +
                "\"result\": \"fail\",\n" +
                "\"data\": [\n" +
                "{\n" +
                "}\n" +
                "]\n" +
                "}";
        SqjgResponseVo sqjgResponseVo = JSON.parseObject(str,SqjgResponseVo.class);
        //sqjgResponseVo.setResult(new String("success"));
        String s = sqjgResponseVo.getResult();
        System.out.println(s=="fail");
    }

    @Test
    public void t21() {
        String versionStr = "-1";
        String[] versions = versionStr.split(",");
        String maxVersion = "";
        for (String version : versions) {
            int versionInt = Integer.parseInt(version);
            if (StringUtils.isBlank(maxVersion)) {
                maxVersion = versionInt + "";
            } else {
                int maxVersionInt = Integer.parseInt(maxVersion);
                if (versionInt > maxVersionInt) {
                    maxVersion = versionInt + "";
                }
            }
        }
        System.out.println(maxVersion);
    }

    @Test
    public void t22(){
        String fieldStrs = "sxxzc,xqtbzs,xqtbwcl,cjsczs,cjscwcl,nbshzs,nbshwcl,zjspzs,zjspwcl,spwczs,nhyyzs,nhyywcl,xzyszs,xzyswcl,ydsqzs,ydsqwcl,kgsqzs,kgsqwcl,yssqzs,yssqwcl,nczjdpzs,xcjsghxkz,jfysspcg,jfysspcgbl,pzsbl,xkzbl,zjdbjbl,zszxsqzs,zszxsqwcl,zszxnbcszs,zszxnbcswcl,zszxnbfszs,zszxnbfswcl,zszxnbzszs,zszxnbzswcl";
        String[] fields = fieldStrs.split(",");
        for (String field:fields){
            System.out.println(field+"+=cgtjZjdspDO.get"+field.substring(0, 1).toUpperCase() + field.substring(1)+"();" );
        }
    }
    @Test
    public void t23(){
        Float a = 2.0f;
        System.out.println(a.intValue());
    }
}
