package com.hqt.Date;

import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class TestDate {

    @Test
    public void t1() throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String beginTime = "2022-05-05";
        String endTime = "2022-08-01";
        Date beginDate = sdf.parse(beginTime);
        Date endDate = sdf.parse(endTime);

        List<Date> dateList = getDateByTimeRange(beginDate, endDate);
        // 输出打印
        List<String> dateStrList = dateList.stream().map((item)->sdf.format(item)).collect(Collectors.toList());
        System.out.println(dateStrList);
    }
    public static List<Date> getDateByTimeRange(Date beginDate, Date endDate) {
        List dateList = new ArrayList();
        dateList.add(beginDate);
        Calendar beginCalendar = Calendar.getInstance();
        // 使用给定的 Date 设置此 Calendar 的时间
        beginCalendar.setTime(beginDate);
        Calendar endCalendar = Calendar.getInstance();
        // 使用给定的 Date 设置此 Calendar 的时间
        endCalendar.setTime(endDate);
        // 测试此日期是否在指定日期之后
        while (endDate.after(beginCalendar.getTime())) {
            // 根据日历的规则,为给定的日历字段添加或减去指定的时间量
            beginCalendar.add(Calendar.DAY_OF_MONTH, 1);
            dateList.add(beginCalendar.getTime());
        }
        return dateList;
    }
    @Test
    public void t3() throws ParseException {
        Date bldate = new SimpleDateFormat("yyyy-MM-dd").parse("2017-07-01");
        Date date = new Date();
        System.out.println(bldate.before(date));
    }

    @Test
    public void t4() throws Exception {
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH)+1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        System.out.println(year+" "+month+" "+day);
        System.out.println(yearToUpper(year)+"年"+numToUpper(month)+"月"+numToUpper(day)+"日");
    }

    // 将数字转化为大写(年份)
    public String yearToUpper(int num) {
        String[] u = {"〇","一","二","三","四","五","六","七","八","九"};
        char[] str = String.valueOf(num).toCharArray();
        StringBuilder rstr = new StringBuilder();
        for (char c : str) {
            rstr.append(u[Integer.parseInt(c + "")]);
        }
        return rstr.toString();
    }
    // 将数字转化为大写(月份、天)
    public String numToUpper(int num) throws Exception {
        String s;
        switch (num){
            case 1:s="一";break;
            case 2:s="二";break;
            case 3:s="三";break;
            case 4:s="四";break;
            case 5:s="五";break;
            case 6:s="六";break;
            case 7:s="七";break;
            case 8:s="八";break;
            case 9:s="九";break;
            case 10:s="十";break;
            case 11:s="十一";break;
            case 12:s="十二";break;
            case 13:s="十三";break;
            case 14:s="十四";break;
            case 15:s="十五";break;
            case 16:s="十六";break;
            case 17:s="十七";break;
            case 18:s="十八";break;
            case 19:s="十九";break;
            case 20:s="二十";break;
            case 21:s="二十一";break;
            case 22:s="二十二";break;
            case 23:s="二十三";break;
            case 24:s="二十四";break;
            case 25:s="二十五";break;
            case 26:s="二十六";break;
            case 27:s="二十七";break;
            case 28:s="二十八";break;
            case 29:s="二十九";break;
            case 30:s="三十";break;
            case 31:s="三十一";break;
            default:throw new Exception("日期转换异常，超出月份或者天数");
        }
        return s;
    }
}
