package com.hqt.json;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;

/**
 * 顺德短信日志实体
 */
public class SdSmsLog {
    /**
     * mType	短信服务号
     * mPassword	短信服务密码
     * phones	短信接收手机
     * content	短信内容
     * sendUserNam	发送用户名称
     * sendUserUuid	发送用户ID
     * sendPhone	发送用户手机
     * sendDepUuid	发送用户部门ID
     * sendDepName	发送用户部门名称
     * relateDocUuid	业务查询关键字
     */
    private String bsm;

    private String mType;

    private String mPassword;

    private String phones;

    private String content;

    private String sendUserName;

    private String sendUserUuid;

    private String sendPhone;

    private String sendDepUuid;

    private String sendDepName;

    private String relateDocUuid;

    private Date sendTime;
    private String url;
    private String responseStr;
    //发送状态 0 失败  1成功
    private Integer state;

    //获取短信状态
    @JsonIgnore
    private String queryUrl;
    @JsonIgnore
    private String queryResponseJson;
    @JsonIgnore
    private String queryResponseXml;

    public String getBsm() {
        return bsm;
    }

    public void setBsm(String bsm) {
        this.bsm = bsm;
    }

    public String getmType() {
        return mType;
    }

    public void setmType(String mType) {
        this.mType = mType;
    }

    public String getmPassword() {
        return mPassword;
    }

    public void setmPassword(String mPassword) {
        this.mPassword = mPassword;
    }

    public String getPhones() {
        return phones;
    }

    public void setPhones(String phones) {
        this.phones = phones;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSendUserName() {
        return sendUserName;
    }

    public void setSendUserName(String sendUserName) {
        this.sendUserName = sendUserName;
    }

    public String getSendUserUuid() {
        return sendUserUuid;
    }

    public void setSendUserUuid(String sendUserUuid) {
        this.sendUserUuid = sendUserUuid;
    }

    public String getSendPhone() {
        return sendPhone;
    }

    public void setSendPhone(String sendPhone) {
        this.sendPhone = sendPhone;
    }

    public String getSendDepUuid() {
        return sendDepUuid;
    }

    public void setSendDepUuid(String sendDepUuid) {
        this.sendDepUuid = sendDepUuid;
    }

    public String getSendDepName() {
        return sendDepName;
    }

    public void setSendDepName(String sendDepName) {
        this.sendDepName = sendDepName;
    }

    public String getRelateDocUuid() {
        return relateDocUuid;
    }

    public void setRelateDocUuid(String relateDocUuid) {
        this.relateDocUuid = relateDocUuid;
    }

    public Date getSendTime() {
        return sendTime;
    }

    public void setSendTime(Date sendTime) {
        this.sendTime = sendTime;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getResponseStr() {
        return responseStr;
    }

    public void setResponseStr(String responseStr) {
        this.responseStr = responseStr;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getQueryUrl() {
        return queryUrl;
    }

    public void setQueryUrl(String queryUrl) {
        this.queryUrl = queryUrl;
    }

    public String getQueryResponseJson() {
        return queryResponseJson;
    }

    public void setQueryResponseJson(String queryResponseJson) {
        this.queryResponseJson = queryResponseJson;
    }

    public String getQueryResponseXml() {
        return queryResponseXml;
    }

    public void setQueryResponseXml(String queryResponseXml) {
        this.queryResponseXml = queryResponseXml;
    }
}
