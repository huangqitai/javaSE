package com.hqt.json;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hqt.utils.JsonUtil;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class JsonTest {
    @Test
    public void t1() throws UnsupportedEncodingException {
        SdSmsLog smsLog = new SdSmsLog();
        String bsm = UUID.randomUUID().toString().replace("-","");
        smsLog.setBsm(bsm);
        String smsUrl = "xxx";
        smsLog.setmType("122");
        smsLog.setmPassword("25333");
        smsLog.setRelateDocUuid(bsm);
        smsLog.setContent("收藏收藏");
        Map<String,String> paramMap = JsonUtil.jsonStringToObject(JsonUtil.toJsonString(smsLog), new TypeReference<Map<String, String>>(){});
        List<String> paramList = new ArrayList<>();
        for (String key: paramMap.keySet()){
            if (StringUtils.isBlank(paramMap.get(key))){
                continue;
            }
            paramList.add(key + "=" + new String(paramMap.get(key).getBytes("GBK"),"GBK"));
        }
        String paramStr = String.join("&",paramList);
        String url = smsUrl+"?"+paramStr;
    }
}
