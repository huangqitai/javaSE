package com.hqt.wzt;

public class ColumnInfo {
    private String xh;
    private String zdmc;
    private String zddm;
    private String zdlx;
    private String zdcd;
    private String xsws;
    private String zy;
    private String ystj;

    public String getXh() {
        return xh;
    }

    public void setXh(String xh) {
        this.xh = xh;
    }

    public String getZdmc() {
        return zdmc;
    }

    public void setZdmc(String zdmc) {
        this.zdmc = zdmc;
    }

    public String getZddm() {
        return zddm;
    }

    public void setZddm(String zddm) {
        this.zddm = zddm;
    }

    public String getZdlx() {
        return zdlx;
    }

    public void setZdlx(String zdlx) {
        this.zdlx = zdlx;
    }

    public String getZdcd() {
        return zdcd;
    }

    public void setZdcd(String zdcd) {
        this.zdcd = zdcd;
    }

    public String getXsws() {
        return xsws;
    }

    public void setXsws(String xsws) {
        this.xsws = xsws;
    }

    public String getZy() {
        return zy;
    }

    public void setZy(String zy) {
        this.zy = zy;
    }

    public String getYstj() {
        return ystj;
    }

    public void setYstj(String ystj) {
        this.ystj = ystj;
    }
}
