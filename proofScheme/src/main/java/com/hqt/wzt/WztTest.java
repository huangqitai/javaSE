package com.hqt.wzt;

import cn.hutool.core.io.file.FileWriter;
import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.io.File;
import java.util.List;
import java.util.Map;

public class WztTest {
    @Test
    public void t1(){
        File file = new File("D:\\臻善科技资源收藏\\工作文档20230130\\字段表.xlsx");
        File txtfile = new File("D:\\臻善科技资源收藏\\工作文档20230130\\sql.txt");
        ExcelReader reader = ExcelUtil.getReader(file);
        List<ColumnInfo> readAll = reader.readAll(ColumnInfo.class);
        //List<Map<String,Object>> readAll = reader.readAll();
        String sqlStr = "INSERT INTO \"jghj\".\"jghj_b_zd_pz\"(\"xh\",\"jdmc\", \"zdmc\",\"zddm\", \"sfbt\", \"sfmj\", \"zdcd\", \"zdlx\",\"xsdws\") " +
                "VALUES (%s,'%s', '%s','%s', '%s', '%s', %s, '%s',%s);\n";
        String jdmc = "YDJFYS";
        StringBuilder sql = new StringBuilder();
        for (ColumnInfo columnInfo:readAll){
            String sfbt = "0",sfmj = "0",xsws = "null";
            if ("M".equals(columnInfo.getYstj())){
                sfbt = "1";
            }
            if (columnInfo.getZy().contains("见表")){
                sfmj = "1";
            }
            if (!StringUtils.isBlank(columnInfo.getXsws())){
                xsws = columnInfo.getXsws();
            }
            if (StringUtils.isBlank(columnInfo.getZdcd())){
                columnInfo.setZdcd("null");
            }
            else if ("-".equals(columnInfo.getZdcd())){
                columnInfo.setZdcd("2");
            }
            sql.append(String.format(sqlStr, columnInfo.getXh(), jdmc, columnInfo.getZdmc(), columnInfo.getZddm(), sfbt, sfmj,
                     columnInfo.getZdcd(), columnInfo.getZdlx(), xsws));
        }
        FileWriter writer = new FileWriter(txtfile);
        writer.write(sql.toString());
        System.out.println("xxx");
    }
}
