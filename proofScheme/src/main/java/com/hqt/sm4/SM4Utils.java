package com.hqt.sm4;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.security.SecureRandom;
import java.security.Security;
import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 	 说	明:SM4的对称加解密工具类，支持ECB
 *
 * @author
 *
 *
 * @version
 *
 */
public class SM4Utils {


    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    private static final String ENCODING = "UTF-8";
    public static final String ALGORITHM_NAME = "SM4";
    public static final String SECURE_LOG_KEY = "JQOh0JRc1P+X8M/Cn7kPAw==";//审计日志加密密钥,不可随意更改

    public static final String SECURE_PHONEANDCERTIFICATECODE_KEY = "ctaxBfe1VpoS54JouDoleA==";//用户手机号证件号加密密钥,不可随意更改
    /**
     * 加密算法/分组加密模式/分组填充方式
     * PKCS5Padding-以8个字节为一组进行分组加密
     * 定义分组加密模式使用：PKCS5Padding
     */
    public static final String ALGORITHM_NAME_ECB_PADDING = "SM4/ECB/PKCS5Padding";
    /**
     * 128-32位16进制；256-64位16进制
     */
    public static final int DEFAULT_KEY_SIZE = 128;

    /**
     * 生成ECB暗号
     *
     * @param algorithmName 算法名称
     * @param mode          模式
     * @param key
     * @return
     * @throws Exception
     * @explain ECB模式（电子密码本模式：Electronic codebook）
     */
    private static Cipher generateEcbCipher(String algorithmName, int mode, byte[] key) {
        try {
            Cipher cipher = Cipher.getInstance(algorithmName, BouncyCastleProvider.PROVIDER_NAME);
            Key sm4Key = new SecretKeySpec(key, ALGORITHM_NAME);
            cipher.init(mode, sm4Key);
            return cipher;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }


    /* 自动生成密钥
     * @explain
     * @return
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     */
    public static String generateKey() {
        return generateKey(DEFAULT_KEY_SIZE);
    }

    /**
     * @param keySize
     * @return
     * @throws Exception
     * @explain
     */
    public static String generateKey(int keySize) {
        try {
            KeyGenerator kg = KeyGenerator.getInstance(ALGORITHM_NAME, BouncyCastleProvider.PROVIDER_NAME);
            kg.init(keySize, new SecureRandom());
            return byte2Base64(kg.generateKey().getEncoded());
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

    }


    /**
     * sm4加密
     *
     * @param paramStr 待加密字符串
     * @param hexKey   16进制密钥（忽略大小写）
     * @return 返回16进制的加密字符串
     * @throws Exception
     * @explain 加密模式：ECB
     * 密文长度不固定，会随着被加密字符串长度的变化而变化
     */
    public static String encryptEcb(String paramStr, String hexKey) {
        try {
            String cipherText = "";
            // 16进制字符串--&gt;byte[]
            //byte[] keyData = ByteUtils.fromHexString(hexKey);
            byte[] keyData = base642Byte(hexKey);
            // String--&gt;byte[]
            byte[] srcData = paramStr.getBytes(ENCODING);
            // 加密后的数组
            byte[] cipherArray = encrypt_Ecb_Padding(keyData, srcData);
            // byte[]--&gt;hexString
            //cipherText = ByteUtils.toHexString(cipherArray);
            cipherText = byte2Base64(cipherArray);
            return cipherText;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * 加密模式之Ecb
     *
     * @param key
     * @param data
     * @return
     * @throws Exception
     * @explain
     */
    public static byte[] encrypt_Ecb_Padding(byte[] key, byte[] data) {
        try {
            Cipher cipher = generateEcbCipher(ALGORITHM_NAME_ECB_PADDING, Cipher.ENCRYPT_MODE, key);
            return cipher.doFinal(data);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }


    /**
     * sm4解密
     *
     * @param cipherText 16进制的加密字符串（忽略大小写）
     * @param hexKey     16进制密钥
     * @return 解密后的字符串
     * @throws Exception
     * @explain 解密模式：采用ECB
     */
    public static String decryptEcb(String cipherText, String hexKey) {
        try {
            // 用于接收解密后的字符串
            String decryptStr = "";
            // hexString--&gt;byte[]
            //byte[] keyData = ByteUtils.fromHexString(hexKey);
            byte[] keyData = base642Byte(hexKey);
            // hexString--&gt;byte[]
            //byte[] cipherData = ByteUtils.fromHexString(cipherText);
            byte[] cipherData = base642Byte(cipherText);
            // 解密
            byte[] srcData = decrypt_Ecb_Padding(keyData, cipherData);
            // byte[]--&gt;String
            decryptStr = new String(srcData, ENCODING);
            return decryptStr;
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * 解密
     *
     * @param key
     * @param cipherText
     * @return
     * @throws Exception
     * @explain
     */
    public static byte[] decrypt_Ecb_Padding(byte[] key, byte[] cipherText) throws Exception {
        Cipher cipher = generateEcbCipher(ALGORITHM_NAME_ECB_PADDING, Cipher.DECRYPT_MODE, key);
        return cipher.doFinal(cipherText);
    }


    /**
     * Base64解码数据
     *
     * @param key 需要解码的字符串
     * @return 字节数组
     */
    public static byte[] base642Byte(String key) {
        //return javax.xml.bind.DatatypeConverter.parseBase64Binary(key);
        return Base64.getDecoder().decode(key);

    }

    /**
     * Base64编码加密数据
     *
     * @param key 需要编码的字节数组
     * @return 字符串
     * @throws Exception
     */
    public static String byte2Base64(byte[] key) {
        //return javax.xml.bind.DatatypeConverter.printBase64Binary(key);
        return Base64.getEncoder().encodeToString(key);
    }


    public static void main(String[] args) {

        String key = generateKey();
        System.out.println(key);
        String s = "{\n" +
                "\t\"resultCode\": 0,\n" +
                "\t\"success\": true,\n" +
                "\t\"resultMsg\": \"OK\",\n" +
                "\t\"data\": [{\n" +
                "\t\t\"qlrlist\": [{\n" +
                "\t\t   \"qlrmc\":\"权利人名称\",\n" +
                "\t\t   \"zjzl\":\"证件种类\",\n" +
                "\t\t   \"zjh\":\"证件号\",\n" +
                "\t\t   \"xb\":\"性别\",\n" +
                "\t\t   \"gddh\":\"固定电话\",\n" +
                "\t\t   \"yddh\":\"移动电话\",\n" +
                "\t\t   \"dz\":\"地址\",\n" +
                "\t\t   \"qlrlx\":\"权利人类型\",\n" +
                "\t\t   \"qlmj\":\"权利面积\",\n" +
                "\t\t   \"qlbl\":\"权利比例\",\n" +
                "\t\t   \"gyfs\":\"共有方式\",\n" +
                "\t\t   \"gyqk\":\"共有情况\",\n" +
                "\t\t   \"qlrfrmc\":\"权利人法人名称\",\n" +
                "\t\t   \"qlrdlrmc\":\"权利人代理人名称\"\n" +
                "\t\t}],\n" +
                "\t\t\"qlr\": \"杭州市下城区财政税务局\",\n" +
                "\t\t\"bdcdyh\": \"330103001015GB00043F00190029\",\n" +
                "\t\t\"bdcqzh\": \"杭房权证移字第11311号\",\n" +
                "\t\t\"zl\": \"朝晖六区38幢1单元501室\",\n" +
                "\t\t\"qlxz\": \"/其他\",\n" +
                "\t\t\"qllx\": \"房屋（构筑物）所有权\",\n" +
                "\t\t\"gyqk\": \"单独所有\",\n" +
                "\t\t\"zjh\": \"9********\",\n" +
                "\t\t\"zjzl\": \"组织机构代码\",\n" +
                "\t\t\"mj\": \"70.94\",\n" +
                "\t\t\"yt\": \"非住宅\",\n" +
                "\t\t\"dybsm\": \"2196140\",\n" +
                "\t\t\"dybm\": \"H\",\n" +
                "\t\t\"qlbm\": \"FDCQ\",\n" +
                "\t\t\"qlbsm\": \"5776066\",\n" +
                "\t\t\"qxdm\": 330103,\n" +
                "\t\t\"zt\": \"现状\",\n" +
                "\t\t\"zszt\": \"办理中(3308-20190921-0003758)\",\n" +
                "\t\t\"dbsj\": \"1995年08月09日\",\n" +
                "\t\t\"hh\": \"501\",\n" +
                "\t\t\"ch\": \"5\",\n" +
                "\t\t\"szc\": \"5\",\n" +
                "\t\t\"zrzh\": \"38\",\n" +
                "\t\t\"bdclx\": \"土地和房屋等建筑物\",\n" +
                "\t\t\"qtdjbm\": null,\n" +
                "\t\t\"qtdjbsm\": null,\n" +
                "\t\t\"dsId\": \"数据源id\",\n" +
                "\t\t\"dsName\": \"市本级\",\n" +
                "\t\t\"restrict\": true\n" +
                "\t}]\n" +
                "}";
        /*System.out.println("加密: " + System.currentTimeMillis());
        String e = encryptEcb(s, key);
        System.out.println("加密: " + System.currentTimeMillis());
        System.out.println(e);
        System.out.println("解密: " + System.currentTimeMillis());
        String s1 = decryptEcb(e, key);
        System.out.println("解密: " + System.currentTimeMillis());
        System.out.println(s1);*/

        String plane = "1886737";
        String encryptEcb = encryptEcb(plane, SECURE_LOG_KEY);
        System.out.println(encryptEcb);//2UNNiPi6DKaVT0JZBk5W7g==
        String decryptEcb = decryptEcb(encryptEcb, SECURE_LOG_KEY);
        System.out.println("en = "+encryptEcb+",de = "+decryptEcb);

        String tar = "nihao,aK+wiYW2YraEFElMQJLnFA==abdscx";
//        String regex = "[\\w\\W]{22}==";
//        String regex = "[\\s\\S]{22}==";
        String regex = "[\\d\\D]{22}==";
        Pattern compile = Pattern.compile(regex);
        Matcher matcher = compile.matcher(tar);
        while (matcher.find()){
            System.out.println("res = "+matcher.group());
        }


    }
}
