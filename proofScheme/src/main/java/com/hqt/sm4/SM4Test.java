package com.hqt.sm4;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.crypto.SmUtil;
import cn.hutool.crypto.symmetric.SymmetricCrypto;
import org.junit.Test;

public class SM4Test {
    @Test
    public void t1() throws Exception {
        String content = "jmgly";
        // key必须是16位
        String key="WatA4pb7TVJJsNLy";
        //byte[] keybytes = Sm4Utils.generateKey();
        SymmetricCrypto sm4 = SmUtil.sm4(key.getBytes());

        String encryptHex = sm4.encryptHex(content);
        String decryptStr = sm4.decryptStr(encryptHex, CharsetUtil.CHARSET_UTF_8);
        System.out.println(encryptHex);
        System.out.println(decryptStr);
    }

    @Test
    public void t2(){
        String key = "WatA4pb7TVJJsNLyr3pI1w==";
        String plainText = "jmgly";
        String encryptEcb = SM4Utils.encryptEcb(plainText, key);
        System.out.println(encryptEcb);//2UNNiPi6DKaVT0JZBk5W7g==
        String decryptEcb = SM4Utils.decryptEcb(encryptEcb, key);
        System.out.println("en = "+encryptEcb+",de = "+decryptEcb);
    }

    @Test
    public void t3(){
        String key = SM4Utils.SECURE_PHONEANDCERTIFICATECODE_KEY;
        String plainText = "18396910839";
        String encryptEcb = SM4Utils.encryptEcb(plainText, key);
        System.out.println(encryptEcb);//2UNNiPi6DKaVT0JZBk5W7g==
        String decryptEcb = SM4Utils.decryptEcb(encryptEcb, key);
        System.out.println("en = "+encryptEcb+",de = "+decryptEcb);
    }
}
