package com.hqt.list;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ListTest {

    @Test
    public void t1(){
        List<String> list = new ArrayList<>();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        list.add("6");
        list.add("7");
        List<List<String>> groupList = groupList(list,5);
        System.out.println(groupList.size());
    }
    public List<List<String>> groupList(List<String> list,int size) {
        List<List<String>> listGroup = new ArrayList<List<String>>();
        int listSize = list.size();
        //子集合的长度，比如 500
        int toIndex = size;
        for (int i = 0; i < list.size(); i += size) {
            if (i + size > listSize) {
                toIndex = listSize - i;
            }
            List<String> newList = list.subList(i, i + toIndex);
            listGroup.add(newList);
        }
        return listGroup;
    }

    @Test
    public void t2(){
        List<String> list = new ArrayList<>();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        list.add("6");
        list.add("7");
        list.forEach(t->{
            System.out.println(1/0);
            if ("1".equals(t)){
                try {
                    throw new Exception("测试异常");
                } catch (Exception e) {
                    return;
                }
            }
            System.out.println(t);
        });
    }
}
