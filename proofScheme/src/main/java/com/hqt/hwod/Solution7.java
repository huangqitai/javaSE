package com.hqt.hwod;

import org.junit.Test;

/**
 * 给定一个包含非负整数的 m x n 网格 grid ，请找出一条从左上角到右下角的路径，使得路径上的数字总和为最小。
 * 说明：每次只能向下或者向右移动一步。
 */
public class Solution7 {
    @Test
    public void t1(){
        int[][] grid = new int[][]{
                {1,3,1}, {1,5,1},{4,2,1}
        };
        System.out.println(minPathSum(grid));
    }
    public int minPathSum(int[][] grid) {
        int m = grid.length;
        if (m==0){
            return 0;
        }
        int n = grid[0].length;
        if (n==0){
            return 0;
        }
        int[][] minSum = new int[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (i==0&&j==0){
                    minSum[i][j] = grid[i][j];
                }
                if (i==0&&j>0){
                    minSum[i][j] = minSum[i][j-1]+grid[i][j];
                }
                if (i>0&&j==0){
                    minSum[i][j] = minSum[i-1][j]+grid[i][j];
                }
                if (i>0&&j>0){
                    minSum[i][j] = Math.min(minSum[i-1][j],minSum[i][j-1])+grid[i][j];
                }
            }
        }
        return minSum[m-1][n-1];
    }
}
