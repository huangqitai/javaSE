package com.hqt.hwod;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * 描述
 * 题目描述
 * 若两个正整数的和为素数，则这两个正整数称之为“素数伴侣”，
 * 如2和5、6和13，它们能应用于通信加密。现在密码学会请你设计一个程序，
 * 从已有的 N （ N 为偶数）个正整数中挑选出若干对组成“素数伴侣”，
 * 挑选方案多种多样，例如有4个正整数：2，5，6，13，如果将5和6分为一组中只能得到一组“素数伴侣”，
 * 而将2和5、6和13编组将得到两组“素数伴侣”，能组成“素数伴侣”最多的方案称为“最佳方案”，
 * 当然密码学会希望你寻找出“最佳方案”。
 *
 * 输入:
 * 有一个正偶数 n ，表示待挑选的自然数的个数。后面给出 n 个具体的数字。
 * 输出:
 * 输出一个整数 K ，表示你求得的“最佳方案”组成“素数伴侣”的对数。
 * 数据范围： 1≤n≤100  ，输入的数据大小满足 2≤val≤30000
 * 输入描述：
 * 输入说明
 * 1 输入一个正偶数 n
 * 2 输入 n 个整数
 *
 * 输出描述：
 * 求得的“最佳方案”组成“素数伴侣”的对数。
 * 示例1
 * 输入：
 * 4
 * 2 5 6 13
 * 复制
 * 输出：
 * 2
 */
public class Solution15 {
    //素数：除了1和本身，不能被其它数整除
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNext()) {
            int n = in.nextInt();
            int[] nums = new int[n];
            //只有奇数才有可能是素数，所以吧奇数分开，与偶数匹配
            List<Integer> jnums = new ArrayList<>();
            List<Integer> onums = new ArrayList<>();
            for (int i = 0; i < n; i++) {
                nums[i] = in.nextInt();
                if (nums[i]%2==0){
                    onums.add(nums[i]);
                }else {
                    jnums.add(nums[i]);
                }
            }
            int sum = 0;
            //对于偶数来说，已经匹配的奇数伴侣数组，记录已经匹配的奇数
            int[] jnumValues = new int[onums.size()];
            for (int jnum:jnums){
                //假定对于一个新的奇数来说，所有偶数都可以去尝试匹配
                int[] onumssatus = new int[onums.size()];
                if (fun(jnum,onums,jnumValues,onumssatus)){
                    sum++;
                }
            }
            System.out.println(sum);
        }
    }
    //判断某个奇数是否能匹配到偶数伴侣
    private static boolean fun(int jnum,List<Integer> onums,int[] jnumValues,int[] onumssatus){
        //遍历偶数，寻找当前奇数的伴侣
        for (int i = 0; i < onums.size(); i++) {
            //寻找到了伴侣，并且此伴侣是还没有被使用的
            if (fun1(jnum+onums.get(i))&&onumssatus[i]==0){
                //设定这个偶数被使用了
                onumssatus[i]=1;
                //1、选中的偶数之前没有匹配过奇数伴侣
                //2、或者匹配过的奇数伴侣可以找到其它没有使用过的偶数伴侣
                if (jnumValues[i]==0||fun(jnumValues[i], onums, jnumValues, onumssatus)){
                    //记录当前偶数匹配的奇数伴侣
                    jnumValues[i] = jnum;
                    return true;
                }
            }
        }
        //没有找到伴侣或者伴侣已经被其它奇数找到了，
        return false;
    }
    private static boolean fun1(int num){
        if (num==2||num==3||num==5||num==7){
            return true;
        }
        if (num%2==0||num%3==0||num%5==0||num%7==0){
            return false;
        }
        int k = 1;
        int numk = 0;
        while (num>20&&numk<num/2){
            numk = 10*k+1;
            if (num%numk==0){
                return false;
            }
            numk = 10*k+3;
            if (num%numk==0){
                return false;
            }
            numk = 10*k+7;
            if (num%numk==0){
                return false;
            }
            numk = 10*k+9;
            if (num%numk==0){
                return false;
            }
            k++;
        }
        return true;
    }
}
