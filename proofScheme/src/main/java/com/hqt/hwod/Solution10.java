package com.hqt.hwod;

import org.junit.Test;

/**
 * 4. 寻找两个正序数组的中位数
 * 给定两个大小分别为 m 和 n 的正序（从小到大）数组 nums1 和 nums2。请你找出并返回这两个正序数组的 中位数 。
 * 算法的时间复杂度应该为 O(log (m+n)) 。
 * 示例 1：
 * 输入：nums1 = [1,3], nums2 = [2]
 * 输出：2.00000
 * 解释：合并数组 = [1,2,3] ，中位数 2
 */
public class Solution10 {
    @Test
    public void t1(){
        int[] nums1 = new int[]{1,3};
        int[] nums2 = new int[]{2,7};
        System.out.println(findMedianSortedArrays(nums1,nums2));
    }
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int nums1Length = nums1.length;
        int nums2Length = nums2.length;
        if (nums1Length==0&&nums2Length==0){
            return 0;
        }else if (nums1Length==1&&nums2Length==0){
            return nums1[0];
        }else if (nums2Length==1&&nums1Length==0){
            return nums2[0];
        }

        if (nums1Length==0){
            return fun(nums2);
        }
        if (nums2Length==0){
            return fun(nums1);
        }
        int newNumsLength = nums1Length+nums2Length;
        int[] newNums = new int[newNumsLength];
        int nums1Index = 0;
        int nums2Index = 0;
        for (int i = 0; i < newNumsLength; i++) {
            if (nums1Index==nums1Length&&nums2Index!=nums2Length){
                newNums[i] = nums2[nums2Index];
                nums2Index++;
                continue;
            }
            if (nums2Index==nums2Length&&nums1Index!=nums1Length){
                newNums[i] = nums1[nums1Index];
                nums1Index++;
                continue;
            }
            if (nums1[nums1Index]>nums2[nums2Index]){
                newNums[i] = nums2[nums2Index];
                nums2Index++;
            }else {
                newNums[i] = nums1[nums1Index];
                nums1Index++;
            }
        }
        return fun(newNums);
    }
    private double fun(int[] nums){
        int numsLength = nums.length;
        double numsMedian;
        if (numsLength==0){
            numsMedian = 0;
        }else if (numsLength==1){
            numsMedian = nums[0];
        }
        else if (numsLength%2==0){
            numsMedian = (nums[(numsLength-1)/2]+nums[(numsLength-1)/2+1])/2.0;
        }else {
            numsMedian = nums[(numsLength-1)/2];
        }
        return numsMedian;
    }
}
