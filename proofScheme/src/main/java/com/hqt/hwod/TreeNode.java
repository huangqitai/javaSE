package com.hqt.hwod;

public class TreeNode {
    String val = "";
    TreeNode left = null;
    TreeNode right = null;
    TreeNode parent = null;

    public TreeNode(String val) {
        this.val = val;
    }
}
