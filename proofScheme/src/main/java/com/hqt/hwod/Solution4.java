package com.hqt.hwod;

import org.junit.Test;

/**
 * MELON有一堆精美的雨花石（数量为n，重量各异），准备送给S和W。
 * MELON希望送给俩人的雨花石重量一致，请你设计一个程序，帮MELON确认是否能将雨花石平均分配。
 * 假定重量都是整数
 */
public class Solution4 {
    boolean flag = false;
    @Test
    public void t1(){
        int[] zl = new int[]{6,5,8,9,2,2};
        System.out.println(svg(zl));
    }
    public boolean svg(int[] zl){
        int n = zl.length;
        int sum = 0;
        for (int i = 0; i < n; i++) {
            sum+=zl[i];
        }
        if (sum%2!=0){
            return false;
        }
        int svgzl = sum/2;
        fun(zl,svgzl,0,0,"");
        return flag;
    }
    private void fun(int[] zl,int svgzl,int index,int currentSum,String bds){
        if (flag){
            return;
        }
        if (index>=zl.length){
            return;
        }
        if (currentSum>svgzl){
            return;
        }
        for (int i = index; i < zl.length; i++) {
            int sum = currentSum+zl[i];
            String bd = bds+"+"+zl[i];
            if (sum==svgzl){
                System.out.println(bd);
                flag = true;
                return;
            }
            fun(zl, svgzl, index+1, sum,bd);
        }
    }
}
