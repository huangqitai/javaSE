package com.hqt.hwod;

import org.junit.Test;

/**
 * 在一个 n * m 的二维数组中，每一行都按照从左到右非递减的顺序排序，
 * 每一列都按照从上到下非递减的顺序排序。请完成一个高效的函数，输入这样的一个二维数组和一个整数，
 * 判断数组中是否含有该整数。
 * 示例:
 * 现有矩阵 matrix 如下：
 * [
 *   [1,   4,  7, 11, 15],
 *   [2,   5,  8, 12, 19],
 *   [3,   6,  9, 16, 22],
 *   [10, 13, 14, 17, 24],
 *   [18, 21, 23, 26, 30]
 * ]
 * 给定 target=5，返回true。
 * 给定target=20，返回false。
 */
public class Solution2 {
    boolean flag = false;
    @Test
    public void t1(){
        int[][] matrix = new int[][]{
                {1,   4,  7, 11, 15},
                {2,   5,  8, 12, 19},
                {3,   6,  9, 16, 22},
                {10, 13, 14, 17, 24},
                {18, 21, 23, 26, 30},
        };
        /*int[][] matrix = new int[][]{
                {1,3,5}
        };*/
        /*int[][] matrix = new int[][]{
                {1,3,5}
        };*/
        int target = 5;
        System.out.println(findNumberIn2DArray(matrix,target));
    }
    public boolean findNumberIn2DArray(int[][] matrix, int target) {
        if (matrix==null||matrix.length==0||matrix[0].length==0){
            return false;
        }
        int n = matrix.length-1;
        int m = matrix[0].length-1;
        fun(matrix,target,n,m,0);
        return flag;
    }

    /**
     * 寻找目标值是否存在
     * @param matrix 二维数组
     * @param target 目标值
     * @param x 当前下标
     * @param n 一维最大下标
     * @param m 二维最大下标
     * @return
     */
    private void fun(int[][] matrix, int target,int n,int m,int x){
        if (flag){
            return;
        }
        if (x>n){
            return;
        }
        if (!(matrix[x][0]>target||matrix[x][m]<target)){
            fun(matrix[x],target,0,m);
        }
        if (flag){
            return;
        }
        fun(matrix, target, n, m, ++x);

    }

    /**
     * 在锁定行（第二层数组）中寻找目标值
     * @param arr 一维数组
     * @param target 目标值
     * @param n 起始下标
     * @param m 结束下标
     * @return
     */
    private void fun(int[] arr, int target,int n,int m){
        //已经是当前行的第一个元素了
        if (m==0){
            flag = arr[m]==target;
            return;
        }
        //开始下标和结束坐标相等，无法再次二分
        if (n==m){
            flag = arr[m]==target;
            return;
        }
        //只剩下最后两个元素
        if (n+1==m){
            flag = arr[m]==target||arr[n]==target;
            return;
        }
        int center = (m+n)/2;
        //中间值等于目标值，直接返回true
        if (arr[center]==target){
            flag = true;
            return;
        }

        //目标值大于中间值，则目标值在后半部分
        if (arr[center]<target){
            //中间下标等于0，说明最后一个元素也不匹配
            if (center==0){
                return;
            }
            fun(arr, target, center, m);
        }
        //目标值小于中间值，则目标值在前半部分
        else if (arr[center]>target){
            fun(arr, target, n, center);
        }
    }
}
