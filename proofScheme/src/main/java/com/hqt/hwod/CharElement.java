package com.hqt.hwod;

public class CharElement {
    private String val;
    private Integer sd;

    public CharElement(String val, Integer sd) {
        this.val = val;
        this.sd = sd;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

    public Integer getSd() {
        return sd;
    }

    public void setSd(Integer sd) {
        this.sd = sd;
    }
}
