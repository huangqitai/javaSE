package com.hqt.hwod;

import java.util.Scanner;

/**
 * 3.
 * 进制转换
 * 写出一个程序，接受一个十六进制的数，输出该数值的十进制表示。
 * 数据范围：保证结果在 1≤n≤2^31-1
 * 示例1
 * 输入例子：
 * 0xAA
 * 输出例子：
 * 170
 */
public class Solution14 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNext()){
            String input = in.next();
            input = input.substring(2);
            if (input.length()==2){
                input = "00"+input;
            }
            int sum = 0;
            char[] chars = input.toCharArray();
            for (int i = 0; i < chars.length; i++) {
                int m = chars.length-i-1;
                int n;
                String c = chars[i]+"";
                switch (c){
                    case "A":n = 10;break;
                    case "B":n = 11;break;
                    case "C":n = 12;break;
                    case "D":n = 13;break;
                    case "E":n = 14;break;
                    case "F":n = 15;break;
                    default:n = Integer.parseInt(c);
                }
                sum+=n*fun(16,m);
            }
            System.out.println(sum);
        }
    }
    private static int fun(int n,int m){
        if (m<=0){
            return 1;
        }
        return n*fun(n, m-1);
    }
}
