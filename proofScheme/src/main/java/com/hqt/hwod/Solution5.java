package com.hqt.hwod;

import org.junit.Test;

/**
 * 在一个 m*n 的棋盘的每一格都放有一个礼物，每个礼物都有一定的价值（价值大于 0）。
 * 你可以从棋盘的左上角开始拿格子里的礼物，并每次向右或者向下移动一格、直到到达棋盘的右下角。
 * 给定一个棋盘及其上面的礼物的价值，请计算你最多能拿到多少价值的礼物？
 * 示例 1:
 * 输入:
 * [
 *  [1,3,1],
 *  [1,5,1],
 *  [4,2,1]
 * ]
 * 输出: 12
 * 解释: 路径 1→3→5→2→1 可以拿到最多价值的礼物
 */
public class Solution5 {
    @Test
    public void t1(){
        int[][] grid = new int[][]{
                {1,3,1},
                {1,5,1},
                {4,2,1}
        };
        System.out.println(maxValue(grid));
    }
    public int maxValue(int[][] grid) {
        int maxi = grid.length;
        int maxj = grid[0].length;
        int[][] gridValue = new int[maxi][maxj];
        for (int i = 0; i < maxi; i++) {
            for (int j = 0; j < maxj; j++) {
                if (i==0&&j==0){
                    gridValue[i][j] = grid[i][j];
                }
                if (i==0&&j>0){
                    gridValue[i][j] = gridValue[i][j-1]+grid[i][j];
                }
                if (i>0&&j==0){
                    gridValue[i][j] = gridValue[i-1][j]+grid[i][j];
                }
                if (i>0&&j>0){
                    gridValue[i][j] = Math.max(gridValue[i-1][j],gridValue[i][j-1])+grid[i][j];
                }
            }
        }
        return gridValue[maxi-1][maxj-1];
    }
}
