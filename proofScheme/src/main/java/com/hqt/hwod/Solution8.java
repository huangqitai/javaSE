package com.hqt.hwod;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 给定一个仅包含数字2-9的字符串，返回所有它能表示的字母组合。答案可以按 任意顺序 返回。
 * 给出数字到字母的映射如下（与电话按键相同）。注意 1 不对应任何字母。
 *
 * 输入：digits = "23"
 * 输出：["ad","ae","af","bd","be","bf","cd","ce","cf"]
 */
public class Solution8 {
    int[] num = new int[]{2,3,4,5,6,7,8,9};
    String[] strings = new String[]{"abc","def","ghi","jkl","mno","pqrs","tuv","wxyz"};

    @Test
    public void t1(){
        List<String> letterCs = letterCombinations("24");
        System.out.println(letterCs);
    }
    public List<String> letterCombinations(String digits) {
        List<String> letterCs = new ArrayList<>();
        if ("".equals(digits)){
            return letterCs;
        }
        List<char[]> charStrs = new ArrayList<>();
        for (int i = 0; i < digits.length(); i++) {
            int index = Integer.parseInt(String.valueOf(digits.charAt(i)));
            String s = strings[num[index-2]-2];
            char[] chars = s.toCharArray();
            charStrs.add(chars);
        }
        fun(charStrs,"",0,letterCs);
        return letterCs;
    }
    private void fun(List<char[]> charStrs,String output,int index,List<String> letterCs){
        if (index>=charStrs.size()){
            return;
        }
        char[] chars = charStrs.get(index);
        ++index;
        for (int i = 0; i < chars.length; i++) {
            char aChar = chars[i];
            String s = output + aChar;
            fun(charStrs, s,index ,letterCs);
            if (index==charStrs.size()){
                letterCs.add(s);
            }
        }
    }
}
