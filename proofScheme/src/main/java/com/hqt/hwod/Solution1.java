package com.hqt.hwod;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;


/**
 * 【二叉树中序遍历】
 * 根据给定的二叉树结构描述字符串，输出该二叉树按照中序遍历结果字符串。
 * 中序遍历顺序为：左子树，根结点，右子树。
 * 输入描述
 * 由大小写字母、左右大括号、逗号组成的字符串:字母代表一个节点值，左右括号内包含该节点的子节点。
 * 左右子节点使用逗号分隔，逗号前为空则表示左子节点为空，没有逗号则表示右子节点为空。
 * 二叉树节点数最大不超过100。
 * 注:输入字符串格式是正确的，无需考虑格式错误的情况。
 * 输出描述
 * 输出一个字符串为二叉树中序遍历各节点值的拼接结果。
 * 示例 1  输入输出示例仅供调试，后台判题数据一般不包含示例
 * 输入
 * a{b{d,e{g,h{,I}}},c{f}}
 * 输出
 * dbgehiafc
 */
public class Solution1 {
    @Test
    public void t1(){
        String input = "a{b{d,e{g,h{,I}}},c{f}}";
        fun(input);
    }
    public void fun(String input){
        TreeNode treeNode = buildTree(input);
        List<String> resultStr = new ArrayList<>();
        centerForSearch(treeNode,resultStr);
        StringBuilder output = new StringBuilder();
        for (String s:resultStr){
            if (!"".equals(s)){
                output.append(s);
            }
        }
        System.out.println(output.toString());
    }
    private void centerForSearch(TreeNode treeNode,List<String> resultStr){
        if (treeNode==null){
            return;
        }
        centerForSearch(treeNode.left,resultStr);
        resultStr.add(treeNode.val);
        centerForSearch(treeNode.right,resultStr);

    }
    private TreeNode buildTree(String input){
        TreeNode treeNode = new TreeNode(""+input.charAt(0));
        char[] chars = input.toCharArray();
        int sd = 0;
        List<CharElement> charElements = new ArrayList<>();
        TreeNode currentNode = treeNode;

        for (int i = 1; i < chars.length; i++) {
            char c = chars[i];
            if (c==','){
                if (!(chars[i-1]=='{'||chars[i+1]=='}')){
                    continue;
                }
            }
            if (c=='{'){
                sd++;
            }else if (c=='}'){
                currentNode = currentNode.parent;
                sd--;
            }else {
                TreeNode cTreeNode = null;
                if (c==','){
                    cTreeNode = new TreeNode("");
                    charElements.add(new CharElement("",sd));
                }else {
                    cTreeNode = new TreeNode(c+"");
                    charElements.add(new CharElement(c+"",sd));
                }
                if (currentNode.left==null){
                    currentNode.left = cTreeNode;
                    cTreeNode.parent = currentNode;
                }else {
                    currentNode.right = cTreeNode;
                    cTreeNode.parent = currentNode;
                }
                if (chars[i+1]=='{'){
                    currentNode = cTreeNode;
                }
                System.out.println(c+"深度"+sd);
            }
        }
        return treeNode;
    }
}
