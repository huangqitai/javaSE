package com.hqt.hwod;

import org.junit.Test;

/**
 * 给你一根长度为 n 的绳子，请把绳子剪成整数长度的 m 段（m、n都是整数，n>1并且m>1），
 * 每段绳子的长度记为 k[0],k[1]...k[m-1] 。请问 k[0]*k[1]*...*k[m-1] 可能的最大乘积是多少？
 * 例如，当绳子的长度是8时，我们把它剪成长度分别为2、3、3的三段，此时得到的最大乘积是18。
 * 示例 1：
 * 输入: 2
 * 输出: 1
 * 解释: 2 = 1 + 1, 1 × 1 = 1
 * 示例2:
 * 输入: 10
 * 输出: 36
 * 解释: 10 = 3 + 3 + 4, 3 ×3 ×4 = 36
 */
public class Solution3 {
    @Test
    public void t1(){
        int n = 10;
        System.out.println(cuttingRope(n));
    }
    public int cuttingRope(int n) {
        //可知每段长度不能小于1，也就是不能有小数的长度，否则乘积小于乘以1的
        if (n==2){
            return 1;
        }
        if (n==3){
            return 2;
        }
        if (n==4){
            return 4;
        }
        return cuttingRopeFun(n);
    }
    private int cuttingRopeFun(int n){
        int n3Y = n%3;
        int n3 = n/3;
        if (n3Y==0){
            return fun(3,n3);
        }
        if (n3Y==1){
            n3 = n3-1;
            return fun(3,n3)*(n-n3*3);
        }
        //if (n3Y==2){
            return fun(3,n3)*(n-n3*3);
        //}
    }
    private int fun(int x,int y){
        if (y==1){
            return x;
        }
        return x*fun(x, y-1);
    }
}
