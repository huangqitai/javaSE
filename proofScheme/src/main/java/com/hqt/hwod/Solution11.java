package com.hqt.hwod;

import org.junit.Test;

/**
 * 10. 正则表达式匹配
 * 给你一个字符串 s 和一个字符规律 p，请你来实现一个支持 '.' 和 '*' 的正则表达式匹配。
 * '.' 匹配任意单个字符
 * '*' 匹配零个或多个前面的那一个元素
 * 所谓匹配，是要涵盖 整个 字符串 s的，而不是部分字符串。
 * 示例 1：
 * 输入：s = "aa", p = "a"
 * 输出：false
 * 解释："a" 无法匹配 "aa" 整个字符串。
 * 示例 2:
 * 输入：s = "aa", p = "a*"
 * 输出：true
 * 解释：因为 '*' 代表可以匹配零个或多个前面的那一个元素, 在这里前面的元素就是 'a'。
 * 因此，字符串 "aa" 可被视为 'a' 重复了一次。
 *
 * s 只包含从 a-z 的小写字母。
 * p 只包含从 a-z 的小写字母，以及字符 . 和 *。
 */
public class Solution11 {
    String[] ss = new String[]{"a","b","c","d","e","f","g","h","i","j","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};
    boolean flag = false;
    @Test
    public void t1(){
        //String s = "a";
        //String p = "ab*a";
        String s ="a";
        String p =".*..a*";
        System.out.println(isMatch(s,p));
    }
    public boolean isMatch(String s, String p) {
        if ("".equals(s)||"".equals(p)){
            return false;
        }
        if (!(p.contains("*")||p.contains("."))){
            return p.equals(s);
        }
        String pends = "";
        int pendsindex = -1;
        if (!(p.endsWith("*")||p.endsWith("."))){
            pendsindex = Math.max(p.lastIndexOf("."),p.lastIndexOf("*"))+1;
            pends = p.substring(Math.max(p.lastIndexOf("."),p.lastIndexOf("*"))+1);
            if (!s.endsWith(pends)){
                return false;
            }
        }
        if (!(p.startsWith("*")||p.startsWith("."))){
            int p1 = p.indexOf(".");
            int p2 = p.indexOf("*");
            int pindexof;
            if (p1==-1){
                pindexof = p2;
            }else if (p2==-1){
                pindexof = p1;
            }else {
                pindexof = Math.min(p.indexOf("."),p.indexOf("*"));
            }
            if (pindexof>=2){
                String pstarts = p.substring(0,pindexof-1);
                if (!s.startsWith(pstarts)||(pstarts+pends).length()>s.length()){
                    return false;
                }
            }
        }
        //s = s.replace(pends,"");
        if (pendsindex!=-1){
            s = s.substring(0,s.length()-(p.length()-pendsindex));
            p = p.substring(0,pendsindex);
        }

        fun(s,p,"",0);
        return flag;
    }
    private void fun(String s,String p,String c,int pindex){
        if (!s.startsWith(c)){
            return;
        }
        if (c.length()==s.length()){
            flag = c.equals(s);
            return;
        }
        if (c.length()>s.length()){
            return;
        }
        if (pindex>=p.length()){
            return;
        }
        if (p.charAt(pindex)=='.'){
            for (int i = 0; i < ss.length; i++) {
                String newc = c+ss[i];
                fun(s, p, newc, pindex+1);
            }
        }
        else if (p.charAt(pindex)=='*'){
            if (p.charAt(pindex-1)=='.'){
                for (int i = 0; i < ss.length; i++) {
                    String prec = ss[i];
                    String newc = c;
                    for (int j = 0; j < s.length(); j++) {
                        newc+=prec;
                        fun(s, p, newc, pindex);
                    }
                }
            }
            else {
                String prec = p.charAt(pindex-1)+"";
                for (int i = 0; i <= s.length(); i++) {
                    String newc = c;
                    for (int j = 0; j < i; j++) {
                        newc+=prec;
                    }
                    fun(s, p, newc, pindex+1);
                }
            }

        }else {
            if (pindex+1<p.length()&&p.charAt(pindex+1)=='*'){
                fun(s, p, c, pindex+1);
            }else {
                c+=p.charAt(pindex);
                fun(s, p, c, pindex+1);
            }
        }
    }
}
