package com.hqt.hwod;

import org.junit.Test;

/**
 * 请从字符串中找出一个最长的不包含重复字符的子字符串，计算该最长子字符串的长度。
 * 示例1:
 *
 * 输入: "bbcabcbb"
 * 输出: 3
 * 解释: 因为无重复字符的最长子串是 "abc"，所以其长度为 3。
 */
public class Solution6 {
    @Test
    public void t1(){
        String s = "abcabcbb";
        System.out.println(lengthOfLongestSubstring(s));
    }
    public int lengthOfLongestSubstring(String s) {
        return indexLength(s,0,0);
    }
    private int indexLength(String s,int maxLength,int index){
        if (index==s.length()){
            return maxLength;
        }
        StringBuilder chars = new StringBuilder();
        int lengthOfLongestSubstring = 0;
        for (int i = index; i < s.length(); i++) {
            String c = s.charAt(i)+"";
            if (chars.toString().contains(c)){
                break;
            }
            chars.append(c);
            ++lengthOfLongestSubstring;
        }
        //indexLength[index] = lengthOfLongestSubstring;
        return indexLength(s, Math.max(maxLength,lengthOfLongestSubstring), ++index);
    }
}
