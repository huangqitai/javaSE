package com.hqt.hwod;

import java.util.Scanner;

/**
 * 1.
 * 汽水瓶
 * 某商店规定：三个空汽水瓶可以换一瓶汽水，允许向老板借空汽水瓶（但是必须要归还）。
 * 小张手上有n个空汽水瓶，她想知道自己最多可以喝到多少瓶汽水。
 * 数据范围：输入的正整数满足
 * 1≤n≤100
 * 示例1
 * 输入例子：
 * 3
 * 10
 * 81
 * 0
 * 输出例子：
 * 1
 * 5
 * 40
 * 例子说明：
 * 样例 1 解释：用三个空瓶换一瓶汽水，剩一个空瓶无法继续交换
 * 样例 2 解释：用九个空瓶换三瓶汽水，剩四个空瓶再用三个空瓶换一瓶汽水，剩两个空瓶，
 * 向老板借一个空瓶再用三个空瓶换一瓶汽水喝完得一个空瓶还给老板
 */
public class Solution12 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextInt()) { // 注意 while 处理多个 case
            int n = in.nextInt();
            if (n==0){
                break;
            }
            if (n<1||n>100){
                System.exit(0);
            }
            if (n==1){
                System.out.println(0);
            }
            if (n==2){
                System.out.println(1);
            }
            System.out.println(fun(n));
        }
    }
    private static int fun(int n){
        int k = 0;
        while (n>1){
            if (n==2){
                k++;
                break;
            }
            //被3整除，说明手里的空瓶可以完全兑换汽水
            if (n%3==0){
                int m = n/3;
                k+=m;
                n = m;
            } else if (n%3==1){
                int m = n/3;
                k+=m;
                n = m+1;
            }else {
                int m = n/3;
                k+=m;
                n = m+2;
            }
        }
        return k;
    }

}
