package com.hqt.hwod;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

/**
 * 如果要买归类为附件的物品，必须先买该附件所属的主件，且每件物品只能购买一次。
 * 每个主件可以有 0 个、 1 个或 2 个附件。附件不再有从属于自己的附件。
 * 王强查到了每件物品的价格（都是 10 元的整数倍），而他只有 N 元的预算。
 * 除此之外，他给每件物品规定了一个重要度，用整数 1 ~ 5 表示。他希望在花费不超过 N 元的前提下，使自己的满意度达到最大。
 * 满意度是指所购买的每件物品的价格与重要度的乘积的总和，
 * 请你帮助王强计算可获得的最大的满意度。
 *
 * 输入描述：
 * 输入的第 1 行，为两个正整数N，m，用一个空格隔开：
 * （其中 N （ N<32000 ）表示总钱数， m （m <60 ）为可购买的物品的个数。）
 * 从第 2 行到第 m+1 行，第 j 行给出了编号为 j-1 的物品的基本数据，每行有 3 个非负整数 v p q
 *
 * （其中 v 表示该物品的价格（ v<10000 ）， p 表示该物品的重要度（ 1 ~ 5 ），
 * q 表示该物品是主件还是附件。如果 q=0 ，表示该物品为主件，如果 q>0 ，
 * 表示该物品为附件， q 是所属主件的编号）
 */
public class Solution16 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // 注意 hasNext 和 hasNextLine 的区别
        while (in.hasNextInt()) { // 注意 while 处理多个 case
            int N = in.nextInt();
            int m = in.nextInt();
            List<List<Integer>> vpqList = new ArrayList<>();
            for (int i = 0; i < m; i++) {
                List<Integer> vpqs = new ArrayList<>();
                int v = in.nextInt();
                int p = in.nextInt();
                int q = in.nextInt();
                vpqs.add(v);
                vpqs.add(p);
                vpqs.add(q);
                vpqList.add(vpqs);
            }
            if (vpqList.size()==m){
                //System.out.println(fun1(N,m,vpqList,new int[m],0,0));
                System.out.println(fun(N,m,vpqList));
            }
        }
    }
    private static int fun(int N,int m,List<List<Integer>> vpqList){
        int maxvp = 0;
        for (int i = 0; i < m; i++) {
            List<Integer> vpqs = vpqList.get(i);
            int[] used = new int[m];
            used[i] = 1;
            int vsum = vpqs.get(0);
            int vpsum = vpqs.get(0)*vpqs.get(1);
            if (vpqs.get(2)!=0&&used[vpqs.get(2)-1]!=1){
                List<Integer> vpqsmain = vpqList.get(vpqs.get(2)-1);
                if (vsum+vpqsmain.get(0)>N){
                    continue;
                }
                used[vpqs.get(2)-1] = 1;
                vpsum+=vpqsmain.get(0)*vpqsmain.get(1);
                vsum+=vpqsmain.get(0);
            }
            maxvp = Math.max(maxvp,vpsum+fun(N-vsum, m, vpqList,used,0,0,i+1));
        }
        return maxvp;
    }
    private static int fun(int N,int m,List<List<Integer>> vpqList,int[] used,int vsum,int vpsum,int i){
        for (; i < m; i++) {
            if (used[i]!=0){
                continue;
            }
            List<Integer> vpqs = vpqList.get(i);
            if (vpqs.get(2)!=0&&used[vpqs.get(2)-1]!=1){
                List<Integer> vpqsmain = vpqList.get(vpqs.get(2)-1);
                if (vsum+vpqsmain.get(0)+vpqs.get(0)>N){
                    continue;
                }
                used[vpqs.get(2)-1] = 1;
                vpsum+=vpqsmain.get(0)*vpqsmain.get(1);
                vsum+=vpqsmain.get(0);
            }
            if (vsum+vpqs.get(0)>N){
                continue;
            }
            used[i] = 1;
            vpsum+=vpqs.get(0)*vpqs.get(1);
            vsum += vpqs.get(0);
            if (vsum==N){
                break;
            }
            vpsum+=fun(N-vsum, m, vpqList, used, 0,0,i+1);
        }
        return vpsum;
    }
    private static int fun1(int N,int m,List<List<Integer>> vpqList,int[] used,int v,int i){
        int maxvp = 0;
        for (; i < m; i++) {
            int vp = 0;
            if (used[i]!=0){
                continue;
            }
            List<Integer> vpqs = vpqList.get(i);
            if (vpqs.get(2)!=0&&used[vpqs.get(2)-1]!=1){
                List<Integer> vpqsmain = vpqList.get(vpqs.get(2)-1);
                if (v+vpqsmain.get(0)>N){
                    return vp;
                }
                used[vpqs.get(2)-1] = 1;
                vp+=vpqsmain.get(0)*vpqsmain.get(1);
                v+=vpqsmain.get(0);
            }
            if (v+vpqs.get(0)>N){
                return vp;
            }
            used[i] = 1;
            vp+=vpqs.get(0)*vpqs.get(1);
            v += vpqs.get(0);
            //vp+=fun(N, m, vpqList, used, v,i+1);
            maxvp = Math.max(vp,maxvp);
        }
        return maxvp;
    }
}
