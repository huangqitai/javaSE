package com.hqt.database;

import java.math.BigDecimal;


public class SpKjNjzjdxx  {
    /**
     * 标识码
     */

    private Long bsm;

    /**
     * 宅基地宗地标识码
     */

    private String zjdzdbsm;

    /**
     * 要素代码
     */

    private String ysdm;

    /**
     * 建房标识码
     */

    private String jfbsm;

    /**
     * 行政区代码
     */

    private String xzqdm;

    /**
     * 区县代码
     */

    private String qxdm;

    /**
     * 所有权人标识码
     */

    private String suoyqrbsm;

    /**
     * 所有权人代码
     */

    private String suyqrdm;

    /**
     * 宅基地代码a
     */

    private String zjddm;

    /**
     * 宗地代码b
     */

    private String zddm;

    /**
     * 不动产单元号
     */

    private String bdcdyh;

    /**
     * 坐落
     */

    private String zl;

    /**
     * 坐落单位代码c
     */

    private String zldwdm;

    /**
     * 宗地面积
     */

    private BigDecimal zdmj;

    /**
     * 用途d
     */

    private String yt;

    /**
     * 用途名称
     */

    private String ytmc;

    /**
     * 等级
     */

    private String dj;

    /**
     * 价格
     */

    private BigDecimal jg;

    /**
     * 权利类型
     */

    private String qllx;

    /**
     * 权利性质
     */

    private String qlxz;

    /**
     * 权利设定方式
     */

    private String qlsdfs;

    /**
     * 容积率
     */

    private String rjl;

    /**
     * 建筑密度
     */

    private BigDecimal jzmd;

    /**
     * 建筑限高
     */

    private BigDecimal jzxg;

    /**
     * 宗地四至-东
     */

    private String zdszd;

    /**
     * 宗地四至-南
     */

    private String zdszn;

    /**
     * 宗地四至-西
     */

    private String zdszx;

   }