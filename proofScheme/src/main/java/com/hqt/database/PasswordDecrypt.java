package com.hqt.database;

import com.alibaba.druid.filter.config.ConfigTools;
import org.junit.Test;

public class PasswordDecrypt {
    @Test
    public void t1() throws Exception {
        String publicKey = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAJWK6Aw4hcmBvwDBXDmQv24lEs9mUTqcFlFUQzrcLNo90oUaYII3JGyi8n0lSWfpmWNCNdxwDcrYw3fT5qXjn28CAwEAAQ==";
        //String passwordEncrypt = "YDRAAeisVdZyOjhA2pRokzSgHQrQEYzP336NDpUdas1f35/t7EcQiu+ln/1QAT4/xgUG7ufUG0a/8eOAj9mTdA==";
        String passwordEncrypt = "EveX6hsgyelt4Svi7vj6y19n3yj1JdgYBzKNU2WYjIwXXJVczFUHZ2vKhmsMkd9yzQ7OsvLTtrGOApQX7n4nvw==";
        String password = ConfigTools.decrypt(publicKey,passwordEncrypt);
        System.out.println(password);
    }
}
