package com.hqt.database;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class MybatisSql {

    @Test
    public void t1(){
        String fieldSql = "<choose><when test=\"item.%s != null\">#{item.%s,jdbcType=VARCHAR},</when><otherwise>'',</otherwise></choose>";
        List<String> fields = Arrays.asList("bsm","zjdzdbsm","ysdm","jfbsm","xzqdm","qxdm","suoyqrbsm","suyqrdm","zjddm","zddm","bdcdyh","zl","zldwdm","zdmj","yt","ytmc","dj","jg","qllx","qlxz","qlsdfs","rjl","jzmd","jzxg","zdszd","zdszn","zdszx","zdszb","zdt","tfh","djh","djbbh","zjdpzmj","sfcz","czmj","zjdqdfs","zjdqdsj","ywh","sffz","zsh","zsmj","fzdw","wbzyy","sjly","sjlybsm","bz","hzxm","zjdzt","zt","geom","ydjfsqbsm","zjqydm");
        for (String field:fields){
            System.out.println(String.format(fieldSql,field,field));
        }
    }
    @Test
    public void t2(){
        //List<String> fields = Arrays.asList("BSM","ZJDZDBSM","YSDM","JFBSM","XZQDM","QXDM","SUOYQRBSM","SUYQRDM","ZJDDM","ZDDM","BDCDYH","ZL","ZLDWDM","ZDMJ","YT","YTMC","DJ","JG","QLLX","QLXZ","QLSDFS","RJL","JZMD","JZXG","ZDSZD","ZDSZN","ZDSZX","ZDSZB","ZDT","TFH","DJH","DJBBH","ZJDPZMJ","SFCZ","CZMJ","ZJDQDFS","ZJDQDSJ","YWH","SFFZ","ZSH","ZSMJ","FZDW","WBZYY","SJLY","SJLYBSM","BZ","HZXM","ZJDZT","ZT","YDJFSQBSM","ZJQYDM");
        //List<String> types = Arrays.asList("NUMBER","VARCHAR2","VARCHAR2","VARCHAR2","VARCHAR2","VARCHAR2","VARCHAR2","VARCHAR2","VARCHAR2","VARCHAR2","VARCHAR2","VARCHAR2","VARCHAR2","NUMBER","VARCHAR2","VARCHAR2","VARCHAR2","NUMBER","VARCHAR2","VARCHAR2","VARCHAR2","VARCHAR2","NUMBER","NUMBER","VARCHAR2","VARCHAR2","VARCHAR2","VARCHAR2","VARCHAR2","VARCHAR2","VARCHAR2","VARCHAR2","NUMBER","VARCHAR2","NUMBER","VARCHAR2","DATE","VARCHAR2","VARCHAR2","VARCHAR2","NUMBER","VARCHAR2","VARCHAR2","VARCHAR2","VARCHAR2","VARCHAR2","VARCHAR2","VARCHAR2","VARCHAR2","VARCHAR2","VARCHAR2");
        //List<Integer> bls = Arrays.asList(0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,4,0,0,0,0,2,2,0,0,0,0,0,0,0,0,2,0,2,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0);
        List<String> fields = Arrays.asList("BSM","GDBSM","DAH","CLMC","CLBM","QSY","JSY","PXH","YS","CJRY","CJSJ","XGRY","XGSJ","QXDM","YJS","FYJS","DZJS","CLLY","BZ","SSBM");
        List<String> types = Arrays.asList("VARCHAR2","VARCHAR2","VARCHAR2","VARCHAR2","VARCHAR2","NUMBER","NUMBER","NUMBER","NUMBER","VARCHAR2","TIMESTAMP","VARCHAR2","TIMESTAMP","VARCHAR2","NUMBER","NUMBER","NUMBER","VARCHAR2","VARCHAR2","VARCHAR2");
        List<Integer> bls = Arrays.asList(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

        String fieldSql = "<choose><when test=\"item.%s != null\">#{item.%s,jdbcType=%s},</when><otherwise>%s,</otherwise></choose>";
        for (int i = 0; i < fields.size(); i++) {
            String type = types.get(i);
            String field = fields.get(i).toLowerCase();
            String jdbcType = "VARCHAR";
            String defaultValue = "''";
            if ("NUMBER".equals(type)){
                if (bls.get(i)!=0){
                    jdbcType = "DOUBLE";
                }else {
                    jdbcType = "INTEGER";
                }
                defaultValue = "null";
            }
            else if ("DATE".equals(type)){
                jdbcType = "DATE";
                defaultValue = "null";
            }else if ("TIMESTAMP".equals(type)){
                jdbcType = "TIMESTAMP";
                defaultValue = "null";
            }
            System.out.println(String.format(fieldSql,field,field,jdbcType,defaultValue));
        }
    }
}
