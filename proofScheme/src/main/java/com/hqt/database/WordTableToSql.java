package com.hqt.database;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WordTableToSql {
    List<String> tableNames = Arrays.asList("UP_PRO_ACCEPT_V4","UP_PRO_PROCESS_V4","UP_PRO_RESULT_V4","UP_PRO_SPECIALPROCEDURE_V4","UP_PRO_MATERIAL_V4","UP_PRO_CORRECTION_V4","UP_PRO_RECEIVE_V4","UP_DATA_RECONCILIATION_V4");
    @Test
    public void t1() throws FileNotFoundException {
        String wordPath = "D:\\臻善科技资源收藏\\工作文档20230515\\政务服务办件过程对接\\建表.docx";
        String sqlFilePath = "D:\\臻善科技资源收藏\\工作文档20230515\\政务服务办件过程对接";
        File file = new File(wordPath);
        Map<String, List<Map<String,String>>> tableFieldMap = doDocx(file,1,9);
        writeCreateTableSql(tableFieldMap,"zwhj",sqlFilePath);
    }

    public void writeCreateTableSql(Map<String, List<Map<String,String>>> tableFieldMap,String schemaName,String sqlFilePath) throws FileNotFoundException {
        for (String tableName:tableFieldMap.keySet()){
            File sqlFile = new File(sqlFilePath+"\\"+tableName+".sql");
            // 使用PrintWriter添加换行符
            PrintWriter writer = new PrintWriter(sqlFile);
            writer.println("create table "+ schemaName+"."+tableName+"();");
            List<Map<String,String>> fieldList = tableFieldMap.get(tableName);
            for (Map<String,String> fieldMap:fieldList){
                String fieldName = fieldMap.get("fieldName").toLowerCase();
                String type = fieldMap.get("typeAndLength");
                if (type.startsWith("C")){
                    if (type.contains(".")){
                        String[] types = type.split("\\.");
                        type = "varchar("+types[types.length-1]+")";
                    }else {
                        type = "varchar("+type.substring(1)+")";
                    }
                }
                else if (type.startsWith("D")){
                    type = "timestamp";
                }
                else if (type.startsWith("N")){
                    type = "int4";
                }
                writer.println(String.format("alter table %s add column %s %s;",schemaName+"."+tableName,fieldName,type));
                String comment = fieldMap.get("name")+":"+fieldMap.get("bz");
                writer.println(String.format("comment on column %s is '%s';",schemaName+"."+tableName+"."+fieldName,comment));
            }
            writer.close();
        }
    }

    /**
     * 读取word中表格的数据
     * @param file 文件
     * @param start 从第几个表格开始 默认从0
     * @param end 到第几个表格结束 默认全部
     * @return
     */
    public Map<String, List<Map<String,String>>> doDocx(File file, Integer start, Integer end) {
        //用于存放预览信息
        Map<String, List<Map<String,String>>> tableFieldMap = new HashMap<>();
        try {
            List<String> cellTitles = Arrays.asList("序号","名称","字段名","数据格式","必填项","备注");
            List<String> cellNames = Arrays.asList("xh","name","fieldName","typeAndLength","notNull","bz");
            //获取文件流
            InputStream is = new FileInputStream(file);
            //获取文件对象
            XWPFDocument doc = new XWPFDocument(is);

            //获取所有的表格对象
            List<XWPFTable> tables = doc.getTables();
            if (start==null){
                start = 0;
            }
            if (end==null){
                end = tables.size();
            }
            for (int i = start; i < end; i++){
                XWPFTable table = tables.get(i);
                List<Map<String,String>> fieldList = new ArrayList<>();
                //迭代行，第一行为标题，跳过
                for (int j = 1; j < table.getRows().size(); j++) {
                    Map<String,String> fieldMap = new HashMap<>();
                    //当前行
                    XWPFTableRow tr = table.getRow(j);
                    //迭代列，从0开始
                    for (int x = 0; x < tr.getTableCells().size(); x++) {
                        //取得单元格
                        XWPFTableCell td = tr.getCell(x);
                        //取得单元格的内容
                        String text = td.getText();
                        fieldMap.put(cellNames.get(x),text);
                    }

                    fieldList.add(fieldMap);
                }
                System.out.println(i-start);
                tableFieldMap.put(tableNames.get(i-start).toLowerCase(),fieldList);
            }
            return tableFieldMap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tableFieldMap;
    }
    // 读取doc文档中表格数据示例
    public List<String> doDocx(File file) {
        //用于存放预览信息
        List<String> list = new ArrayList<String>();
        try {
            //获取文件流
            InputStream is = new FileInputStream(file);
            //获取文件对象
            XWPFDocument doc = new XWPFDocument(is);

            //获取所有的表格对象
            List tables = doc.getTables();
            //因我文档中只有一个所以这里没有去遍历，直接获取指定表格对象
            XWPFTable table = (XWPFTable) tables.get(0);

            //迭代行，默认从0开始
            for (int j = 0; j < table.getRows().size(); j++) {
                //当前行
                XWPFTableRow tr = table.getRow(j);
                //用于存放一行数据，不需要可以不用
                String rowText = "";
                //迭代列，默认从0开始
                for (int x = 0; x < tr.getTableCells().size(); x++) {
                    //取得单元格
                    XWPFTableCell td = tr.getCell(x);
                    //取得单元格的内容
                    String text = td.getText();
                    //自己用“ ”区分两列数据，根据自己需求 可以省略
                    if (StringUtils.isEmpty(rowText)) {
                        rowText = text;
                    } else {
                        rowText = rowText + " " + text;
                    }
                }
                list.add(rowText);
            }
            return list;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
}
