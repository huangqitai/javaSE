package com.hqt.solution;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 描述
 * 给定一棵二叉树，判断其是否是自身的镜像（即：是否对称）
 */
public class Solution28 {
    boolean recursion(TreeNode root1, TreeNode root2){
        //可以两个都为空
        if(root1 == null && root2 == null)
            return true;
        //只有一个为空或者节点值不同，必定不对称
        if(root1 == null || root2 == null || root1.val != root2.val)
            return false;
        //每层对应的节点进入递归比较
        return recursion(root1.left, root2.right) && recursion(root1.right, root2.left);
    }
    //牛客的解题思路，比自己的好很多
    boolean isSymmetrical1(TreeNode pRoot) {
        return recursion(pRoot, pRoot);
    }

    boolean isSymmetrical(TreeNode pRoot) {
        if (pRoot==null){
            return true;
        }
        if (pRoot.left==null&&pRoot.right==null){
            return true;
        }
        Map<Integer, List<Integer>> indexValueMap = new HashMap<>();
        levelTraverse(pRoot,0,indexValueMap);
        return isSymmetrical(indexValueMap);
    }
    //层次遍历
    private void levelTraverse(TreeNode treeNode, int index, Map<Integer, List<Integer>> indexValueMap){
        if (treeNode==null){
            if (indexValueMap.containsKey(index)){
                List<Integer> indexValues = indexValueMap.get(index);
                indexValues.add(null);
            }else {
                List<Integer> indexValues = new ArrayList<>();
                indexValues.add(null);
                indexValueMap.put(index,indexValues);
            }
            return;
        }
        if (indexValueMap.containsKey(index)){
            List<Integer> indexValues = indexValueMap.get(index);
            indexValues.add(treeNode.val);
        }else {
            List<Integer> indexValues = new ArrayList<>();
            indexValues.add(treeNode.val);
            indexValueMap.put(index,indexValues);
        }
        if (treeNode.left==null&&treeNode.right==null){
            return;
        }
        levelTraverse(treeNode.left,index+1,indexValueMap);
        levelTraverse(treeNode.right,index+1,indexValueMap);
    }
    private boolean isSymmetrical(Map<Integer, List<Integer>> indexValueMap){
        if (indexValueMap.isEmpty()){
            return false;
        }
        if (indexValueMap.size()==1){
            return true;
        }
        for (int i = 1; i < indexValueMap.size(); i++) {
            List<Integer> indexValues = indexValueMap.get(i);
            if (!isSymmetrical(indexValues)){
                return false;
            }
        }
        return true;
    }
    private boolean isSymmetrical(List<Integer> indexValues){
        if (indexValues.size()%2!=0){
            return false;
        }
        int length = indexValues.size()-1;
        for (int i = 0; i <= length; i++) {
            if ((indexValues.get(i)==null&&indexValues.get(length-i)!=null)||(indexValues.get(i)!=null&&indexValues.get(length-i)==null)){
                return false;
            }
            if (indexValues.get(i)==null&&indexValues.get(length-i)==null){
                continue;
            }
            if (!indexValues.get(i).equals(indexValues.get(length - i))){
                return false;
            }
        }
        return true;
    }

    @Test
    public void t1() {
        /*TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.left.left = new TreeNode(3);
        root.left.right = new TreeNode(4);
        root.right = new TreeNode(2);
        root.right.left = new TreeNode(4);
        root.right.right = new TreeNode(3);*/
        /*TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.left.left = new TreeNode(3);
        root.left.right = null;
        root.right = new TreeNode(2);
        root.right.left = new TreeNode(3);*/
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.left.left = null;
        root.left.right = new TreeNode(3);
        root.right = new TreeNode(2);
        root.right.left = new TreeNode(3);
        System.out.println(isSymmetrical(root));
    }
}
