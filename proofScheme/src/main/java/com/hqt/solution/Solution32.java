package com.hqt.solution;

import org.junit.Test;

import java.util.ArrayList;

public class Solution32 {
    ArrayList<Integer> vals = new ArrayList<>();
    @Test
    public void t1(){
        String str = "8,6,10,#,#,2,1";
        String[] strings = str.split(",");
        TreeNode treeNode = createBinTree(strings,0);
        System.out.println(treeNode.val);
    }
    public TreeNode createBinTree(String[] array, int num) {
        if ("#".equals(array[num])){
            return null;
        }
        //根节点为第一个数
        TreeNode root = new TreeNode(Integer.parseInt(array[num]));
        // 左孩子
        if(num * 2 + 1 < array.length){
            root.left = createBinTree(array, num * 2 + 1);
        }

        // 右孩子
        if(num * 2 + 2 < array.length){
            root.right = createBinTree(array, num * 2 + 2);
        }
        return root;
    }


    public ArrayList<Integer> PrintFromTopToBottom(TreeNode root) {
        if (root==null){
            return vals;
        }
        vals.add(root.val);
        PrintFromTopToBottom(root.left);
        return null;
    }
}
