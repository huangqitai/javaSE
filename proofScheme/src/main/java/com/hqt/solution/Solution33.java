package com.hqt.solution;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * 描述
 * 输入一个整数数组，判断该数组是不是某二叉搜索树的后序遍历的结果。如果是则返回 true ,否则返回 false 。
 * 假设输入的数组的任意两个数字都互不相同。
 *
 * 数据范围： 节点数量
 * 0≤n≤1000 ，节点上的值满足
 * 1≤val≤10^5
 * 保证节点上的值各不相同
 * 提示：
 * 1.二叉搜索树是指父亲节点大于左子树中的全部节点，但是小于右子树中的全部节点的树。
 * 2.该题我们约定空树不是二叉搜索树
 * 3.后序遍历是指按照 “左子树-右子树-根节点” 的顺序遍历
 */
public class Solution33 {
    private boolean a = true;
    @Test
    public void t1(){
        int[] sequence = new int[]{5,7,6,9,11,10,8};
        //int[] sequence = new int[]{1,2,7,4,6,5,3};
        //int[] sequence = new int[]{10,2,6};
        System.out.println(VerifySquenceOfBST(sequence));
    }
    public boolean VerifySquenceOfBST(int [] sequence) {
        if (sequence==null||sequence.length==0){
            return false;
        }
        sequenceFun(sequence);
        return a;
    }
    public void sequenceFun(int [] sequence) {
        if (sequence==null||sequence.length==0){
            return;
        }
        int slipIndex = slipIndex(sequence);
        int[] left = left(sequence,slipIndex);
        int[] right = right(sequence,slipIndex);
        int length = sequence.length;
        int last = sequence[length-1];
        for (int k : left) {
            if (last < k) {
                if (a){
                    a = false;
                    return;
                }
                //return false;
            }
        }
        for (int j : right) {
            if (last > j) {
                if (a){
                    a = false;
                    return;
                }
                //return false;
            }
        }
        if (left.length >1){
            sequenceFun(left);
        }
        if (right.length >1){
            sequenceFun(right);
        }
        //return true;
    }
    public int slipIndex(int [] sequence){
        int length = sequence.length;
        int last = sequence[length-1];
        int slipIndex;
        for (slipIndex = 0; slipIndex < length-1; slipIndex++) {
            if (sequence[slipIndex]>last){
                return slipIndex;
            }
        }
        return slipIndex;
    }
    public int[] left(int [] sequence,int slipIndex){
        int[] left = new int[slipIndex];
        for (int i = 0; i < slipIndex; i++) {
            left[i] = sequence[i];
        }
        return left;
    }
    public int[] right(int [] sequence,int slipIndex){
        int length = sequence.length;
        int[] right = new int[length-slipIndex-1];
        for (int i = slipIndex; i < length-1; i++) {
            right[i-slipIndex] = sequence[i];
        }
        return right;
    }
    /**
     * 返回比数组最后一个元素小、大的子数组
     */
    public Map<String,int[]> slip(int [] sequence){
        Map<String,int[]> resultMap = new HashMap<>();
        int length = sequence.length;
        int last = sequence[length-1];
        int leftLength = 0;
        for (int i = length/2; i>=0&&i+1 < length; ) {
            if (sequence[i]<last&&sequence[i+1]>last){
                leftLength = i+1;
                break;
            }else if (sequence[i]>last){
                i--;
            }else if (sequence[i+1]<last){
                i++;
            }
        }
        int[] left = new int[leftLength];
        int[] right = new int[length-leftLength-1];
        for (int i = 0; i < length-1; i++) {
            if (i<leftLength){
                left[i] = sequence[i];
            }else {
                right[i-leftLength] = sequence[i];
            }
        }
        resultMap.put("left",left);
        resultMap.put("right",right);
        return resultMap;
    }
}
