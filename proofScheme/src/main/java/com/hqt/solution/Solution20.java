package com.hqt.solution;

import org.junit.Test;

import java.util.regex.Pattern;

/**
 * 描述
 * 请实现一个函数用来判断字符串str是否表示数值（包括科学计数法的数字，小数和整数）。
 *
 * 科学计数法的数字(按顺序）可以分成以下几个部分:
 * 1.若干空格
 * 2.一个整数或者小数
 * 3.（可选）一个 'e' 或 'E' ，后面跟着一个整数(可正可负)
 * 4.若干空格
 *
 * 小数（按顺序）可以分成以下几个部分：
 * 1.若干空格
 * 2.（可选）一个符号字符（'+' 或 '-'）
 * 3. 可能是以下描述格式之一:
 * 3.1 至少一位数字，后面跟着一个点 '.'
 * 3.2 至少一位数字，后面跟着一个点 '.' ，后面再跟着至少一位数字
 * 3.3 一个点 '.' ，后面跟着至少一位数字
 * 4.若干空格
 *
 * 整数（按顺序）可以分成以下几个部分：
 * 1.若干空格
 * 2.（可选）一个符号字符（'+' 或 '-')
 * 3. 至少一位数字
 * 4.若干空格
 *
 *
 * 例如，字符串["+100","5e2","-123","3.1416","-1E-16"]都表示数值。
 * 但是["12e","1a3.14","1.2.3","+-5","12e+4.3"]都不是数值。
 *
 * 提示:
 * 1.1 <= str.length <= 25
 * 2.str 仅含英文字母（大写和小写），数字（0-9），加号 '+' ，减号 '-' ，空格 ' ' 或者点 '.'
 */
public class Solution20 {
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param str string字符串
     * @return bool布尔型
     */
    public static final String numberRegex = "(^(-|\\+)?\\d+(\\.\\d+)?$)|(^(-|\\+)?\\.\\d+$)|(^(-|\\+)?\\d+\\.$)";
    public static final String eRegex = "((^(-|\\+)?\\d+(\\.\\d+)?)|(^(-|\\+)?\\.\\d+)|(^(-|\\+)?\\d+\\.))(e|E)((\\+|-)?\\d+)$";
    private static Pattern numberPattern = Pattern.compile(numberRegex);
    private static Pattern ePattern = Pattern.compile(eRegex);
    public boolean isNumeric (String str) {
        str = str.replace(" ","");
        if (numberPattern.matcher(str).find()) {
            return true;
        }else if(ePattern.matcher(str).find()){
            return true;
        }
        return false;
    }

    @Test
    public void t1(){

        System.out.println(isNumeric("123.45e+6"));
    }
}
