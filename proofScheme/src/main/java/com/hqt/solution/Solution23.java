package com.hqt.solution;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 描述
 * 给一个长度为n链表，若其中包含环，请找出该链表的环的入口结点，否则，返回null。
 * {1,2},{3,4,5}
 */
public class Solution23 {
    /*public ListNode EntryNodeOfLoop(ListNode pHead) {
        List<ListNode> listNodes = new ArrayList<>();
        listNodes.add(pHead);
        while (pHead.next!=null){
            if (listNodes.contains(pHead.next)){
                return pHead.next;
            }
            listNodes.add(pHead.next);
            pHead = pHead.next;
        }
        return null;
    }*/
    public ListNode EntryNodeOfLoop(ListNode pHead) {
        ListNode fast = pHead;
        ListNode low = pHead;
        boolean isRing = false;
        while (fast!=null&&fast.next!=null){
            fast = fast.next.next;
            low = low.next;
            if (fast==low){
                isRing = true;
                break;
            }
        }
        if (!isRing){
            return null;
        }
        while (pHead!=low){
            pHead = pHead.next;
            low = low.next;
        }
        return low;
    }
    @Test
    public void t1(){
        ListNode listNode = new ListNode(1);
        listNode.next = new ListNode(2);
        listNode.next.next = new ListNode(3);
        listNode.next.next.next = new ListNode(4);
        listNode.next.next.next.next = new ListNode(5);
        listNode.next.next.next.next.next = listNode.next.next;
        System.out.println(EntryNodeOfLoop(listNode).val);
    }
}
