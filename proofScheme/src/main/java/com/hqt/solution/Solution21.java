package com.hqt.solution;

import org.junit.Test;

import java.util.Arrays;

/**
 * 描述
 * 输入一个长度为 n 整数数组，实现一个函数来调整该数组中数字的顺序，使得所有的奇数位于数组的前面部分，
 * 所有的偶数位于数组的后面部分，并保证奇数和奇数，偶数和偶数之间的相对位置不变。
 *
 * 示例1
 * 输入：
 * [1,2,3,4]
 *
 * 返回值：
 * [1,3,2,4]
 *
 * 示例2
 * 输入：
 * [2,4,6,5,7]
 *
 * 返回值：
 * [5,7,2,4,6]
 */
public class Solution21 {
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param array int整型一维数组
     * @return int整型一维数组
     */
    public int[] reOrderArray (int[] array) {
            for (int i = 0; i < array.length-1;) {
                if (array[i]%2==0&&array[i+1]%2!=0){
                    array[i] = array[i]+array[i+1];
                    array[i+1] = array[i]-array[i+1];
                    array[i] = array[i]-array[i+1];
                    if (i>0){
                        i--;
                    }else {
                        i++;
                    }
                }else {
                    i++;
                }
            }
        return array;
    }

    @Test
    public void t1(){
        int[] a = {2,4,6,5,7};
        System.out.println(Arrays.toString(reOrderArray(a)));
    }
}
