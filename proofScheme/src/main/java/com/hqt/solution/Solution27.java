package com.hqt.solution;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * 描述
 * 操作给定的二叉树，将其变换为源二叉树的镜像。
 * 输入：
 * {8,6,10,5,7,9,11}
 *
 * 返回值：
 * {8,10,6,11,9,7,5}
 */
public class Solution27 {
    public TreeNode Mirror (TreeNode pRoot) {
        if (pRoot==null){
            return null;
        }
        TreeNode left = pRoot.left;
        TreeNode right = pRoot.right;

        pRoot.left = right;
        pRoot.right = left;
        Mirror(pRoot.left);
        Mirror(pRoot.right);
        return pRoot;
    }
    @Test
    public void t1(){
        TreeNode root = new TreeNode(8);
        root.left = new TreeNode(6);
        root.left.left = new TreeNode(5);
        root.left.right = new TreeNode(7);
        root.right = new TreeNode(10);
        root.right.left = new TreeNode(9);
        root.right.right = new TreeNode(11);
        TreeNode mirror = Mirror(root);
        Map<Integer,String> valMap = new HashMap<>();
        printTreeNode(mirror,0,valMap);
        for (int i = 0; i < valMap.size(); i++) {
            System.out.print(valMap.get(i)+"    ");
        }
    }
    private void printTreeNode(TreeNode treeNode,int index,Map<Integer,String> valMap){
        if (treeNode==null){
            return;
        }
        if (valMap.containsKey(index)){
            valMap.put(index,valMap.get(index)+"    "+treeNode.val);
        }else {
            valMap.put(index,treeNode.val+"");
        }
        printTreeNode(treeNode.left,index+1,valMap);
        printTreeNode(treeNode.right,index+1,valMap);
    }
}
