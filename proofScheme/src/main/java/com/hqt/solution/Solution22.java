package com.hqt.solution;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * 描述
 * 输入一个长度为 n 的链表，设链表中的元素的值为 ai ，返回该链表中倒数第k个节点。
 * 如果该链表长度小于k，请返回一个长度为 0 的链表。
 */
public class Solution22 {
    /**
     * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
     *
     *
     * @param pHead ListNode类
     * @param k int整型
     * @return ListNode类
     */
    public ListNode FindKthToTail (ListNode pHead, int k) {
        if (pHead==null||k <=0){
            return null;
        }
        int length = 1;
        List<ListNode> listNodes = new ArrayList<>();
        while (pHead.next!=null){
            length++;
            listNodes.add(pHead);
            pHead = pHead.next;
        }
        if (length<k){
            return null;
        }else {
            return listNodes.get(length-k);
        }
    }

    @Test
    public void t1(){
        ListNode listNode = new ListNode(1);
        listNode.next = new ListNode(2);
        listNode.next.next = new ListNode(3);
        listNode.next.next.next = new ListNode(4);
        listNode.next.next.next.next = new ListNode(5);
        System.out.println(FindKthToTail(listNode,0));
    }
}
