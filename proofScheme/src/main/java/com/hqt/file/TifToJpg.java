package com.hqt.file;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class TifToJpg {
    @Test
    public void t1() throws IOException {
        String filePath = "D:\\臻善科技资源收藏\\工作文档20220518\\3\\3\\2389.tif";
        /*String fileJpg;
        if (filePath.endsWith(".tif")){
            fileJpg = filePath.replace(".tif",".jpg");
            tiffToJpg(filePath,fileJpg);
        }else {
            fileJpg = filePath;
        }*/
        base64ToFile(fileToBase64("D:\\臻善科技资源收藏\\工作文档20220518\\3\\3\\2389.jpg"),"D:\\臻善科技资源收藏\\工作文档20220518\\3\\3\\2389_1.jpg");
        /*FileInputStream fileInputStream = new FileInputStream(new File(fileJpg));
        String fileInputStr = IOUtils.toString(fileInputStream, StandardCharsets.UTF_8);
        System.out.println(fileInputStr);*/
    }
    /**
     * base64字符串转文件
     * @param base64Str
     * @param targetFilePath
     * @return
     */
    public static void base64ToFile(String base64Str, String targetFilePath) {
        byte[] buffer = Base64.decodeBase64(base64Str);
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(targetFilePath);
            out.write(buffer);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != out) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * @Description: 文件转为base64字符串。filePath：文件路径
     */
    public static String fileToBase64(String filePath) throws IOException {
        File file = new File(filePath);
        FileInputStream inputFile = null;
        byte[] buffer = null;
        try {
            inputFile = new FileInputStream(file);
            buffer = new byte[(int) file.length()];
            inputFile.read(buffer);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != inputFile) {
                inputFile.close();
            }
        }
        byte[] bs = Base64.encodeBase64(buffer);
        return new String(bs);
    }

    /**
     * 	将tiff图片转化为jpg，生成新的文件
     * @param oldPath	原图片的全路径
     * @param newPath	生成新的图片的全路径
     */
    public void tiffToJpg(String oldPath,String newPath) {
        try {
            BufferedImage bufferegImage = ImageIO.read(new File(oldPath));
            ImageIO.write(bufferegImage, "jpg", new File(newPath));//可以是png等其它图片格式

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
