package com.hqt.file;

import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.zip.ZipFile;

public class FileTest {
    @Test
    public void t1() {
        File dir = new File("D:\\opt\\opengis\\data\\export\\11653459191542");
        File[] files = dir.listFiles();
        deleteDirectory(dir);
    }
    @Test
    public void t2() throws IOException {
        File file = new File("D:\\臻善科技资源收藏\\工作文档20220518\\440703003001GX00004F0001.zip");
        //File file = new File("D:\\臻善科技资源收藏\\工作文档20220518\\440703003001GX00004F0001(1).zip");
        ZipFile zFile = new ZipFile(file);

        System.out.println(zFile.size());
    }
    @Test
    public void t3() throws IOException {
        Path sourcePath = Paths.get("D:/臻善科技资源收藏/工作文档20231019/台山市粤智助自助机对接/申请人1-111-44078120231024000022.pdf");
        Path destPath = Paths.get("E:/yzz/申请人1-111-44078120231024000022.pdf");
        Files.move(sourcePath, destPath, StandardCopyOption.REPLACE_EXISTING);
    }

    public static void deleteDirectory(File file) {

        if (file.isFile()) {
            file.delete();//清理文件
        } else {

            File list[] = file.listFiles();

            if (list != null) {

                for (File f : list) {

                    deleteDirectory(f);

                }
                file.delete();//清理目录
            }
        }
    }
}

