package com.hqt.file.template;

import com.aspose.words.Document;
import com.aspose.words.FontSettings;
import com.deepoove.poi.XWPFTemplate;
import com.deepoove.poi.config.Configure;
import com.deepoove.poi.config.ConfigureBuilder;
import com.deepoove.poi.data.PictureRenderData;
import com.deepoove.poi.policy.HackLoopTableRenderPolicy;
import com.fasterxml.jackson.core.type.TypeReference;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.hqt.utils.GetKeyWordPosition;
import com.hqt.utils.ImageUtils;
import com.hqt.utils.JsonUtil;
import com.hqt.utils.ZxingCodeUtils;
import org.apache.commons.io.FileUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class WordTempToPdfTest {
    @Test
    public void t1() throws Exception {
        //测试数据，读取文件获取
        Map<String,Object> map = readDataByFile();
        wordTempToPdf(map);
    }
    public void wordTempToPdf(Map<String, Object> dataMap) throws Exception {
        addBarcode(dataMap);
        //获取表格父标签，表格父标签需要特殊处理，设定表示此标签不是文本标签，是表格
        Configure config;
        HackLoopTableRenderPolicy policy = new HackLoopTableRenderPolicy();
        ConfigureBuilder configureBuilder = Configure.newBuilder();
        configureBuilder.bind("{{人员}}".replace("{{", "").replace("}}", ""), policy);
        configureBuilder.bind("{{不动产信息}}".replace("{{", "").replace("}}", ""), policy);
        config = configureBuilder.build();
        //模板文件读取，测试采用本地
        //InputStream in = new FileInputStream(new File("D:\\第二阶段学习笔记\\word模板.docx"));
        InputStream in = new FileInputStream(new File("D:\\臻善科技资源收藏\\工作文档20231019\\台山市粤智助自助机对接\\个人或家庭住房证明模板.docx"));
        XWPFTemplate template = XWPFTemplate.compile(in, config);
        //将数据写入模板
        template.render(dataMap);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        template.write(baos);
        template.close();
        ByteArrayInputStream parse = parse(baos);
        baos.close();
        ByteArrayOutputStream baost = new ByteArrayOutputStream();
        wordToPDF(parse, baost);
        byte[] bytes = baost.toByteArray();
        baost.close();
        String pdfPath = "D:\\臻善科技资源收藏\\工作文档20231019\\台山市粤智助自助机对接\\"+System.currentTimeMillis()+".pdf";
        String newPdfPath = "D:\\臻善科技资源收藏\\工作文档20231019\\台山市粤智助自助机对接\\"+System.currentTimeMillis()+".pdf";
        File pdfFile = new File(pdfPath);
        FileUtils.writeByteArrayToFile(pdfFile, bytes);
        addImage(pdfPath,newPdfPath);
    }
    public static void wordToPDF(InputStream in, OutputStream out) {
        try {
            long old = System.currentTimeMillis();
            Document doc = new Document(in);
            doc.save(out, 40);
            long now = System.currentTimeMillis();
            out.close();
            System.out.println("word转pdf成功，共耗时：" + (now - old) / 1000.0D + "秒");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * outputStream转inputStream
     *
     * @param out
     * @return
     * @throws Exception
     */
    public ByteArrayInputStream parse(OutputStream out) throws Exception {
        ByteArrayOutputStream baos = (ByteArrayOutputStream) out;
        ByteArrayInputStream swapStream = new ByteArrayInputStream(baos.toByteArray());
        return swapStream;
    }
    public Map<String,Object> readDataByFile() throws IOException {
        File file = new File("D:\\第二阶段学习笔记\\datatest.json");
        InputStream in = new FileInputStream(file);
        byte[] b = new byte[in.available()];
        int len = 0;
        //全部读取的内容都使用temp接收
        int temp = 0;
        //当没有读取完时，继续读取
        while ((temp = in.read()) != -1) {
            b[len] = (byte) temp;
            len++;
        }
        in .close();
        String content = new String(b, 0, len);
        //System.out.println(content);
        return JsonUtil.jsonStringToObject(content,new TypeReference<Map<String,Object>>(){});
    }
    private void addBarcode(Map<String,Object> map) throws WriterException {
        String bh = map.get("编号").toString();
        int width = 100,height = 20;
        BufferedImage bufferedImage;
        bufferedImage = ZxingCodeUtils.barcodeEncode(bh.toString(), BarcodeFormat.CODE_128, width, height);
        Object value = new PictureRenderData(width, height, ".png", bufferedImage);
        map.put("条形码",value);
    }
    private void addImage(String pdfPath,String newPdfPath) throws IOException {
        modifyPdf(pdfPath, newPdfPath,"D:\\臻善科技资源收藏\\工作文档20231019\\台山市粤智助自助机对接\\不动产登记信息查询专用章2.png");
    }

    /**
     * 将印章插入pdf中
     * @param pdfLsPath  模板PDF
     * @param pdfOutPath 模板生成后的PDF
     * @param gongz 印章图片
     * @throws IOException
     */
    public static void modifyPdf(String pdfLsPath,String pdfOutPath,String gongz) throws IOException {
        List<Float> positions = getKeyWordXY(pdfLsPath,"(电子印章)");
        FileInputStream pdfInputStream = new FileInputStream(pdfLsPath);
        PDDocument  pdDocument = PDDocument.load(pdfInputStream);
        PDPage page = pdDocument.getPage(0);
        PDImageXObject image = PDImageXObject.createFromFile(gongz, pdDocument);
        PDPageContentStream pageStream = new PDPageContentStream(pdDocument, page,PDPageContentStream.AppendMode.APPEND,false,false);

        // 6、pageStream对象绘制图片位置及大小，已PDF文件右下角为原点（x,y）是图片左下角左边，width、height是图片的长和宽
        pageStream.drawImage(image, positions.get(1)-30, positions.get(2)-60,100,100);
        pageStream.close();
        pdDocument.save(pdfOutPath);
        pdDocument.close();
        pdfInputStream.close();
    }
    private static List<Float> getKeyWordXY(String pdfLsPath,String keyWord) throws IOException {
        List<Float> positions = GetKeyWordPosition.findKeywordPostions(new File(pdfLsPath),keyWord);
        return positions;
    }
}
