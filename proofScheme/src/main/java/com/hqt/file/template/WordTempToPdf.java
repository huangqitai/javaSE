package com.hqt.file.template;

import com.aspose.words.Document;
import com.aspose.words.FontSettings;
import com.deepoove.poi.XWPFTemplate;
import com.deepoove.poi.config.Configure;
import com.deepoove.poi.config.ConfigureBuilder;
import com.deepoove.poi.data.PictureRenderData;
import com.deepoove.poi.policy.HackLoopTableRenderPolicy;
import com.fasterxml.jackson.core.type.TypeReference;
import com.hqt.utils.JsonUtil;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class WordTempToPdf {

    @Test
    public void t1() throws Exception {
        //测试数据，读取文件获取
        Map<String,Object> map = readDataByFile();
        //测试模板与数据节点、字段表关联关系，此处测试使用读取文本，实际使用中可查询数据库配置
        List<TemplateBind> templateBindList = readFileToTemplateBind();
        //字段表数据，此处测试使用读取文本，实际使用中可查询数据库配置
        List<QueryColumn> queryColumnList = readFileToQueryColumn();
        //节点表数据，此处测试使用读取文本，实际使用中可查询数据库配置
        List<Rs> rsList = readFileToRs();
        wordTempToPdf(map,templateBindList,queryColumnList,rsList);
    }
    public void wordTempToPdf(Map<String, Object> dataMap, List<TemplateBind> templateBindList, List<QueryColumn> queryColumnList, List<Rs> rsList) throws Exception {
        //获取表格父标签，表格父标签需要特殊处理，设定表示此标签不是文本标签，是表格
        List<TemplateBind> tableLabelList = templateBindList.stream().filter(t -> t.getLabelType() != null && t.getLabelType().equals(LabelType.TABLE.getVal().toString())).collect(Collectors.toList());
        Configure config;
        HackLoopTableRenderPolicy policy = new HackLoopTableRenderPolicy();
        ConfigureBuilder configureBuilder = Configure.newBuilder();
        for (TemplateBind templateBind : tableLabelList) {
            configureBuilder.bind(templateBind.getTempLabelName().replace("{{", "").replace("}}", ""), policy);
        }
        config = configureBuilder.build();
        //模板文件读取，测试采用本地
        InputStream in = new FileInputStream(new File("D:\\臻善科技资源收藏\\工作文档20230213\\输出表单说明+示例代码+测试数据\\1658796956016-1658796954821.docx"));
        XWPFTemplate template = XWPFTemplate.compile(in, config);
        //文件标签数据根据配置关系组建
        Map<String, Object> map = new HashMap<>(getTextLabelData(dataMap, queryColumnList, templateBindList, rsList));
        //表格标签数据根据配置关系组建
        map.putAll(getListLabelData(dataMap,queryColumnList,templateBindList,rsList));
        System.out.println(JsonUtil.toJsonString(map));
        //将数据写入模板
        template.render(map);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        template.write(baos);
        template.close();
        ByteArrayInputStream parse = parse(baos);
        baos.close();
        ByteArrayOutputStream baost = new ByteArrayOutputStream();
        wordToPDF(parse, baost);
        byte[] bytes = baost.toByteArray();
        baost.close();
        String pdfPath = "D:\\臻善科技资源收藏\\工作文档20230213\\输出表单说明+示例代码+测试数据\\"+System.currentTimeMillis()+".pdf";
        File pdfFile = new File(pdfPath);
        FileUtils.writeByteArrayToFile(pdfFile, bytes);
    }

    /**
     * 文本类标签数据生成
     * @param templateBinds
     * @return
     */
    private Map<String, Object> getTextLabelData(Map<String, Object> dataMap, List<QueryColumn> queryColumnList, List<TemplateBind> templateBinds,List<Rs> rsList) throws IOException {
        Map<String, Object> map = new HashMap<>();
        List<TemplateBind> collect = templateBinds.stream().filter(temp -> temp.getLabelType().equals(LabelType.TEXT.getVal().toString())).collect(Collectors.toList());
        for (TemplateBind templateBind : collect) {
            Rs rs = rsList.stream().filter(temp -> temp.getRsGuid().equals(templateBind.getRsGuid())).findFirst().get();
            String rsName = rs.getRsName();
            if (rs.getRsType().equals(RsType.MAP.getVal())) {
                Map<String,Object> rsDataMap = (Map<String, Object>) dataMap.get(rsName);
                QueryColumn queryColumn = queryColumnList.stream().filter(temp -> temp.getColumnGuid().equals(templateBind.getColumnGuid())).findFirst().orElse(null);
                Object value = convert(rsDataMap.get(queryColumn.getFieldLetterName()), templateBind,null);
                map.put(templateBind.getTempLabelName().replace("{{", "").replace("}}", ""), value);
            }
        }
        return map;
    }
    /**
     * 列表类标签数据生成
     * @param templateBinds
     * @return
     */
    private Map<String, Object> getListLabelData(Map<String, Object> dataMap, List<QueryColumn> queryColumnList, List<TemplateBind> templateBinds,List<Rs> rsList) throws IOException {
        Map<String, Object> map = new HashMap();
        for (Rs rs:rsList){
            String rsName = rs.getRsName();
            if (rs.getRsType().equals(RsType.LIST.getVal())) {
                List<Map<String, Object>> rsDataList = (List<Map<String, Object>>) dataMap.get(rsName);
                List<TemplateBind> currentRsLabelList = templateBinds.stream().filter(t -> t.getLabelType() != null && t.getLabelType().equals(LabelType.LIST.getVal().toString()) && t.getRsGuid().equals(rs.getRsGuid())).collect(Collectors.toList());
                TemplateBind templateBind = currentRsLabelList.get(0);
                List<Map<String, Object>> mapList = getTableLabelData(rsDataList,rs,queryColumnList,currentRsLabelList);
                map.put(templateBind.getParentLabelName().replace("{{", "").replace("}}", ""), mapList);
            }
        }
        return map;
    }
    private List<Map<String, Object>> getTableLabelData(List<Map<String, Object>> rsDataList, Rs rs, List<QueryColumn> queryColumnList, List<TemplateBind> templateBinds) throws IOException {
        List<Map<String, Object>> mapList = new ArrayList<>();

        int index = 0;
        for (Map<String, Object> data : rsDataList) {
            Map<String, Object> map = new HashMap<>();
            for (int i = 0; i < templateBinds.size(); i++) {
                TemplateBind bind = templateBinds.get(i);
                String lableName = bind.getTempLabelName();
                QueryColumn queryColumn = queryColumnList.stream().filter(temp -> temp.getColumnGuid().equals(bind.getColumnGuid())).findFirst().orElse(null);
                if (queryColumn==null){
                    map.put(bind.getTempLabelName().replace("[", "").replace("]", ""), convert(null,bind, index));
                }else {
                    map.put(bind.getTempLabelName().replace("[", "").replace("]", ""), convert(data.get(queryColumn.getFieldLetterName()),bind, index));
                }
            }
            mapList.add(map);
            index++;
        }

        return mapList;
    }
    public static void wordToPDF(InputStream in, OutputStream out) {
        try {
            long old = System.currentTimeMillis();
            Document doc = new Document(in);

            boolean isWindows = System.getProperties().getProperty("os.name").toUpperCase().indexOf("WINDOWS") != -1;
            if (!isWindows) {
                String fontpath = "/usr/share/fonts";
                System.out.println("fontpath: " + fontpath);
                FontSettings.getDefaultInstance().setFontsFolder(fontpath, false);
            }
            doc.save(out, 40);
            long now = System.currentTimeMillis();
            out.close();
            System.out.println("word转pdf成功，共耗时：" + (now - old) / 1000.0D + "秒");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * outputStream转inputStream
     *
     * @param out
     * @return
     * @throws Exception
     */
    public ByteArrayInputStream parse(OutputStream out) throws Exception {
        ByteArrayOutputStream baos = (ByteArrayOutputStream) out;
        ByteArrayInputStream swapStream = new ByteArrayInputStream(baos.toByteArray());
        return swapStream;
    }
    public List<TemplateBind> readFileToTemplateBind() throws IOException {
        File file = new File("D:\\臻善科技资源收藏\\工作文档20230213\\输出表单说明+示例代码+测试数据\\tempbind.json");
        InputStream in = new FileInputStream(file);
        byte[] b = new byte[in.available()];
        int len = 0;
        //全部读取的内容都使用temp接收
        int temp = 0;
        //当没有读取完时，继续读取
        while ((temp = in.read()) != -1) {
            b[len] = (byte) temp;
            len++;
        }
        in .close();
        String content = new String(b, 0, len);
        //System.out.println(content);
        return JsonUtil.jsonStringToObject(content,new TypeReference<List<TemplateBind>>(){});
    }
  public List<QueryColumn> readFileToQueryColumn() throws IOException {
        File file = new File("D:\\臻善科技资源收藏\\工作文档20230213\\输出表单说明+示例代码+测试数据\\column.json");
        InputStream in = new FileInputStream(file);
        byte[] b = new byte[in.available()];
        int len = 0;
        //全部读取的内容都使用temp接收
        int temp = 0;
        //当没有读取完时，继续读取
        while ((temp = in.read()) != -1) {
            b[len] = (byte) temp;
            len++;
        }
        in .close();
        String content = new String(b, 0, len);
        //System.out.println(content);
        return JsonUtil.jsonStringToObject(content,new TypeReference<List<QueryColumn>>(){});
    }
    public List<Rs> readFileToRs() throws IOException {
            File file = new File("D:\\臻善科技资源收藏\\工作文档20230213\\输出表单说明+示例代码+测试数据\\rs.json");
            InputStream in = new FileInputStream(file);
            byte[] b = new byte[in.available()];
            int len = 0;
            //全部读取的内容都使用temp接收
            int temp = 0;
            //当没有读取完时，继续读取
            while ((temp = in.read()) != -1) {
                b[len] = (byte) temp;
                len++;
            }
            in .close();
            String content = new String(b, 0, len);
            //System.out.println(content);
            return JsonUtil.jsonStringToObject(content,new TypeReference<List<Rs>>(){});
    }
    public Map<String,Object> readDataByFile() throws IOException {
        File file = new File("D:\\臻善科技资源收藏\\工作文档20230213\\输出表单说明+示例代码+测试数据\\datatest.json");
        InputStream in = new FileInputStream(file);
        byte[] b = new byte[in.available()];
        int len = 0;
        //全部读取的内容都使用temp接收
        int temp = 0;
        //当没有读取完时，继续读取
        while ((temp = in.read()) != -1) {
            b[len] = (byte) temp;
            len++;
        }
        in .close();
        String content = new String(b, 0, len);
        //System.out.println(content);
        return JsonUtil.jsonStringToObject(content,new TypeReference<Map<String,Object>>(){});
    }
    /**
     * 数据格式转换
     *
     * @param value
     * @param templateBind
     * @return
     */
    public Object convert(Object value, TemplateBind templateBind,Integer index) throws IOException {

        Object newValue = null;
        String formatType = templateBind.getFormatType();
        if (StringUtils.isEmpty(formatType)) {
            formatType = "0";
        }
        Integer formatTypeCode = Integer.parseInt(formatType);
        DataFormatType type = EnumUtils.getEnumByVal(formatTypeCode, DataFormatType.class);
        if (value == null&&type!=null&&!type.getVal().equals(DataFormatType.SEQUENCE.getVal())) {
            return null;
        }
        switch (Objects.requireNonNull(type)) {
            case NORMAL:
                newValue = value.toString();
                break;
            case NUMBER:
                //例:"###.##"
                DecimalFormat df = new DecimalFormat(templateBind.getFormat());
                //Number number = (Number) value;
                //number.
                BigDecimal bigDecimal = new BigDecimal(value.toString());
                newValue = df.format(bigDecimal);
                break;
            case MONEY:
                String moneySign = templateBind.getFormat().substring(0, 1);
                DecimalFormat df2 = new DecimalFormat(templateBind.getFormat().substring(1));
                newValue = moneySign + df2.format(value);
                break;
            case PERCENTAGE:
                DecimalFormat df3 = new DecimalFormat(templateBind.getFormat());
                newValue = df3.format(value);
                break;
            case DATA:
                try {
                    if(StringUtils.isBlank(templateBind.getFormat())){
                        newValue = new SimpleDateFormat(SystemConstants.DATE_FORMAT_YMD).format(new SimpleDateFormat("yyyy-MM-dd").parse(value.toString()));
                    }else {
                        newValue = new SimpleDateFormat(templateBind.getFormat()).format(new SimpleDateFormat("yyyy-MM-dd").parse(value.toString()));
                    }
                } catch (Exception e) {
                    newValue = new SimpleDateFormat(SystemConstants.DATE_FORMAT_YMD).format(new Date());
                }
                break;
            case TIME:
                //例:yyyy年MM月dd日
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern(templateBind.getFormat());
                try {
                    DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern(SystemConstants.DATE_FORMAT_YMD);
                    LocalDate localDate = LocalDate.parse(value.toString(), formatter1);
                    newValue = localDate.format(formatter);
                } catch (Exception e) {
                    DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern(SystemConstants.DATE_FORMAT_Y_M_D_H_M_S);
                    LocalDateTime localDate = LocalDateTime.parse(value.toString(), formatter1);
                    newValue = localDate.format(formatter);
                }
                break;
            case SEQUENCE:
                //序号，为数组角标加1
                if (index != null) {
                    newValue = index + 1 + "";
                } else {
                    newValue = "";
                }
                break;
        }
        return newValue;
    }
}
