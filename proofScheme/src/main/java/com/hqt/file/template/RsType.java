package com.hqt.file.template;

/**
 * 数据集类型枚举
 *
 * @author HHB 2018/4/9
 */
public enum RsType {
	MAP(1, "Map"),
	LIST(2, "List");

	private int val; // 描述编码
	private String msg; // 描述信息

	RsType(int val, String msg) {
		this.val = val;
		this.msg = msg;
	}

	public int getVal() {
		return val;
	}

	public String getMsg() {
		return msg;
	}
}
