package com.hqt.file.template;

/**
 * word中标签类型
 */
public enum LabelType {

    TEXT(0,"文本标签"),
    TABLE(1,"表格父标签"),
    LIST(2,"表格标签");
    /**
     * 描述编码
     */
    private int val;
    /**
     *  描述编码
     */
    private String msg;

    LabelType(Integer val, String msg) {
        this.val = val;
        this.msg = msg;
    }

    public Integer getVal() {
        return val;
    }

    public String getMsg() {
        return msg;
    }
}
