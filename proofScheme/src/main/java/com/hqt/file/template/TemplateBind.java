package com.hqt.file.template;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TemplateBind {
    @JsonProperty(value = "relation_guid")
    private String relationGuid;
    @JsonProperty(value = "temp_guid")
    private String tempGuid;
    @JsonProperty(value = "query_guid")
    private String queryGuid;
    @JsonProperty(value = "rs_guid")
    private String rsGuid;
    @JsonProperty(value = "column_guid")
    private String columnGuid;
    @JsonProperty(value = "temp_label_name")
    private String tempLabelName;
    @JsonProperty(value = "format_type")
    private String formatType;
    @JsonProperty(value = "format")
    private String format;
    @JsonProperty(value = "filter_column")
    private String filterColumn;
    @JsonProperty(value = "filter_value")
    private String filterValue;
    @JsonProperty(value = "query_condition")
    private String queryCondition;
    @JsonProperty(value = "query_value")
    private String queryValue;
    @JsonProperty(value = "label_type")
    private String labelType;
    @JsonProperty(value = "parent_label_name")
    private String parentLabelName;
    @JsonProperty(value = "sort")
    private int sort;

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public String getRelationGuid() {
        return relationGuid;
    }

    public void setRelationGuid(String relationGuid) {
        this.relationGuid = relationGuid;
    }

    public String getTempGuid() {
        return tempGuid;
    }

    public void setTempGuid(String tempGuid) {
        this.tempGuid = tempGuid;
    }

    public String getQueryGuid() {
        return queryGuid;
    }

    public void setQueryGuid(String queryGuid) {
        this.queryGuid = queryGuid;
    }

    public String getRsGuid() {
        return rsGuid;
    }

    public void setRsGuid(String rsGuid) {
        this.rsGuid = rsGuid;
    }

    public String getColumnGuid() {
        return columnGuid;
    }

    public void setColumnGuid(String columnGuid) {
        this.columnGuid = columnGuid;
    }

    public String getTempLabelName() {
        return tempLabelName;
    }

    public void setTempLabelName(String tempLabelName) {
        this.tempLabelName = tempLabelName;
    }

    public String getFormatType() {
        return formatType;
    }

    public void setFormatType(String formatType) {
        this.formatType = formatType;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getFilterColumn() {
        return filterColumn;
    }

    public void setFilterColumn(String filterColumn) {
        this.filterColumn = filterColumn;
    }

    public String getFilterValue() {
        return filterValue;
    }

    public void setFilterValue(String filterValue) {
        this.filterValue = filterValue;
    }

    public String getQueryCondition() {
        return queryCondition;
    }

    public void setQueryCondition(String queryCondition) {
        this.queryCondition = queryCondition;
    }

    public String getQueryValue() {
        return queryValue;
    }

    public void setQueryValue(String queryValue) {
        this.queryValue = queryValue;
    }

    public String getLabelType() {
        return labelType;
    }

    public void setLabelType(String labelType) {
        this.labelType = labelType;
    }

    public String getParentLabelName() {
        return parentLabelName;
    }

    public void setParentLabelName(String parentLabelName) {
        this.parentLabelName = parentLabelName;
    }

    /**
     * 重写equal函数用来比较用
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if(o == null) {
            return true;
        }
        TemplateBind obj = (TemplateBind) o;
        if(tempLabelName.equals(obj.tempLabelName)) {
            return true;
        }
        return false;
    }
}