package com.hqt.file.template;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class QueryColumn implements Serializable {
	@JsonProperty(value = "COLUMN_GUID")
	private String columnGuid;
	@JsonProperty(value = "FIELD_LETTER_NAME")
	private String fieldLetterName;

	public String getColumnGuid() {
		return columnGuid;
	}

	public void setColumnGuid(String columnGuid) {
		this.columnGuid = columnGuid;
	}

	public String getFieldLetterName() {
		return fieldLetterName;
	}

	public void setFieldLetterName(String fieldLetterName) {
		this.fieldLetterName = fieldLetterName;
	}
}
