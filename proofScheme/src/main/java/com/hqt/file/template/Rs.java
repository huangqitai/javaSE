package com.hqt.file.template;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Rs implements Serializable {
	@JsonProperty(value = "RS_GUID")
	private String rsGuid;
	@JsonProperty(value = "RS_NAME")
	private String rsName;
	@JsonProperty(value = "RS_TYPE")
	private Integer rsType;

	public String getRsGuid() {
		return rsGuid;
	}

	public void setRsGuid(String rsGuid) {
		this.rsGuid = rsGuid;
	}

	public String getRsName() {
		return rsName;
	}

	public void setRsName(String rsName) {
		this.rsName = rsName;
	}

	public Integer getRsType() {
		return rsType;
	}

	public void setRsType(Integer rsType) {
		this.rsType = rsType;
	}
}
