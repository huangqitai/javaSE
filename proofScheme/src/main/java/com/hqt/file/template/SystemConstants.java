package com.hqt.file.template;

/**
 * 常量参数配置
 */
public class SystemConstants {

	// 日期格式
	public static final String DATE_FORMAT_Y = "yyyy";
	public static final String DATE_FORMAT_YM = "yyyy-MM";
	public static final String DATE_FORMAT_YMD = "yyyy-MM-dd";
	public static final String DATE_FORMAT_YMD2 = "yyyy年MM月dd日";
	public static final String DATE_FORMAT_Y_M_D_H_M_S = "yyyy-MM-dd HH:mm:ss";
	public static final String DATE_FORMAT_Y_M_D_H_M_S_ORACLE = "yyyy-MM-dd hh24:mi:ss";

}
