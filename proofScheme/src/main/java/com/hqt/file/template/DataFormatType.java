package com.hqt.file.template;


/**
 * word数据格式
 */
public enum DataFormatType implements EnumBase {

    NORMAL(0,"常规"),
    NUMBER(1,"数字"),
    MONEY(2,"货币"),
    PERCENTAGE(3,"百分比"),
    DATA(4,"日期"),
    TIME(5,"时间"),
    CHINESES_NUMBER(6,"中文数字"),
    BARCODE(7,"条形码"),
    EXPRESSION(8,"表达式"),
    IMAGE(9, "图片"),
    QRCODE(10, "二维码"),
    STAMP(11, "电子签章"),
    SEQUENCE(12, "序号")
    ;
    /**
     * 描述编码
     */
    private int val;
    /**
     *  描述编码
     */
    private String msg;

    DataFormatType(Integer val, String msg) {
        this.val = val;
        this.msg = msg;
    }

    public Integer getVal() {
        return val;
    }

    public String getMsg() {
        return msg;
    }
}
