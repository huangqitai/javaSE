package com.hqt.file.template;

public interface EnumBase {

    Integer getVal();

    String getMsg();
}
