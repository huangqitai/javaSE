package com.hqt.file.template;

public class EnumUtils {

    public static <T extends EnumBase> T getEnumByVal(Integer code, Class<T> enumClass) {
        for (T t : enumClass.getEnumConstants()) {
            if (t.getVal().equals(code)) {
                return t;
            }
        }
        return null;
    }
}
