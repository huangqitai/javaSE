package com.hqt.paike;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Contants {
    public final static List<String> ROOMS = new ArrayList<>(Arrays.asList("101","102","103","201","202","301","302"));
    public final static LinkedList<String> CLASSS = new LinkedList<>(Arrays.asList("语文","数学","英语","化学","物理","生物","政治","历史","地理"));
    public final static Map<String,List<String>> GRADE_MAP = new HashMap<String,List<String>>(){{
        GRADE_MAP.put("1",Arrays.asList("1","2","3"));
        GRADE_MAP.put("2",Arrays.asList("1","2"));
        GRADE_MAP.put("3",Arrays.asList("1","2"));

    }};
    public final static Map<String,String> GRADE_ROOM_MAP = new HashMap<String,String>(){{
        GRADE_ROOM_MAP.put("1-1","101");
        GRADE_ROOM_MAP.put("1-2","102");
        GRADE_ROOM_MAP.put("1-3","103");
        GRADE_ROOM_MAP.put("2-1","201");
        GRADE_ROOM_MAP.put("2-2","202");
        GRADE_ROOM_MAP.put("3-1","301");
        GRADE_ROOM_MAP.put("3-2","302");
    }};
    public final static Map<String,List<String>> TEACHER_MAP = new HashMap<String,List<String>>(){{
        List<String> yws = new ArrayList<>(Arrays.asList("语文1","语文2","语文3"));
        TEACHER_MAP.put("语文",yws);
        List<String> sxs = new ArrayList<>(Arrays.asList("数学1","数学2","数学3","数学4"));
        TEACHER_MAP.put("数学",sxs);
        List<String> yys = new ArrayList<>(Arrays.asList("英语1","英语2"));
        TEACHER_MAP.put("英语",yys);
        List<String> hss = new ArrayList<>(Arrays.asList("化学1","化学2"));
        TEACHER_MAP.put("化学",hss);
        List<String> wls = new ArrayList<>(Arrays.asList("物理1","物理2"));
        TEACHER_MAP.put("物理",wls);
        List<String> sws = new ArrayList<>(Arrays.asList("生物1","生物2"));
        TEACHER_MAP.put("生物",sws);
        List<String> zzs = new ArrayList<>(Arrays.asList("政治1","政治2"));
        TEACHER_MAP.put("政治",zzs);
        List<String> lss = new ArrayList<>(Arrays.asList("历史1","历史2"));
        TEACHER_MAP.put("历史",lss);
        List<String> dls = new ArrayList<>(Arrays.asList("地理1","地理2"));
        TEACHER_MAP.put("地理",dls);
    }};
}
