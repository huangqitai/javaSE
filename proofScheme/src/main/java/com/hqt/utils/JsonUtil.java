package com.hqt.utils;



import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.Map;

public class JsonUtil {
    public static HashMap<String,Object> jsonStringToMap(String jsonString){
        return jsonStringToObject(jsonString,new TypeReference<HashMap<String, Object>>(){});
    }
    public static <T> T jsonStringToObject(String json, TypeReference<T> type)
    {
        T result;
        try
        {
            result = getMapper(true).readValue(json, type);
        }
        catch(Exception e)
        {

            e.printStackTrace();
            result = null;
        }
        return result;
    }

    public static String toJsonString(Object value)
    {
        if(value==null) return "null";

        String result;
        try
        {
            result = getMapper(false).writeValueAsString(value);
        }
        catch(Exception e)
        {
            e.printStackTrace();
            result = "";
        }
        return result;
    }
    private static ObjectMapper getMapper(boolean state)
    {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, !state);
        return mapper;
    }

    public static Map<String, String> jsonStringToMapString(String jsonString) {
        return jsonStringToObject(jsonString,new TypeReference<HashMap<String, String>>(){});
    }
}
