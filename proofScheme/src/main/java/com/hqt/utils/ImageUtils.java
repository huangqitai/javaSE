package com.hqt.utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

/**
 * 图片处理 工具类
 *
 */
public class ImageUtils {

    /**
     * 剪裁掉图片四周空白部分
     *
     * @param bufferedImage
     * @return
     */
    public static BufferedImage cutImageBlank(BufferedImage bufferedImage) {
        int x = 0, y = 0;
        int width = 0, height = 0;
        int flag = 0;

        // 上
        for (int i = 0; i < bufferedImage.getHeight(); i++) {
            for (int j = 0; j < bufferedImage.getWidth(); j++) {
                if (bufferedImage.getRGB(j, i) != Color.WHITE.getRGB()) {
                    flag++;
                    y = i;
                    break;
                }
            }
            if (flag != 0) {
                flag = 0;
                break;
            }
        }

        // 下
        for (int i = bufferedImage.getHeight() - 1; i >= 0; i--) {
            for (int j = 0; j < bufferedImage.getWidth(); j++) {
                if (bufferedImage.getRGB(j, i) != Color.WHITE.getRGB()) {
                    flag++;
                    height = i - y;
                    break;
                }
            }
            if (flag != 0) {
                flag = 0;
                break;
            }
        }

        // 左
        for (int i = 0; i < bufferedImage.getWidth(); i++) {
            for (int j = 0; j < bufferedImage.getHeight(); j++) {
                if (bufferedImage.getRGB(i, j) != Color.WHITE.getRGB()) {
                    flag++;
                    x = i;
                    break;
                }
            }
            if (flag != 0) {
                flag = 0;
                break;
            }
        }

        // 右
        for (int i = bufferedImage.getWidth() - 1; i >= 0; i--) {
            for (int j = 0; j < bufferedImage.getHeight(); j++) {
                if (bufferedImage.getRGB(i, j) != Color.WHITE.getRGB()) {
                    flag++;
                    width = i - x;
                    break;
                }
            }
            if (flag != 0) {
                flag = 0;
                break;
            }
        }

        return bufferedImage.getSubimage(x, y, width, height);
    }

    public static BufferedImage byteToBufferedImage(byte[] data) throws IOException{
        ByteArrayInputStream inputStream = new ByteArrayInputStream(data);
        BufferedImage bufferedImage = ImageIO.read(inputStream);
        return bufferedImage;
    }

    public static void main(String[] args) throws IOException {
        BufferedImage bufferedImage = ImageIO.read(new File("test.png"));
        bufferedImage = cutImageBlank(bufferedImage);
        ImageIO.write(bufferedImage, "png", new File("result.png"));
    }
}
