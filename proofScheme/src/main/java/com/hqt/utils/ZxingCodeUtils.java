package com.hqt.utils;

import com.google.zxing.*;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Hashtable;

/**
 * Zxing条形码二维码 工具类
 *
 * @author huanluncai
 * @date 2020-12-09
 */
public class ZxingCodeUtils {

    /**
     * 生成条形码
     *
     * @param contents 内容
     * @param barcodeFormat 编码格式
     * @param width 宽度
     * @param height 高度
     * @return
     * @throws WriterException
     */
    public static BufferedImage barcodeEncode(String contents, BarcodeFormat barcodeFormat, int width, int height) throws WriterException {
        // 使用CODE_128编码格式进行编码
        BitMatrix bitMatrix = new MultiFormatWriter().encode(contents,
                barcodeFormat, width, height, null);
        // 生成BufferedImage
        return MatrixToImageWriter.toBufferedImage(bitMatrix);
    }

    /**
     * 生成二维码
     *
     * @param contents 内容
     * @param width 宽度
     * @param height 高度
     * @return
     * @throws WriterException
     */
    public static BufferedImage qRCodeEncode(String contents, int width, int height) throws WriterException {
        Hashtable<EncodeHintType, Object> hints = new Hashtable();
        // 指定纠错等级
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
        // 指定显示格式为GBK
        hints.put(EncodeHintType.CHARACTER_SET, "GBK");

        BitMatrix bitMatrix = new MultiFormatWriter().encode(contents,
                BarcodeFormat.QR_CODE, width, height, hints);
        // 生成BufferedImage
        return MatrixToImageWriter.toBufferedImage(bitMatrix);
    }

    /**
     * 校验条形码
     *
     * @param s
     * @return
     * @throws FormatException
     */
    public static boolean checkStandardUPCEANChecksum(CharSequence s) throws FormatException {
        int length = s.length();
        if (length == 0) {
            return false;
        }

        int sum = 0;
        for (int i = length - 2; i >= 0; i -= 2) {
            int digit = (int) s.charAt(i) - (int) '0';
            if (digit < 0 || digit > 9) {
                throw FormatException.getFormatInstance();
            }
            sum += digit;
        }
        sum *= 3;
        for (int i = length - 1; i >= 0; i -= 2) {
            int digit = (int) s.charAt(i) - (int) '0';
            if (digit < 0 || digit > 9) {
                throw FormatException.getFormatInstance();
            }
            sum += digit;
        }
        return sum % 10 == 0;
    }

    /**
     * 底部带文字条形码/二维码
     *
     * @param contents 内容
     * @param words 底部文字
     * @param barcodeFormat 编码格式
     * @param width 条形码/二维码宽度
     * @param height 条形码/二维码高度
     * @param wordHeight
     * @return
     * @throws WriterException
     */
    public static BufferedImage encodeWithWords(String contents, String words, BarcodeFormat barcodeFormat, int width, int height, int wordHeight) throws WriterException {
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        // 参数顺序分别为：编码内容，编码类型，生成图片宽度，生成图片高度，设置参数
        BitMatrix bm = multiFormatWriter.encode(contents, barcodeFormat, width, height, null);
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        // 开始利用二维码数据创建Bitmap图片，分别设为黑（0xFFFFFFFF）白（0xFF000000）两色
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                image.setRGB(x, y, bm.get(x, y) ? 0xFF000000 : 0xFFFFFFFF);
            }
        }

        // 新的图片，把带logo的二维码下面加上文字
        // 创建一个带透明色的BufferedImage对象
        BufferedImage outImage = new BufferedImage(width, wordHeight, BufferedImage.TYPE_INT_ARGB);
        Graphics2D graphics2D = outImage.createGraphics();

        // 设置Graphics2D属性（抗锯齿）
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_DEFAULT);
        Stroke s = new BasicStroke(1, BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER);
        graphics2D.setStroke(s);

        // 画二维码到新的面板
        graphics2D.drawImage(image, 0, 0, image.getWidth(), image.getHeight(), null);
        // 画文字到新的面板
        Color color = new Color(0,0,0);
        graphics2D.setColor(color);
        // 字体、字型、字号
        graphics2D.setFont(new Font("微软雅黑", Font.PLAIN, 16));
        // 文字长度
        int strWidth = graphics2D.getFontMetrics().stringWidth(words);
        // 总长度减去文字长度的一半  （居中显示）
        int wordStartX = (width - strWidth) / 2;
        int wordStartY = height + 15;
        // 画文字
        graphics2D.drawString(words, wordStartX, wordStartY);
        graphics2D.dispose();
        outImage.flush();
        return outImage;
    }

}
