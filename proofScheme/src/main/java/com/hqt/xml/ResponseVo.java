package com.hqt.xml;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "respond")
public class ResponseVo {
    /**
     * BizMsgID  报文 ID  Char(22)
     * 报文 ID，与业务报文相
     * 同
     * SuccessFlag  成功标识  Integer
     * 1：成功 2：失败，对
     * 于失败的报文，需要由
     * 区县不动产登记系统
     * 进行重新处理，重新接
     * 入。
     * ResponseCode  响应编码  Char(4)
     * ResponseInfo  报文响应信息  Char(256)
     * CertID  配号  Char(128)  不动产证书证书编码
     * QRCode  防伪二维码  Char(32768)
     * 存储对二维码文件进
     * 行 base64 转换后的字
     * 符串。
     * AdditionaIData  附加信息  Char(512)  附加信息，扩展字段。
     * AdditionaIData2  附加信息  Char(512)  附加信息，扩展字段。
     */
    private String BizMsgID;

    private String SuccessFlag;
    private String ResponseCode;
    private String ResponseInfo;
    private String QRCode;
    private String AdditionalData;
    private String AdditionalData2;

    public String getBizMsgID() {
        return BizMsgID;
    }

    public void setBizMsgID(String bizMsgID) {
        BizMsgID = bizMsgID;
    }

    public String getSuccessFlag() {
        return SuccessFlag;
    }

    public void setSuccessFlag(String successFlag) {
        SuccessFlag = successFlag;
    }

    public String getResponseCode() {
        return ResponseCode;
    }

    public void setResponseCode(String responseCode) {
        ResponseCode = responseCode;
    }

    public String getResponseInfo() {
        return ResponseInfo;
    }

    public void setResponseInfo(String responseInfo) {
        ResponseInfo = responseInfo;
    }

    public String getQRCode() {
        return QRCode;
    }

    public void setQRCode(String QRCode) {
        this.QRCode = QRCode;
    }

    public String getAdditionalData() {
        return AdditionalData;
    }

    public void setAdditionalData(String additionalData) {
        AdditionalData = additionalData;
    }

    public String getAdditionalData2() {
        return AdditionalData2;
    }

    public void setAdditionalData2(String additionalData2) {
        AdditionalData2 = additionalData2;
    }
}
