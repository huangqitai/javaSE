package com.hqt.xml;

import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.junit.Test;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

public class XmlTest2 {

    @Test
    public void t1() throws Exception {
        String xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\n" +
                "    <soap:Body>\n" +
                "        <sendSmsResponse xmlns=\"http://tempuri.org/\">\n" +
                "            <sendSmsResult>0|短信已发送。</sendSmsResult>\n" +
                "        </sendSmsResponse>\n" +
                "    </soap:Body>\n" +
                "</soap:Envelope>";
        parseResponse(xml);
    }

    @Test
    public void t2() throws Exception {
        String xml = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                "\n" +
                "<Message> \n" +
                "  <Head> \n" +
                "        <BizMsgID>440703221024004260</BizMsgID>  \n" +
                "    <ASID>AS100</ASID>  \n" +
                "    <AreaCode>440703</AreaCode>  \n" +
                "    <RecType>2000402</RecType>  \n" +
                "    <RightType>4</RightType>  \n" +
                "    <RegType>200</RegType>  \n" +
                "    <CreateDate>2022-10-24T05:26:13</CreateDate>  \n" +
                "    <RecFlowID>20221021-0029767-2</RecFlowID>  \n" +
                "    <RegOrgID>124407007270472219</RegOrgID>  \n" +
                "    <ParcelID>440703003003GX00005</ParcelID>  \n" +
                "    <EstateNum>440703003003GX00005F00020419</EstateNum>  \n" +
                "    <PreEstateNum>440703003003GX00005F00020419</PreEstateNum>  \n" +
                "    <PreCertID>粤（2022）江门市不动产权第0057907号</PreCertID>  \n" +
                "    <CertCount>1</CertCount>  \n" +
                "    <ProofCount>0</ProofCount>  \n" +
                "    </Head>  \n" +
                "  <Data> \n" +
                "    <ZTT_GY_QLR QXDM=\"440703\" SSHY=\"/\" DZYJ=\"/\" YWH=\"20221021-0029767-2\" GYFS=\"0\" QLBL=\"/\" QLLX=\"4\" QLRTZ=\"1\" BDCDYH=\"440703003003GX00005F00020419\" YSDM=\"6003000000\" ZJZL=\"1\" SXH=\"2\" GJ=\"142\" QLRMC=\"黄劲辉\" QLRLX=\"1\" QSZT=\"1\" SFCZR=\"1\" BDCQZH=\"粤（2022）江门市不动产权第0057914号\" ZJH=\"440723197902120017\" GYQK=\"单独所有\" />\n" +
                "<KTT_FW_ZRZ QXDM=\"440703\" YTMC=\"车库/车位\" ZDDM=\"440703003003GX00005\" GHYT=\"85\" JGRQ=\"2018-12-04T00:00:00\" ZYDMJ=\"63725.65\" DSCS=\"0\" BDCDYH=\"440703003003GX00005F00020000\" LDZL=\"江门市蓬江区棠下镇滨江一号花园17号\" BZ=\"已做批量 竣工日期已查     行政区代码:440703\n" +
                "原自然幢号:440703003003GB00081F0018\" YSDM=\"6001030110\" ZRZH=\"440703003003GX00005F0002\" SCJZMJ=\"15107.52\" ZCS=\"1\" DXCS=\"1\" ZZDMJ=\"15142.79\" XMMC=\"江门市蓬江区棠下镇滨江一号花园17号\" JZWMC=\"江门市蓬江区棠下镇滨江一号花园17号\" ZTS=\"617\" ZT=\"1\" BSM=\"1000000566\" FWJG=\"3\" />\n" +
                "<KTT_FW_H HH=\"419\" MJDW=\"1\" QXDM=\"440703\" SJYT=\"/\" YTMC=\"车库/车位\" ZDDM=\"440703003003GX00005\" FWYT1=\"85\" ZRZBSM=\"1000000566\" SCTNJZMJ=\"12.72\" BDCDYH=\"440703003003GX00005F00020419\" SCFTJZMJ=\"12.53\" YSDM=\"6001030140\" GYTDMJ=\"63725.65\" LJZH=\"蓬江区棠下镇滨江一号花园17号\" ZRZH=\"440703003003GX00005F0002\" SHBW=\"DX390车位\" SCJZMJ=\"25.25\" FWXZ=\"0\" CH=\"-1\" FWBM=\"P0043445\" FWLX=\"6\" ZL=\"江门市蓬江区棠下镇滨江一号花园17号DX390车位\" SJCS=\"1\" FWXZMC=\"市场化商品房\" ZT=\"1\" HXJG=\"1\" FWLXMC=\"车库\" />\n" +
                "<QLT_FW_FDCQ_YZ QXDM=\"440703\" DJYY=\"转移登记\" YTMC=\"车库/车位\" YWH=\"20221021-0029767-2\" GHYT=\"85\" QLLX=\"4\" TDSYQX=\"城镇住宅用地 2016年09月18日起2086年09月17日止\" BDCDYH=\"440703003003GX00005F00020419\" FTJZMJ=\"12.53\" YSDM=\"6002010210\" YT=\"0701\" TDSYQSSJ=\"2016-09-18T00:00:00\" SZC=\"-1\" TDSYQR=\"黄劲辉\" FDZL=\"江门市蓬江区棠下镇滨江一号花园17号DX390车位\" JZMJ=\"25.25\" SSYWH=\"20221021-0029767-1\" FWXZ=\"0\" JGSJ=\"2018-12-04\" ZYJZMJ=\"12.72\" QLXZ=\"102\" QSZT=\"1\" ZCS=\"1\" DJJG=\"江门市自然资源局\" DJSJ=\"2022-10-24T17:26:13\" FDCJYJG=\"18.8000\" BDCQZH=\"粤（2022）江门市不动产权第0057914号\" DBR=\"叶思茗\" TDSYJSSJ=\"2086-09-17T00:00:00\" FWXZMC=\"市场化商品房\" DJLX=\"200\" FWJG=\"3\" JEDW=\"2\" />\n" +
                "<DJF_DJ_SQ SQSJ=\"2022-10-21T09:16:25\" QXDM=\"440703\" QLRMC=\"黄劲辉\" YWRMC=\"江门市蓬江区凤凰碧桂园房地产开发有限公司\" YWH=\"20221021-0029767-2\" QLRDLRMC=\"赵璐璐\" QLRZJZL=\"1\" YWRZJZL=\"7\" YWRZJH=\"91440703MA4URUT3X5\" QLRZJH=\"440723197902120017\" YSDM=\"6004010000\" />\n" +
                "<DJT_DJ_SL ZL=\"江门市蓬江区棠下镇滨江一号花园17号DX390车位\" SQFBCZ=\"1\" QXDM=\"440703\" SLRY=\"梁嘉仪\" SLSJ=\"2022-10-21T09:16:25\" SQZSBS=\"0\" YSDM=\"6004020000\" YWH=\"20221021-0029767-2\" DJLX=\"200\" />\n" +
                "<DJF_DJ_SJ SFSJSY=\"0\" SFBCSJ=\"0\" SJSL=\"1\" QXDM=\"440703\" SJLX=\"1\" SFEWSJ=\"0\" SJMC=\"1-不动产登记申请书.pdf\" YWH=\"20221021-0029767-2\" YSDM=\"6004030000\" SJSJ=\"2022-10-21T09:16:24\" />\n" +
                "<DJF_DJ_SJ SFSJSY=\"0\" SFBCSJ=\"0\" SJSL=\"1\" QXDM=\"440703\" SJLX=\"1\" SFEWSJ=\"0\" SJMC=\"3-委托书.pdf\" YWH=\"20221021-0029767-2\" YSDM=\"6004030000\" SJSJ=\"2022-10-21T09:16:24\" />\n" +
                "<DJF_DJ_SJ SFSJSY=\"0\" SFBCSJ=\"0\" SJSL=\"1\" QXDM=\"440703\" SJLX=\"1\" SFEWSJ=\"0\" SJMC=\"5-商品房权属登记证明书.pdf\" YWH=\"20221021-0029767-2\" YSDM=\"6004030000\" SJSJ=\"2022-10-21T09:16:24\" />\n" +
                "<DJF_DJ_SJ SFSJSY=\"0\" SFBCSJ=\"0\" SJSL=\"1\" QXDM=\"440703\" SJLX=\"1\" SFEWSJ=\"0\" SJMC=\"7-完税证明.pdf\" YWH=\"20221021-0029767-2\" YSDM=\"6004030000\" SJSJ=\"2022-10-21T09:16:24\" />\n" +
                "<DJF_DJ_SJ SFSJSY=\"0\" SFBCSJ=\"0\" SJSL=\"1\" QXDM=\"440703\" SJLX=\"1\" SFEWSJ=\"0\" SJMC=\"电子证照\" YWH=\"20221021-0029767-2\" YSDM=\"6004030000\" SJSJ=\"2022-10-21T09:16:24\" />\n" +
                "<DJF_DJ_SJ SFSJSY=\"0\" SFBCSJ=\"0\" SJSL=\"1\" QXDM=\"440703\" SJLX=\"1\" SFEWSJ=\"0\" SJMC=\"2-身份证信息.pdf\" YWH=\"20221021-0029767-2\" YSDM=\"6004030000\" SJSJ=\"2022-10-21T09:21:17\" />\n" +
                "<DJF_DJ_SJ SFSJSY=\"0\" SFBCSJ=\"0\" SJSL=\"1\" QXDM=\"440703\" SJLX=\"1\" SFEWSJ=\"0\" SJMC=\"4-商品房买卖合同.pdf\" YWH=\"20221021-0029767-2\" YSDM=\"6004030000\" SJSJ=\"2022-10-21T09:21:17\" />\n" +
                "<DJF_DJ_SJ SFSJSY=\"0\" SFBCSJ=\"0\" SJSL=\"1\" QXDM=\"440703\" SJLX=\"1\" SFEWSJ=\"0\" SJMC=\"6-收费单.pdf\" YWH=\"20221021-0029767-2\" YSDM=\"6004030000\" SJSJ=\"2022-10-21T09:21:17\" />\n" +
                "<DJF_DJ_SJ SFSJSY=\"0\" SFBCSJ=\"0\" SJSL=\"1\" QXDM=\"440703\" SJLX=\"1\" SFEWSJ=\"0\" SJMC=\"电子合同\" YWH=\"20221021-0029767-2\" YSDM=\"6004030000\" SJSJ=\"2022-10-21T09:21:17\" />\n" +
                "<DJF_DJ_SJ SFSJSY=\"0\" SFBCSJ=\"0\" SJSL=\"1\" QXDM=\"440703\" SJLX=\"1\" SFEWSJ=\"0\" SJMC=\"电子税票\" YWH=\"20221021-0029767-2\" YSDM=\"6004030000\" SJSJ=\"2022-10-21T09:21:17\" />\n" +
                "<DJF_DJ_SH JDMC=\"受理\" SXH=\"1\" SHYJ=\"经初步核查，申请材料内容齐全及真实有效；该不动产符合登记条件，建议予以办理国有建设用地使用权/房屋所有权-转移登记（买卖(一手房)）\" QXDM=\"440703\" CZJG=\"1\" YWH=\"20221021-0029767-2\" YSDM=\"6004030000\" SHRYXM=\"梁嘉仪\" SHKSSJ=\"2022-10-21T09:16:25\" SHJSSJ=\"2022-10-21T09:16:25\" />\n" +
                "<DJF_DJ_SH JDMC=\"初审\" SXH=\"2\" SHYJ=\"经初步核查，申请材料内容齐全及真实有效；该不动产符合登记条件，建议予以办理国有建设用地使用权/房屋所有权-转移登记（买卖(一手房)）\" QXDM=\"440703\" CZJG=\"1\" YWH=\"20221021-0029767-2\" YSDM=\"6004030000\" SHRYXM=\"梁嘉仪\" SHKSSJ=\"2022-10-21T09:16:25\" SHJSSJ=\"2022-10-21T09:16:25\" />\n" +
                "<DJF_DJ_DB DBYJ=\"同意登簿\" DBRYXM=\"叶思茗\" QXDM=\"440703\" CZJG=\"1\" YWH=\"20221021-0029767-2\" YSDM=\"6004040000\" DBSJ=\"2022-10-24T17:26:13\" />\n" +
                "    </Data> \n" +
                "</Message>\n";
        System.out.println(parseResponse(xml,"CreateDate","xxx"));
    }

    @Test
    public void t3() throws Exception {
        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "\n" +
                "<Message>\n" +
                " <Head>\n" +
                "  <BizMsgID>440705220706005524</BizMsgID>\n" +
                "<DigitalSign>54bd464e8760bd69302def5f6ad28f2327d7bf4c36388e4fc08e028cb206e4a42d9ecfc29eb94866122fa44b52c68d7982f30efbd78da2dc9cc1d0013b239d5439850d9a0fef811ffd49fc720487240e7758d014289dfa9e9e6d0604a8c2bd60b4e1b18feac9c1d705a7d3a73eca85febab5b92625cda8a95b38f5a87bee083c</DigitalSign>\n" +
                "  <ASID>AS100</ASID>\n" +
                "  <AreaCode>440705</AreaCode>\n" +
                "  <RecType>1003701</RecType>\n" +
                "  <RightType>37</RightType>\n" +
                "  <RegType>100</RegType>\n" +
                "  <CreateDate>2022-07-06T06:30:07</CreateDate>\n" +
                "  <RecFlowID>20220610-64555</RecFlowID>\n" +
                "  <RegOrgID>12440705MB2C16269H</RegOrgID>\n" +
                "  <ParcelID>440705012005GX00020</ParcelID>\n" +
                "  <EstateNum>440705012005GX00020F00010046</EstateNum>\n" +
                "  <PreEstateNum>/</PreEstateNum>\n" +
                "  <PreCertID>/</PreCertID>\n" +
                "  <CertCount>0</CertCount>\n" +
                "  <ProofCount>1</ProofCount>\n" +
                "  <PreRecFlowID>/</PreRecFlowID>\n" +
                "  <CertCheck>UZDJuKCt7xFL4n9f525Gp+3L/WoD5qtuWC+P8agmc6tMACY2YC0+nVFq4QozLgsRrVz4VxECuZSwWD2s6DwEdC7SmRksGmhIts+4SuEnJdEUc5+dWTXaruw0/OqEzqMCP9L8DA2ltg2M7J9M6XkdOqNBWgbH8Uw4QnrVHpsTQos=</CertCheck>\n" +
                "  <Projection>1</Projection>\n" +
                "  <QLID>/</QLID>\n" +
                "  <GLID>/</GLID>\n" +
                "  <PreQLID>/</PreQLID>\n" +
                " </Head>\n" +
                " <Data>\n" +
                "  <KTT_FW_ZRZ QXDM=\"440705\" ZDDM=\"440705012005GX00020\" GHYT=\"85\" JGRQ=\"2021-12-03T00:00:00\" ZYDMJ=\"39527.0\" DSCS=\"0\" BDCDYH=\"440705012005GX00020F00010000\" LDZL=\"江门市新会区会城明德一路36号博富名苑地下车库\" YSDM=\"6001030110\" ZRZH=\"440705012005GX00020F0001\" SCJZMJ=\"31716.34\" YCJZMJ=\"31721.15\" ZCS=\"1\" DXCS=\"1\" ZZDMJ=\"31590.14\" XMMC=\"江门市新会区会城明德一路36号博富名苑地下车库\" JZWMC=\"江门市新会区会城明德一路36号博富名苑地下车库\" ZTS=\"1000\" ZT=\"1\" BSM=\"1000000284\" FWJG=\"3\"></KTT_FW_ZRZ>\n" +
                "  <KTT_FW_H HH=\"7\" MJDW=\"1\" QXDM=\"440705\" SJYT=\"/\" ZDDM=\"440705012005GX00020\" FWYT1=\"85\" ZRZBSM=\"1000000284\" SCTNJZMJ=\"13.78\" BDCDYH=\"440705012005GX00020F00010046\" YCTNJZMJ=\"13.78\" SCFTJZMJ=\"16.21\" YSDM=\"6001030140\" GYTDMJ=\"39527.0\" ZRZH=\"440705012005GX00020F0001\" SHBW=\"009#\" YCFTJZMJ=\"16.21\" SCJZMJ=\"29.99\" FWXZ=\"0\" CH=\"-1\" YCJZMJ=\"29.99\" FWBM=\"X511191466\" FWLX=\"6\" ZL=\"江门市新会区会城明德一路36号博富名苑地下车库009#\" SJCS=\"1\" FWXZMC=\"市场化商品房\" ZT=\"1\" HXJG=\"99\" FWLXMC=\"车库\"></KTT_FW_H>\n" +
                "  <ZD_K_103 XH=\"1\" YZB=\"22.50288907\" XZB=\"113.05515793\" BDCDYH=\"440705012005GX00020W00000000\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"2\" YZB=\"22.50379711\" XZB=\"113.05753717\" BDCDYH=\"440705012005GX00020W00000000\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"3\" YZB=\"22.50278389\" XZB=\"113.05800599\" BDCDYH=\"440705012005GX00020W00000000\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"4\" YZB=\"22.50248439\" XZB=\"113.05788016\" BDCDYH=\"440705012005GX00020W00000000\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"5\" YZB=\"22.50168994\" XZB=\"113.05596752\" BDCDYH=\"440705012005GX00020W00000000\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"6\" YZB=\"22.50180519\" XZB=\"113.05564981\" BDCDYH=\"440705012005GX00020W00000000\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"7\" YZB=\"22.50258174\" XZB=\"113.05529048\" BDCDYH=\"440705012005GX00020W00000000\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"8\" YZB=\"22.50280794\" XZB=\"113.05518581\" BDCDYH=\"440705012005GX00020W00000000\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"9\" YZB=\"22.50281093\" XZB=\"113.05518706\" BDCDYH=\"440705012005GX00020W00000000\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"10\" YZB=\"22.50288907\" XZB=\"113.05515793\" BDCDYH=\"440705012005GX00020W00000000\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <KTT_ZDJBXX QLSDFS=\"3\" MJDW=\"1\" QXDM=\"440705\" DJ=\"10\" ZDDM=\"440705012005GX00020\" ZDT=\"/\" QLLX=\"3\" ZDSZD=\"文华路\" ZDSZB=\"空地\" BDCDYH=\"440705012005GX00020W00000000\" YSDM=\"6001010000\" ZDTZM=\"X\" YT=\"城镇住宅用地\" ZDSZN=\"明德路\" JGDW=\"2\" ZDSZX=\"间距路\" GHYTMC=\"/\" QLXZ=\"102\" JFH=\"茶坑\" ZH=\"/\" JDH=\"南新区\" ZL=\"江门市新会区会城明德一路36号博富名苑地下车库\" TFH=\"F49G036081\" ZT=\"1\" BSM=\"1000001039\" ZDMJ=\"39527.0000\" DKDM=\"/\"></KTT_ZDJBXX>\n" +
                " </Data>\n" +
                "</Message>\n";
        String elementName = "DJT_DJ_SL";
        String key = "FJSQK";
        String value = "其他非计时情况|4";
        xml = addXmlResponse(xml,elementName);
        System.out.println(addAttribute(xml,elementName,key,value));
    }
    @Test
    public void t4() throws Exception {
        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "\n" +
                "<Message>\n" +
                " <Head>\n" +
                "  <BizMsgID>440703221116000910</BizMsgID>\n" +
                "  <DigitalSign>0312d719258aaa82aa9ebdb1a1f7af1539a989b67d56f36a77412ea328c66517beda9830c573fbca29ab2c8521d5c6b3fb29ba4854bd7569816ac2743a462cf24f84721e6f9c3b80d32a421bd0a44840eda7263277700a93f0d61b0a0d9d1c87aabd03b4c38552ee3fdb01251d99b96be266e80f201d3935f77e8de96e4aaa05</DigitalSign>\n" +
                "  <ASID>AS100</ASID>\n" +
                "  <AreaCode>440703</AreaCode>\n" +
                "  <RecType>8000101</RecType>\n" +
                "  <RightType>39</RightType>\n" +
                "  <RegType>400</RegType>\n" +
                "  <CreateDate>2022-11-16T10:56:18</CreateDate>\n" +
                "  <RecFlowID>20221115-0044197</RecFlowID>\n" +
                "  <RegOrgID>124407007270472219</RegOrgID>\n" +
                "  <ParcelID>440703003004GB90302</ParcelID>\n" +
                "  <EstateNum>440703003004GB90302F00010090</EstateNum>\n" +
                "  <PreEstateNum>440703003004GB90302F00010090</PreEstateNum>\n" +
                "  <PreCertID>（2022）粤0703执保2305号</PreCertID>\n" +
                "  <CertCount>0</CertCount>\n" +
                "  <ProofCount>0</ProofCount>\n" +
                "  <PreRecFlowID>20220829-0006698</PreRecFlowID>\n" +
                "  <CertCheck>/</CertCheck>\n" +
                "  <Projection>1</Projection>\n" +
                "  <QLID>/</QLID>\n" +
                "  <GLID>/</GLID>\n" +
                "  <PreQLID>/</PreQLID>\n" +
                " </Head>\n" +
                " <Data>\n" +
                "  <DJT_DJ_SL FJSQK=\"批量受理业务|4&amp;其他非计时情况|1\"></DJT_DJ_SL>\n" +
                " </Data>\n" +
                "</Message>\n";
        String elementName = "DJT_DJ_SL";
        System.out.println(getElementValueResponse(xml,elementName,"FJSQK"));
    }
    private String getElementValueResponse(String xml,String elementName,String key) throws Exception {
        String value = "";
        if (StringUtils.isNotEmpty(xml)) {
            //解析xml格式内容
            Document document = DocumentHelper.parseText(xml);
            Element root = document.getRootElement();
            //root = root.element("response");
            if (root != null) {
                root = root.element("Data");
                if (root != null) {
                    root = root.element(elementName);
                    if (root != null) {
                        value = root.attributeValue(key);
                    }
                }
            }
        }
        return value;
    }
    @Test
    public void t5() throws Exception {
        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "\n" +
                "<Message>\n" +
                " <Head>\n" +
                "  <BizMsgID>440705220706005524</BizMsgID>\n" +
                "<DigitalSign>54bd464e8760bd69302def5f6ad28f2327d7bf4c36388e4fc08e028cb206e4a42d9ecfc29eb94866122fa44b52c68d7982f30efbd78da2dc9cc1d0013b239d5439850d9a0fef811ffd49fc720487240e7758d014289dfa9e9e6d0604a8c2bd60b4e1b18feac9c1d705a7d3a73eca85febab5b92625cda8a95b38f5a87bee083c</DigitalSign>\n" +
                "  <ASID>AS100</ASID>\n" +
                "  <AreaCode>440705</AreaCode>\n" +
                "  <RecType>1003701</RecType>\n" +
                "  <RightType>37</RightType>\n" +
                "  <RegType>100</RegType>\n" +
                "  <CreateDate>2022-07-06T06:30:07</CreateDate>\n" +
                "  <RecFlowID>20220610-64555</RecFlowID>\n" +
                "  <RegOrgID>12440705MB2C16269H</RegOrgID>\n" +
                "  <ParcelID>440705012005GX00020</ParcelID>\n" +
                "  <EstateNum>440705012005GX00020F00010046</EstateNum>\n" +
                "  <PreEstateNum>/</PreEstateNum>\n" +
                "  <PreCertID>/</PreCertID>\n" +
                "  <CertCount>0</CertCount>\n" +
                "  <ProofCount>1</ProofCount>\n" +
                "  <PreRecFlowID>/</PreRecFlowID>\n" +
                "  <CertCheck>UZDJuKCt7xFL4n9f525Gp+3L/WoD5qtuWC+P8agmc6tMACY2YC0+nVFq4QozLgsRrVz4VxECuZSwWD2s6DwEdC7SmRksGmhIts+4SuEnJdEUc5+dWTXaruw0/OqEzqMCP9L8DA2ltg2M7J9M6XkdOqNBWgbH8Uw4QnrVHpsTQos=</CertCheck>\n" +
                "  <Projection>1</Projection>\n" +
                "  <QLID>/</QLID>\n" +
                "  <GLID>/</GLID>\n" +
                "  <PreQLID>/</PreQLID>\n" +
                " </Head>\n" +
                " <Data>\n" +
                "  <KTT_FW_ZRZ QXDM=\"440705\" ZDDM=\"440705012005GX00020\" GHYT=\"85\" JGRQ=\"2021-12-03T00:00:00\" ZYDMJ=\"39527.0\" DSCS=\"0\" BDCDYH=\"440705012005GX00020F00010000\" LDZL=\"江门市新会区会城明德一路36号博富名苑地下车库\" YSDM=\"6001030110\" ZRZH=\"440705012005GX00020F0001\" SCJZMJ=\"31716.34\" YCJZMJ=\"31721.15\" ZCS=\"1\" DXCS=\"1\" ZZDMJ=\"31590.14\" XMMC=\"江门市新会区会城明德一路36号博富名苑地下车库\" JZWMC=\"江门市新会区会城明德一路36号博富名苑地下车库\" ZTS=\"1000\" ZT=\"1\" BSM=\"1000000284\" FWJG=\"3\"></KTT_FW_ZRZ>\n" +
                "  <KTT_FW_H HH=\"7\" MJDW=\"1\" QXDM=\"440705\" SJYT=\"/\" ZDDM=\"440705012005GX00020\" FWYT1=\"85\" ZRZBSM=\"1000000284\" SCTNJZMJ=\"13.78\" BDCDYH=\"440705012005GX00020F00010046\" YCTNJZMJ=\"13.78\" SCFTJZMJ=\"16.21\" YSDM=\"6001030140\" GYTDMJ=\"39527.0\" ZRZH=\"440705012005GX00020F0001\" SHBW=\"009#\" YCFTJZMJ=\"16.21\" SCJZMJ=\"29.99\" FWXZ=\"0\" CH=\"-1\" YCJZMJ=\"29.99\" FWBM=\"X511191466\" FWLX=\"6\" ZL=\"江门市新会区会城明德一路36号博富名苑地下车库009#\" SJCS=\"1\" FWXZMC=\"市场化商品房\" ZT=\"1\" HXJG=\"99\" FWLXMC=\"车库\"></KTT_FW_H>\n" +
                "  <ZD_K_103 XH=\"1\" YZB=\"22.50288907\" XZB=\"113.05515793\" BDCDYH=\"440705012005GX00020W00000000\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"2\" YZB=\"22.50379711\" XZB=\"113.05753717\" BDCDYH=\"440705012005GX00020W00000000\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"3\" YZB=\"22.50278389\" XZB=\"113.05800599\" BDCDYH=\"440705012005GX00020W00000000\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"4\" YZB=\"22.50248439\" XZB=\"113.05788016\" BDCDYH=\"440705012005GX00020W00000000\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"5\" YZB=\"22.50168994\" XZB=\"113.05596752\" BDCDYH=\"440705012005GX00020W00000000\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"6\" YZB=\"22.50180519\" XZB=\"113.05564981\" BDCDYH=\"440705012005GX00020W00000000\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"7\" YZB=\"22.50258174\" XZB=\"113.05529048\" BDCDYH=\"440705012005GX00020W00000000\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"8\" YZB=\"22.50280794\" XZB=\"113.05518581\" BDCDYH=\"440705012005GX00020W00000000\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"9\" YZB=\"22.50281093\" XZB=\"113.05518706\" BDCDYH=\"440705012005GX00020W00000000\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <ZD_K_103 XH=\"10\" YZB=\"22.50288907\" XZB=\"113.05515793\" BDCDYH=\"440705012005GX00020W00000000\" KJLX=\"0\" ZDX=\"1\"></ZD_K_103>\n" +
                "  <KTT_ZDJBXX QLSDFS=\"3\" MJDW=\"1\" QXDM=\"440705\" DJ=\"10\" ZDDM=\"440705012005GX00020\" ZDT=\"/\" QLLX=\"3\" ZDSZD=\"文华路\" ZDSZB=\"空地\" BDCDYH=\"440705012005GX00020W00000000\" YSDM=\"6001010000\" ZDTZM=\"X\" YT=\"城镇住宅用地\" ZDSZN=\"明德路\" JGDW=\"2\" ZDSZX=\"间距路\" GHYTMC=\"/\" QLXZ=\"102\" JFH=\"茶坑\" ZH=\"/\" JDH=\"南新区\" ZL=\"江门市新会区会城明德一路36号博富名苑地下车库\" TFH=\"F49G036081\" ZT=\"1\" BSM=\"1000001039\" ZDMJ=\"39527.0000\" DKDM=\"/\"></KTT_ZDJBXX>\n" +
                " </Data>\n" +
                "</Message>\n";
        String elementName = "DigitalSign";
        System.out.println(formatXML(removeXmlResponse(xml,elementName)));
    }

    public String formatXML(String inputXML) throws Exception {
        SAXReader reader = new SAXReader();
        Document document = reader.read(new StringReader(inputXML));
        String requestXML = null;
        XMLWriter writer = null;
        if (document != null) {
            try {
                StringWriter stringWriter = new StringWriter();
                OutputFormat format = new OutputFormat(" ", true);
                format.setTrimText(true);
                format.setExpandEmptyElements(true);
                writer = new XMLWriter(stringWriter, format);
                writer.write(document);
                writer.flush();
                requestXML = stringWriter.getBuffer().toString();
            } finally {
                if (writer != null) {
                    try {
                        writer.close();
                    } catch (IOException e) {
                    }
                }
            }
        }
        return requestXML;
    }
    private String removeXmlResponse(String xml,String elementName) throws Exception {
        if (StringUtils.isNotEmpty(xml)) {
            //解析xml格式内容
            Document document = DocumentHelper.parseText(xml);
            Element root = document.getRootElement();
            if (root != null) {
                root = root.element("Head");
                if (root != null) {
                    root.remove(root.element(elementName));
                }
            }
            return document.asXML();
        }
        return "document";
    }
    private String addXmlResponse(String xml,String elementName) throws Exception {
        if (StringUtils.isNotEmpty(xml)) {
            //解析xml格式内容
            Document document = DocumentHelper.parseText(xml);
            Element root = document.getRootElement();
            if (root != null) {
                root = root.element("Data");
                if (root != null) {
                    root.addElement(elementName);
                }
            }
            return document.asXML();
        }
        return "document";
    }
    private String addAttribute(String xml,String elementName,String key,String value) throws Exception {
        if (StringUtils.isNotEmpty(xml)) {
            //解析xml格式内容
            Document document = DocumentHelper.parseText(xml);
            Element root = document.getRootElement();
            if (root != null) {
                root = root.element("Data");
                if (root != null) {
                    root = root.element(elementName);
                    if (root != null) {
                        root.addAttribute(key,value);
                    }
                }
            }
            return document.asXML();
        }
        return "document";
    }
    private String parseResponse(String xml,String key,String value) throws Exception {
        if (StringUtils.isNotEmpty(xml)) {
            //解析xml格式内容
            Document document = DocumentHelper.parseText(xml);
            Element root = document.getRootElement();
            //root = root.element("Message");
            if (root != null) {
                root = root.element("Head");
                if (root != null) {
                    root = root.element(key);
                    if (root != null) {
                        root.setText(value);
                    }
                }
            }
            return document.asXML();
        }
        return "document";
    }

    private String parseResponse(String xml) throws Exception {
        String value = "";
        if (StringUtils.isNotEmpty(xml)) {
            //解析xml格式内容
            Document document = DocumentHelper.parseText(xml);
            Element root = document.getRootElement();
            //root = root.element("response");
            if (root != null) {
                root = root.element("Body");
                if (root != null) {
                    root = root.element("sendSmsResponse");
                    if (root != null) {
                        root = root.element("sendSmsResult");
                        if (root != null) {
                            value = root.getText();
                        }
                    }
                }
            }
        }
        return value;
    }
}
