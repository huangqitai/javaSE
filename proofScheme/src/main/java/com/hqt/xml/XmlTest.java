package com.hqt.xml;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import org.junit.Test;

import java.io.StringReader;
import java.lang.reflect.Field;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import javax.xml.stream.XMLStreamReader;

public class XmlTest {
    @Test
    public void t1() throws NoSuchFieldException, DocumentException, IllegalAccessException {
        String xmlStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<respond>\n" +
                "  <SuccessFlag>2</SuccessFlag>\n" +
                "  <CertID></CertID>\n" +
                "  <BizMsgID>440703220520000488</BizMsgID>\n" +
                "  <AdditionalData2></AdditionalData2>\n" +
                "  <AdditionalData>当前数据未找到对应的上一手更新信息,请确认上手信息填写真实无误；</AdditionalData>\n" +
                "  <QRCode></QRCode>\n" +
                "  <ResponseInfo>当前数据未找到对应的上一手更新信息,请确认上手信息填写真实无误；</ResponseInfo>\n" +
                "  <ResponseCode>1000</ResponseCode>\n" +
                "</respond>";
        System.out.println("反馈报文内容："+xmlStr);
        //创建Jdom2的解析器对象
        SAXReader reader = new SAXReader();
        Document document = null;
        Element root = null;
        ResponseVo responseVo = null;

        document = reader.read(new StringReader(xmlStr));
        root = document.getRootElement();
        if (!root.getName().equals(ResponseVo.class.getAnnotation(JacksonXmlRootElement.class).localName())) {
            System.out.println("xml内容无法转成  ResponseVo 对象，请检查！");
        }
        responseVo = new ResponseVo();
        List<Element> children = root.elements();
        for (Element child : children) {
            if (child.getText()==null||child.getTextTrim().isEmpty()){
                continue;
            }
            Field field = ResponseVo.class.getDeclaredField(child.getName());
            field.setAccessible(true);
            if ("int".equals(field.getGenericType().getTypeName())) {
                field.set(responseVo, Integer.parseInt(child.getText()));
            } else {
                field.set(responseVo, child.getText());
            }
        }

        // xml转对象
        //ResponseVo responseVo = xmlMapper.readValue(xmlStr, ResponseVo.class);
        String bizId = responseVo.getBizMsgID();
        int successFlag = Integer.parseInt(responseVo.getSuccessFlag());
        String responseInfo = responseVo.getResponseInfo();
    }

    @Test
    public void t2() throws JsonProcessingException {

        String xmlStr = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?> \n" +
                "<dataset>\n" +
                "\t<smsrow>\n" +
                "\t\t<sendName>发送人姓名</sendName> \n" +
                "\t\t<sendId>发送人id</sendId> \n" +
                "\t\t<rcvName>接收人姓名</rcvName> \n" +
                "\t\t<rcvId>接收人id</rcvId> \n" +
                "\t\t<depId>发送者部门id</depId> \n" +
                "\t\t<depName>发送者部门名称</depName> \n" +
                "\t\t<deliverTime>短信投递时间</deliverTime> \n" +
                "\t\t<requestTime >短信提交时间</requestTime>\n" +
                "\t\t<submitTime >短信发送时间</submitTime >  \n" +
                "\t\t<phone>接收者手机</phone> \n" +
                "\t\t<content>短信发送内容</content> \n" +
                "\t\t<docUuid>发送短信id</docUuid> \n" +
                "\t\t<partCount>短信拆分数量</partCount> \n" +
                "\t\t<partStatus>状态</partStatus> \n" +
                "\t\t<smsresult>是否已回复</smsresult> \n" +
                "\t</smsrow>\n" +
                "\t<childs>发送条数</childs> \n" +
                "</dataset>\n";
        //SdResponseVo sdResponseVo =xmlMapper.readValue(xmlStr,SdResponseVo.class);
        //创建XmlMapper对象，用于实体与json和xml之间的相互转换
        XmlMapper xmlMapper = new XmlMapper();
        //反序列化时，若实体类没有对应的属性，是否抛出JsonMappingException异常，false忽略掉
        xmlMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        //序列化是否绕根元素，true，则以类名为根元素
        xmlMapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        //忽略空属性
        xmlMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        //XML标签名:使⽤骆驼命名的属性名，
        xmlMapper.setPropertyNamingStrategy(PropertyNamingStrategy.UPPER_CAMEL_CASE);
        //设置转换模式
        xmlMapper.enable(MapperFeature.USE_STD_BEAN_NAMING);
        SdResponseVo sdResponseVo =xmlMapper.readValue(xmlStr,SdResponseVo.class);
        System.out.println(sdResponseVo.getChilds());
    }

    @Test
    public void t3() throws NoSuchFieldException, DocumentException, IllegalAccessException {
        String xmlStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "\n" +
                "<respond>\n" +
                "  <ResponseInfo>响应成功</ResponseInfo>\n" +
                "  <AdditionalData></AdditionalData>\n" +
                "  <ResponseCode>0000</ResponseCode>\n" +
                "  <QRCode></QRCode>\n" +
                "  <BizMsgID>440703220728002588</BizMsgID>\n" +
                "  <AdditionalData2>20220707-79144</AdditionalData2>\n" +
                "  <SuccessFlag>1</SuccessFlag>\n" +
                "  <CertID></CertID>\n" +
                "</respond>\n";
        System.out.println("反馈报文内容："+xmlStr);
        //创建Jdom2的解析器对象
        SAXReader reader = new SAXReader();
        Document document = null;
        Element root = null;
        ResponseVo responseVo = null;

        document = reader.read(new StringReader(xmlStr));
        root = document.getRootElement();
        if (!root.getName().equals(ResponseVo.class.getAnnotation(JacksonXmlRootElement.class).localName())) {
            System.out.println("xml内容无法转成  ResponseVo 对象，请检查！");
        }
        responseVo = new ResponseVo();
        List<Element> children = root.elements();
        for (Element child : children) {
            if (child.getText()==null||child.getTextTrim().isEmpty()){
                continue;
            }
            Field field = ResponseVo.class.getDeclaredField(child.getName());
            field.setAccessible(true);
            if ("int".equals(field.getGenericType().getTypeName())) {
                field.set(responseVo, Integer.parseInt(child.getText()));
            } else {
                field.set(responseVo, child.getText());
            }
        }

        // xml转对象
        //ResponseVo responseVo = xmlMapper.readValue(xmlStr, ResponseVo.class);
        String bizId = responseVo.getBizMsgID();
        int successFlag = Integer.parseInt(responseVo.getSuccessFlag());
        String responseInfo = responseVo.getResponseInfo();
    }

}
