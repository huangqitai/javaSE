package com.hqt.xml;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "smsrow")
public class SmsRow {
    /**
     * 	<smsrow>
     * 		<sendName>发送人姓名</sendName>
     * 		<sendId>发送人id</sendId>
     * 		<rcvName>接收人姓名</rcvName>
     * 		<rcvId>接收人id</rcvId>
     * 		<depId>发送者部门id</depId>
     * 		<depName>发送者部门名称</depName>
     * 		<deliverTime>短信投递时间</deliverTime>
     * 		<requestTime >短信提交时间</requestTime>
     * 		<submitTime >短信发送时间</submitTime >
     * 		<phone>接收者手机</phone>
     * 		<content>短信发送内容</content>
     * 	    <docUuid>发送短信id</docUuid>
     * 		<partCount>短信拆分数量</partCount>
     * 		<partStatus>状态</partStatus>
     * 		<smsresult>是否已回复</smsresult>
     * 	</smsrow>
     */
    @JacksonXmlProperty(localName = "sendName")
    private String sendName;
    @JacksonXmlProperty(localName = "sendId")
    private String sendId;
    @JacksonXmlProperty(localName = "rcvName")
    private String rcvName;
    @JacksonXmlProperty(localName = "rcvId")
    private String rcvId;
    @JacksonXmlProperty(localName = "depId")
    private String depId;
    @JacksonXmlProperty(localName = "depName")
    private String depName;
    @JacksonXmlProperty(localName = "deliverTime")
    private String deliverTime;
    @JacksonXmlProperty(localName = "requestTime")
    private String requestTime;
    @JacksonXmlProperty(localName = "submitTime")
    private String submitTime;
    @JacksonXmlProperty(localName = "phone")
    private String phone;
    @JacksonXmlProperty(localName = "content")
    private String content;
    @JacksonXmlProperty(localName = "docUuid")
    private String docUuid;
    @JacksonXmlProperty(localName = "partCount")
    private String partCount;
    @JacksonXmlProperty(localName = "partStatus")
    private String partStatus;
    @JacksonXmlProperty(localName = "smsresult")
    private String smsresult;

    public String getSendName() {
        return sendName;
    }

    public void setSendName(String sendName) {
        this.sendName = sendName;
    }

    public String getSendId() {
        return sendId;
    }

    public void setSendId(String sendId) {
        this.sendId = sendId;
    }

    public String getRcvName() {
        return rcvName;
    }

    public void setRcvName(String rcvName) {
        this.rcvName = rcvName;
    }

    public String getRcvId() {
        return rcvId;
    }

    public void setRcvId(String rcvId) {
        this.rcvId = rcvId;
    }

    public String getDepId() {
        return depId;
    }

    public void setDepId(String depId) {
        this.depId = depId;
    }

    public String getDepName() {
        return depName;
    }

    public void setDepName(String depName) {
        this.depName = depName;
    }

    public String getDeliverTime() {
        return deliverTime;
    }

    public void setDeliverTime(String deliverTime) {
        this.deliverTime = deliverTime;
    }

    public String getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(String requestTime) {
        this.requestTime = requestTime;
    }

    public String getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(String submitTime) {
        this.submitTime = submitTime;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDocUuid() {
        return docUuid;
    }

    public void setDocUuid(String docUuid) {
        this.docUuid = docUuid;
    }

    public String getPartCount() {
        return partCount;
    }

    public void setPartCount(String partCount) {
        this.partCount = partCount;
    }

    public String getPartStatus() {
        return partStatus;
    }

    public void setPartStatus(String partStatus) {
        this.partStatus = partStatus;
    }

    public String getSmsresult() {
        return smsresult;
    }

    public void setSmsresult(String smsresult) {
        this.smsresult = smsresult;
    }
}
