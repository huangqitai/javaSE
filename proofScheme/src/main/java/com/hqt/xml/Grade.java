package com.hqt.xml;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.List;
/**
 * 测试对象Grade，包含普通属性name:名称  列表属性Classes:多个Classes对象集合
 */
public class Grade {
    @JacksonXmlProperty(localName = "NAME")
    private String name;
    @JacksonXmlElementWrapper(localName = "ClassList")
    @JacksonXmlProperty(localName = "Class")
    private List<Classes> classes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Classes> getClasses() {
        return classes;
    }

    public void setClasses(List<Classes> classes) {
        this.classes = classes;
    }
}
