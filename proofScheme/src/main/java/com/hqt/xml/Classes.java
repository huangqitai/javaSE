package com.hqt.xml;
/**
 * 测试对象Classes，包含普通属性name:名称  属性studentCount:数量
 */
public class Classes {
    private String name;
    private Integer studentCount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStudentCount() {
        return studentCount;
    }

    public void setStudentCount(Integer studentCount) {
        this.studentCount = studentCount;
    }
}
