package com.hqt.xml;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.List;

/**
 * 测试对象School，包含普通属性name:名称  列表属性grades:多个Grade对象集合
 */
public class School {
    @JacksonXmlProperty(localName = "NAME",isAttribute = true)
    private String name;
    @JacksonXmlElementWrapper(localName = "Grades")
    @JacksonXmlProperty(localName = "Grade")
    private List<Grade> grades;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Grade> getGrades() {
        return grades;
    }

    public void setGrades(List<Grade> grades) {
        this.grades = grades;
    }
}
