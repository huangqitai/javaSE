package com.hqt.xml;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "dataset")
public class SdResponseVo {
    /**
     * <?xml version="1.0" encoding="UTF-8" ?>
     * <dataset>
     * 	<smsrow>
     * 		<sendName>发送人姓名</sendName>
     * 		<sendId>发送人id</sendId>
     * 		<rcvName>接收人姓名</rcvName>
     * 		<rcvId>接收人id</rcvId>
     * 		<depId>发送者部门id</depId>
     * 		<depName>发送者部门名称</depName>
     * 		<deliverTime>短信投递时间</deliverTime>
     * 		<requestTime >短信提交时间</requestTime>
     * 		<submitTime >短信发送时间</submitTime >
     * 		<phone>接收者手机</phone>
     * 		<content>短信发送内容</content>
     * 	    <docUuid>发送短信id</docUuid>
     * 		<partCount>短信拆分数量</partCount>
     * 		<partStatus>状态</partStatus>
     * 		<smsresult>是否已回复</smsresult>
     * 	</smsrow>
     * 	<childs>发送条数</childs>
     * </dataset>
     */
    @JacksonXmlProperty(localName = "smsrow")
    private SmsRow smsRow;
    @JacksonXmlProperty(localName = "childs")
    private String childs;

    @JsonIgnore
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public SmsRow getSmsRow() {
        return smsRow;
    }

    public void setSmsRow(SmsRow smsRow) {
        this.smsRow = smsRow;
    }

    public String getChilds() {
        return childs;
    }

    public void setChilds(String childs) {
        this.childs = childs;
    }
}
