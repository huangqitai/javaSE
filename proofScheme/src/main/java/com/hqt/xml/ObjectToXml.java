package com.hqt.xml;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.dom4j.Document;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.junit.Test;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

public class ObjectToXml {
    //创建XmlMapper对象，用于实体与json和xml之间的相互转换
    private static final XmlMapper xmlMapper = new XmlMapper();
    @Test
    public void t1() throws Exception {
        School school = new School();
        school.setName("学校名称");
        Grade grade1 = new Grade();
        grade1.setName("年级1");
        Grade grade2 = new Grade();
        grade2.setName("年级2");
        Grade grade3 = new Grade();
        grade3.setName("年级3");
        Grade grade4 = new Grade();
        grade4.setName("年级4");
        Classes classes1 = new Classes();
        classes1.setName("年级1-班级1");
        classes1.setStudentCount(60);
        Classes classes2 = new Classes();
        classes2.setName("年级1-班级2");
        classes2.setStudentCount(50);
        Classes classes3 = new Classes();
        classes3.setName("年级1-班级3");
        classes3.setStudentCount(52);
        List<Classes> classesList1 = new ArrayList<>();
        classesList1.add(classes1);
        classesList1.add(classes2);
        classesList1.add(classes3);
        grade1.setClasses(classesList1);
        Classes classes4 = new Classes();
        classes4.setName("年级2-班级1");
        classes4.setStudentCount(45);
        Classes classes5 = new Classes();
        classes5.setName("年级2-班级2");
        classes5.setStudentCount(46);
        Classes classes6 = new Classes();
        classes6.setName("年级2-班级3");
        classes6.setStudentCount(42);
        List<Classes> classesList2 = new ArrayList<>();
        classesList2.add(classes4);
        classesList2.add(classes5);
        classesList2.add(classes6);
        grade2.setClasses(classesList2);
        List<Grade> grades = new ArrayList<>();
        grades.add(grade1);
        grades.add(grade2);
        grades.add(grade3);
        grades.add(grade4);
        school.setGrades(grades);
        String xmlStr = xmlMapper.writeValueAsString(school);
        xmlStr = formatXML(xmlStr);
        System.out.println(xmlStr);
    }

    /**
     * 格式化xml
     * @param inputXML
     * @return
     * @throws Exception
     */
    public String formatXML(String inputXML) throws Exception {
        SAXReader reader = new SAXReader();
        Document document = reader.read(new StringReader(inputXML));
        String requestXML = null;
        XMLWriter writer = null;
        if (document != null) {
            try {
                StringWriter stringWriter = new StringWriter();
                OutputFormat format = new OutputFormat(" ", true);
                format.setTrimText(true);
                format.setExpandEmptyElements(true);
                writer = new XMLWriter(stringWriter, format);
                writer.write(document);
                writer.flush();
                requestXML = stringWriter.getBuffer().toString();
            } finally {
                if (writer != null) {
                    try {
                        writer.close();
                    } catch (IOException e) {
                    }
                }
            }
        }
        return requestXML;
    }

    @Test
    public void t2() throws JsonProcessingException {
        String xmlStr = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "\n" +
                "<School NAME=\"学校名称\">\n" +
                " <Grades>\n" +
                "  <Grade>\n" +
                "   <NAME>年级1</NAME>\n" +
                "   <ClassList>\n" +
                "    <Class>\n" +
                "     <name>年级1-班级1</name>\n" +
                "     <studentCount>60</studentCount>\n" +
                "    </Class>\n" +
                "    <Class>\n" +
                "     <name>年级1-班级2</name>\n" +
                "     <studentCount>50</studentCount>\n" +
                "    </Class>\n" +
                "    <Class>\n" +
                "     <name>年级1-班级3</name>\n" +
                "     <studentCount>52</studentCount>\n" +
                "    </Class>\n" +
                "   </ClassList>\n" +
                "  </Grade>\n" +
                "  <Grade>\n" +
                "   <NAME>年级2</NAME>\n" +
                "   <ClassList>\n" +
                "    <Class>\n" +
                "     <name>年级2-班级1</name>\n" +
                "     <studentCount>45</studentCount>\n" +
                "    </Class>\n" +
                "    <Class>\n" +
                "     <name>年级2-班级2</name>\n" +
                "     <studentCount>46</studentCount>\n" +
                "    </Class>\n" +
                "    <Class>\n" +
                "     <name>年级2-班级3</name>\n" +
                "     <studentCount>42</studentCount>\n" +
                "    </Class>\n" +
                "   </ClassList>\n" +
                "  </Grade>\n" +
                "  <Grade>\n" +
                "   <NAME>年级3</NAME>\n" +
                "  </Grade>\n" +
                "  <Grade>\n" +
                "   <NAME>年级4</NAME>\n" +
                "  </Grade>\n" +
                " </Grades>\n" +
                "</School>";
        School school = xmlMapper.readValue(xmlStr,School.class);
        System.out.println(school.getName());
    }
}
